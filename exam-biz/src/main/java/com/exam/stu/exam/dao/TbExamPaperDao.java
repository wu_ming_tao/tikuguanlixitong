package com.exam.stu.exam.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.exam.model.TbExamPaper;

/**
 * 试卷  DAO层
 * 
 * @author  WANLITAO
 * 2020-04-04 18:45
 */
public class TbExamPaperDao extends IAbstractDaoImpl<TbExamPaper>
{

}