package com.exam.stu.exam.model;

import java.math.BigDecimal;
import java.util.Date;
 
 
import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 课程表 
 * @author WANLITAO
 * 2020-04-20 15:52
 */
public class TbCourse extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6091810393075349308L;
	/** 课程编号 */
 	private String id;
	/** 考试编号 */
 	private String examId;
	/** 课程名 */
 	private String name;
	/** 教师号 */
 	private String teacherId;
	/** 任课教师 */
 	private String teacherName;
	/** 学生学号 */
 	private String studentId;
	/** 学生姓名 */
 	private String studentName;
	/** 学生成绩 */
 	private String grade;
 	/** 学生帐号 */
 	private String account;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	
	/**
     * 课程编号
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 课程编号
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 考试编号
     * @param examId
     */
    public void setExamId(String examId)
    {
        this.examId = examId;
    }
    
    /**
     * 考试编号
     * @return
     */
	public String getExamId()
    {
        return examId;
    }
    
    
	/**
     * 课程名
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * 课程名
     * @return
     */
	public String getName()
    {
        return name;
    }
    
    
	/**
     * 教师号
     * @param teacherId
     */
    public void setTeacherId(String teacherId)
    {
        this.teacherId = teacherId;
    }
    
    /**
     * 教师号
     * @return
     */
	public String getTeacherId()
    {
        return teacherId;
    }
    
    
	/**
     * 任课教师
     * @param teacherName
     */
    public void setTeacherName(String teacherName)
    {
        this.teacherName = teacherName;
    }
    
    /**
     * 任课教师
     * @return
     */
	public String getTeacherName()
    {
        return teacherName;
    }
    
    
	/**
     * 学生学号
     * @param studentId
     */
    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }
    
    /**
     * 学生学号
     * @return
     */
	public String getStudentId()
    {
        return studentId;
    }
    
    
	/**
     * 学生姓名
     * @param studentName
     */
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }
    
    /**
     * 学生姓名
     * @return
     */
	public String getStudentName()
    {
        return studentName;
    }
    
    
	/**
     * 学生成绩
     * @param grade
     */
    public void setGrade(String grade)
    {
        this.grade = grade;
    }
    
    /**
     * 学生成绩
     * @return
     */
	public String getGrade()
    {
        return grade;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
    
    
}