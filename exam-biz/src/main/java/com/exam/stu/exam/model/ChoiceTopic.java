package com.exam.stu.exam.model;

import java.io.Serializable;
/**
 * 选择题参数
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public class ChoiceTopic {

	
	/** 选择题类型 */
 	private String choicType;
 	/** 随机题数 */
 	private String choiceRandom;
 	/** 容易题数 */
 	private String choiceLow;
 	/** 一般题数 */
 	private String choiceMedium;
 	/** 困难题数 */
 	private String choiceIssue;
 	/** 总题数 */
 	private String choiceNumber;
 	/** 题的分值 */
 	private String choiceScore;
 	/** 是否有数据 */
 	boolean hasData = false;
	public String getChoicType() {
		return choicType;
	}
	public void setChoicType(String choicType) {
		this.choicType = choicType;
	}
	public int getChoiceRandom() {
		int random = Integer.parseInt(choiceRandom);
		return random;
	}
	public void setChoiceRandom(String choiceRandom) {
		this.choiceRandom = choiceRandom;
	}
	public int getChoiceLow() {
		int low = Integer.parseInt(choiceLow);
		return low;
	}
	public void setChoiceLow(String choiceLow) {
		this.choiceLow = choiceLow;
	}
	public int getChoiceMedium() {
		int medium = Integer.parseInt(choiceMedium);
		return medium;
	}
	public void setChoiceMedium(String choiceMedium) {
		this.choiceMedium = choiceMedium;
	}
	public int getChoiceIssue() {
		int issue = Integer.parseInt(choiceIssue);		
		return issue;
	}
	public void setChoiceIssue(String choiceIssue) {
		this.choiceIssue = choiceIssue;
	}
	public int getChoiceNumber() {
		
		return getChoiceIssue()+getChoiceMedium()+getChoiceLow()+getChoiceRandom();
	}
	public void setChoiceNumber(String choiceNumber) {
		this.choiceNumber = choiceNumber;
	}
	public int getChoiceScore() {
		int score = Integer.parseInt(choiceScore);		
		return score;
	}
	public void setChoiceScore(String choiceScore) {
		this.choiceScore = choiceScore;
	}
	public boolean isHasData() {
		if(choiceRandom==null&&choiceLow==null&&choiceMedium==null&&choiceIssue==null) {
		return hasData;
		}
		return true;
		
	}
	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}

}
