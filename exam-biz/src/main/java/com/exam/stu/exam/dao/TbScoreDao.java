package com.exam.stu.exam.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.exam.model.TbScore;

/**
 * 成绩表  DAO层
 * 
 * @author  WANLITAO
 * 2020-04-21 18:53
 */
public class TbScoreDao extends IAbstractDaoImpl<TbScore>
{

}