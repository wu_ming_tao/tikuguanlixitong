package com.exam.stu.exam.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.exam.model.TbExam;

/**
 * 考试表  DAO层
 * 
 * @author  WANLITAO
 * 2020-04-20 16:43
 */
public class TbExamDao extends IAbstractDaoImpl<TbExam>
{

}