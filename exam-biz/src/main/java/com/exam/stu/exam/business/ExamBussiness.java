package com.exam.stu.exam.business;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.service.IAbstractService;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.exam.model.TbCourse;
import com.exam.stu.exam.model.TbExam;
import com.exam.stu.exam.model.TbExamPaper;
import com.exam.stu.exam.model.TbResults;
import com.exam.stu.exam.model.TbTopicPaper;
import com.exam.stu.exam.service.TbCourseService;
import com.exam.stu.exam.service.TbExamService;
import com.exam.stu.exam.service.TbResultsService;
import com.exam.stu.exam.service.TbTopicPaperService;
import com.exam.stu.topic.service.TbTopicService;

@Service("ExamBussiness")
public class ExamBussiness extends AbstractBusiness<TbExam>{
	
	@Autowired
	private TbExamService tbExamService;
	@Autowired
	private TbResultsService tbResultsService;
	@Autowired
	private TbCourseService tbCourseService;
	@Autowired
	private TbTopicPaperService tbTopicPaperService;
	@Override
	protected IAbstractService<TbExam> getBaseService() {
		// TODO Auto-generated method stub
		return tbExamService;
	}
	/**
	 * 获取考试表格数据
	 * @param request
	 * @return
	 */
	public ResponsePageModel<TbExam> getExamData(HttpServletRequest request) {
		try {

			TbExam queryModel = ServletBeanTools
					.populate(TbExam.class, request); 
			return this.getPageData(queryModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	public void addResults(TbResults result) {
		
		tbResultsService.add(result);
	}
    public List<TbResults> findResults(String id) {
    	
    	TbResults Results = new TbResults();
    	Results.setId(id);
    	List<TbResults> listData=tbResultsService.queryList(Results);
    	List<TbResults> list = new ArrayList<TbResults>();
    	for(TbResults result : listData) {
    		if("问答题".equals(result.getType())) {
    			TbTopicPaper topicPaper = tbTopicPaperService.findById(result.getExamPaperId());
    			result.setTopicPaper(topicPaper);
    			list.add(result);
    		}
    	}
    	List<TbResults> lists=sort(list);
		return lists;
	}
    public List<TbResults> sort(List<TbResults> listData) {
    	List<TbResults> list = new ArrayList<TbResults>();
    	for(int i = 1;i<=listData.size();i++) {
    		for(TbResults result:listData) {
    			if(result.getNumber()!=null) {
    			int a= Integer.parseInt(result.getNumber());
    			if(i==a){
    				list.add(result);
    			}
    			}
    		}
    	}
    	return list;
    }
    public void addCourse(TbCourse result) {
    	
		
    	tbCourseService.add(result);
	}
}
