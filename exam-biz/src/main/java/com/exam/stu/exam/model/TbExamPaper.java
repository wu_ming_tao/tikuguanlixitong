package com.exam.stu.exam.model;

import java.math.BigDecimal;
import java.util.Date;
 
 
import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 试卷 
 * @author WANLITAO
 * 2020-04-04 20:41
 */
public class TbExamPaper extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6398087670472847022L;
	/** 试卷编号 */
 	private String id;
	/** 试卷名称 */
 	private String name;
	/** 课程编号 */
 	private String courseId;
	/** 考试时间 */
 	private Date time;
	/** 考试时间 开始时间 */
 	private String timeBegin;
    /** 考试时间 结束时间 */
 	private String timeEnd;
	/** 总分 */
 	private Integer total;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 生成方式 */
 	private String generationType;
	
	/**
     * 试卷编号
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 试卷编号
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 试卷名称
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * 试卷名称
     * @return
     */
	public String getName()
    {
        return name;
    }
    
    
	/**
     * 课程编号
     * @param courseId
     */
    public void setCourseId(String courseId)
    {
        this.courseId = courseId;
    }
    
    /**
     * 课程编号
     * @return
     */
	public String getCourseId()
    {
        return courseId;
    }
    
    
	/**
     * 考试时间
     * @param time
     */
    public void setTime(Date time)
    {
        this.time = time;
    }
    
    /**
     * 考试时间
     * @return
     */
	public Date getTime()
    {
        return time;
    }
    
    
    /**
     * 考试时间 开始时间
     * @param timeBegin
     */
    public void setTimeBegin(String timeBegin)
    {
        this.timeBegin = timeBegin;
    }
    
 	/**
     * 考试时间 开始时间
     * @return
     */
	public String getTimeBegin()
    {
        return timeBegin;
    }
    
    /**
     * 考试时间 结束时间
     * @param timeEnd
     */
    public void setTimeEnd(String timeEnd)
    {
        this.timeEnd = timeEnd;
    }
 	
 	/**
     * 考试时间 结束时间
     * @return
     */
	public String getTimeEnd()
    {
        return timeEnd;
    }
    
	/**
     * 总分
     * @param total
     */
    public void setTotal(Integer total)
    {
        this.total = total;
    }
    
    /**
     * 总分
     * @return
     */
	public Integer getTotal()
    {
        return total;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 生成方式
     * @param generationType
     */
    public void setGenerationType(String generationType)
    {
        this.generationType = generationType;
    }
    
    /**
     * 生成方式
     * @return
     */
	public String getGenerationType()
    {
        return generationType;
    }
    
    
}