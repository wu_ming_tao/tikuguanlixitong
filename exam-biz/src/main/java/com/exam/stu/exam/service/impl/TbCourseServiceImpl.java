package com.exam.stu.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.exam.dao.TbCourseDao;
import com.exam.stu.exam.model.TbCourse;
import com.exam.stu.exam.service.TbCourseService;
 


/**
 * 课程表  Service实现
 * 
 * @author  WANLITAO
 * 2020-04-20 15:52
 */
@Service("tbCourseService")
public class TbCourseServiceImpl extends AbstractServiceImpl<TbCourse> implements TbCourseService
{

    @Autowired
    private TbCourseDao tbCourseDao;

	@Override
	protected IAbstractDao<TbCourse> getBaseDao()
	{
		return tbCourseDao;
	}
}