package com.exam.stu.exam.model;

import java.io.Serializable;
import java.util.List;

/**
 * 试题表单
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public class TbTopics implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4272080142844018772L;	
 	/** 题型 */
 	private String type;
 	/** 试题试卷 */
 	private List<TbTopicPaper> tbTopicPaper;
 	/** 题型编号 */
 	private int typeSort;
 	/** 数量 */
 	private int count;
 	/** 总分 */
 	private int totalNumber;
 	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getTotalNumber() {
		return totalNumber;
	}
	public void setTotalNumber(int totalNumber) {
		this.totalNumber = totalNumber;
	}
	/**
	 * 是否有数据
	 */
	boolean hasData = false;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<TbTopicPaper> getTbTopicPaper() {
		return tbTopicPaper;
	}
	public void setTbTopicPaper(List<TbTopicPaper> tbTopicPaper) {
		this.tbTopicPaper = tbTopicPaper;
	}
	public boolean isHasData() {
		return hasData;
	}

	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}
	public int getTypeSort() {
		return typeSort;
	}
	public void setTypeSort(int typeSort) {
		this.typeSort = typeSort;
	}
}
