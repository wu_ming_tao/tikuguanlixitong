package com.exam.stu.exam.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.exam.model.TbCourse;

/**
 * 课程表  Service
 * 
 * @author WANLITAO
 * 2020-04-20 15:52
 */
public interface TbCourseService extends IAbstractService<TbCourse>
{

}
