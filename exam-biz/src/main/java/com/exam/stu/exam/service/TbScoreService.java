package com.exam.stu.exam.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.exam.model.TbScore;


/**
 * 成绩表  Service
 * 
 * @author WANLITAO
 * 2020-04-21 18:53
 */
public interface TbScoreService extends IAbstractService<TbScore>
{

}
