package com.exam.stu.exam.model;

import java.io.Serializable;

/**
 * 判断题参数
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public class JudgementTopic {

	
	/** 选择题类型 */
 	private String judgementType;
 	/** 随机题数 */
 	private String judgementRandom;
 	/** 容易题数 */
 	private String judgementLow;
 	/** 一般题数 */
 	private String judgementMedium;
 	/** 困难题数 */
 	private String judgementIssue;
 	/** 总题数 */
 	private String judgementNumber;
 	/** 题的分值 */
 	private String judgementScore;
 	/** 是否有数据 */
 	boolean hasData = false;
	public String getJudgementType() {
		return judgementType;
	}
	public void setJudgementType(String judgementType) {
		this.judgementType = judgementType;
	}
	public int getJudgementRandom() {
		int medium = Integer.parseInt(judgementRandom);
		return medium;		
	}
	public void setJudgementRandom(String judgementRandom) {
		this.judgementRandom = judgementRandom;
	}
	public int getJudgementLow() {
		int medium = Integer.parseInt(judgementLow);
		return medium;	
	}
	public void setJudgementLow(String judgementLow) {
		this.judgementLow = judgementLow;
	}
	public int getJudgementMedium() {
		int medium = Integer.parseInt(judgementMedium);
		return medium;	
	}
	public void setJudgementMedium(String judgementMedium) {
		this.judgementMedium = judgementMedium;
	}
	public int getJudgementIssue() {
		int medium = Integer.parseInt(judgementIssue);
		return medium;
	}
	public void setJudgementIssue(String judgementIssue) {
		this.judgementIssue = judgementIssue;
	}
	public int getJudgementNumber() {
		int medium = Integer.parseInt(judgementNumber);
		return medium;
	}
	public void setJudgementNumber(String judgementNumber) {
		this.judgementNumber = judgementNumber;
	}
	public int getJudgementScore() {
		int medium = Integer.parseInt(judgementScore);
		return medium;
	}
	public void setJudgementScore(String judgementScore) {
		this.judgementScore = judgementScore;
	}
	public boolean isHasData() {
		if(judgementRandom==null&&judgementLow==null&&judgementMedium==null&&judgementIssue==null) {
			return hasData;
			}
			return true;
	}
	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}
	
}
