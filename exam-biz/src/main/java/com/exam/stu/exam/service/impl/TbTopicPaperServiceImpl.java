package com.exam.stu.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.exam.dao.TbTopicPaperDao;
import com.exam.stu.exam.model.TbTopicPaper;
import com.exam.stu.exam.service.TbTopicPaperService;
 


/**
 * 试题试卷  Service实现
 * 
 * @author  WANLITAO
 * 2020-04-04 18:45
 */
@Service("tbTopicPaperService")
public class TbTopicPaperServiceImpl extends AbstractServiceImpl<TbTopicPaper> implements TbTopicPaperService
{

    @Autowired
    private TbTopicPaperDao tbTopicPaperDao;

	@Override
	protected IAbstractDao<TbTopicPaper> getBaseDao()
	{
		return tbTopicPaperDao;
	}
}