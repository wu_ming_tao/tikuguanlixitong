package com.exam.stu.exam.model;

import java.math.BigDecimal;
import java.util.Date;
 
 
import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 成绩表 
 * @author WANLITAO
 * 2020-04-21 18:53
 */
public class TbScore extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1403668762626829405L;
	/** 学生ID */
 	private String id;
	/** 学生姓名 */
 	private String studentName;
	/** 考试编号 */
 	private String examId;
	/** 课程名 */
 	private String courseName;
	/** 任课教师 */
 	private String teacherName;
	/** 成绩 */
 	private Integer score;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	
	/**
     * 学生ID
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 学生ID
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 学生姓名
     * @param studentName
     */
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }
    
    /**
     * 学生姓名
     * @return
     */
	public String getStudentName()
    {
        return studentName;
    }
    
    
	/**
     * 考试编号
     * @param examId
     */
    public void setExamId(String examId)
    {
        this.examId = examId;
    }
    
    /**
     * 考试编号
     * @return
     */
	public String getExamId()
    {
        return examId;
    }
    
    
	/**
     * 课程名
     * @param courseName
     */
    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }
    
    /**
     * 课程名
     * @return
     */
	public String getCourseName()
    {
        return courseName;
    }
    
    
	/**
     * 任课教师
     * @param teacherName
     */
    public void setTeacherName(String teacherName)
    {
        this.teacherName = teacherName;
    }
    
    /**
     * 任课教师
     * @return
     */
	public String getTeacherName()
    {
        return teacherName;
    }
    
    
	/**
     * 成绩
     * @param score
     */
    public void setScore(Integer score)
    {
        this.score = score;
    }
    
    /**
     * 成绩
     * @return
     */
	public Integer getScore()
    {
        return score;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
}