package com.exam.stu.exam.model;

import java.io.Serializable;

/**
 * 试题表单
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public class TypeCount implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8221171861160430279L;
	/** 选择题数量 */
 	private int choiceCount;
 	/** 判断题数量 */
 	private int judgementCount;
 	/** 问答题数量 */
 	private int anserCount;
 	public int getChoiceCount() {
		return choiceCount;
	}
	public void setChoiceCount(int choiceCount) {
		this.choiceCount = choiceCount;
	}
	public int getJudgementCount() {
		return judgementCount;
	}
	public void setJudgementCount(int judgementCount) {
		this.judgementCount = judgementCount;
	}
	public int getAnserCount() {
		return anserCount;
	}
	public void setAnserCount(int anserCount) {
		this.anserCount = anserCount;
	}
	public boolean isHasData() {
		return hasData;
	}
	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}
	/** 是否有数据 */
 	private boolean hasData = false;
}
