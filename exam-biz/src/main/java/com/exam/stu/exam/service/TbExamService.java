package com.exam.stu.exam.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.exam.model.TbExam;

/**
 * 考试表  Service
 * 
 * @author WANLITAO
 * 2020-04-20 16:43
 */
public interface TbExamService extends IAbstractService<TbExam>
{

}
