package com.exam.stu.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.exam.dao.TbScoreDao;
import com.exam.stu.exam.model.TbScore;
import com.exam.stu.exam.service.TbScoreService;
 


/**
 * 成绩表  Service实现
 * 
 * @author  WANLITAO
 * 2020-04-21 18:53
 */
@Service("tbScoreService")
public class TbScoreServiceImpl extends AbstractServiceImpl<TbScore> implements TbScoreService
{

    @Autowired
    private TbScoreDao tbScoreDao;

	@Override
	protected IAbstractDao<TbScore> getBaseDao()
	{
		return tbScoreDao;
	}
}