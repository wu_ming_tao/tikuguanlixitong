package com.exam.stu.exam.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.exam.model.TbCourse;

/**
 * 课程表  DAO层
 * 
 * @author  WANLITAO
 * 2020-04-20 15:52
 */
public class TbCourseDao extends IAbstractDaoImpl<TbCourse>
{

}