package com.exam.stu.exam.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.service.IAbstractService;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.exam.model.TbExam;
import com.exam.stu.exam.model.TbExamPaper;
import com.exam.stu.exam.model.TbTopicPaper;
import com.exam.stu.exam.model.TbTopics;
import com.exam.stu.exam.service.TbExamPaperService;
import com.exam.stu.exam.service.TbTopicPaperService;
import com.exam.stu.topic.model.TbTopic;
import com.exam.stu.topic.service.TbTopicService;

@Service("examPaperBussiness")
public class ExamPaperBussiness  extends AbstractBusiness<TbExamPaper> {

	@Autowired
	private TbTopicPaperService tbTopicPaperService;
	@Autowired
	private TbExamPaperService tbExamPaperService;
	@Autowired
	private TbTopicService tbTopicService;

	
	
	@Override
	protected IAbstractService<TbExamPaper> getBaseService() { 
		return tbExamPaperService;
	}
	/**
	 * 获取试卷表格数据
	 * @param request
	 * @return
	 */
	public ResponsePageModel<TbExamPaper> getExamPageData(HttpServletRequest request) {
		try {

			TbExamPaper queryModel = ServletBeanTools
					.populate(TbExamPaper.class, request); 
			return this.getPageData(queryModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	


	/**
	 * 删除临时表数据信息
	 * @param userId
	 * @return
	 */
	public void clearn()
	{
		TbExamPaper examPaper = new TbExamPaper();
		List<TbExamPaper> listData = tbExamPaperService.queryList(examPaper);
		for(TbExamPaper tbExamPaper : listData) {
			if(tbExamPaper.getCreatedBy()==null){				
				tbExamPaperService.delete(tbExamPaper.getId());
			}
		}
		TbExamPaper examPapers = new TbExamPaper();
		List<TbExamPaper> listDatas = tbExamPaperService.queryList(examPapers);
		TbTopicPaper topicePaper = new TbTopicPaper();
		List<TbTopicPaper> lisData = tbTopicPaperService.queryList(topicePaper);		
		for(TbTopicPaper topicPapers : lisData) {	
			boolean messge = false;
			for(TbExamPaper ExamPaper : listDatas) {
				if(ExamPaper.getId().equals(topicPapers.getPaperId())){
					messge = true;
					break;
				}				
			}
			if(!messge) {
			tbTopicPaperService.delete(topicPapers.getId());
			}
		}	
	}
	
	
	/**
	 * 数量和总分
	 * @param List<TbTopicPaper>
	 * @return
	 */
	public List<TbTopics> countType(List<TbTopics> listdata){		
		if(listdata.size()>0) {
			int x = 1 ;
				for(TbTopics topics:listdata) {
					if("选择题(单)".equals(topics.getType())) {						
						x =topics.getTbTopicPaper().size();
						topics.setCount(x);	
						topics.setTotalNumber(totalNumber(topics.getTbTopicPaper()));						
					}
				}
				for(TbTopics topics:listdata) {
					if("判断题".equals(topics.getType())) {
						x =topics.getTbTopicPaper().size();
						topics.setCount(x);
						topics.setTotalNumber(totalNumber(topics.getTbTopicPaper()));	
					}
				}				
				for(TbTopics topics:listdata) {
					if("问答题".equals(topics.getType())) {
						x =topics.getTbTopicPaper().size();
						topics.setCount(x);
						topics.setTotalNumber(totalNumber(topics.getTbTopicPaper()));	
					}
				}
		}
		return listdata;
		}
	/**
	 * 总分
	 * @param List<TbTopicPaper>
	 * @return
	 */
	public int totalNumber(List<TbTopicPaper> listdata){
		int total=0;
		for(TbTopicPaper topics: listdata) {
			if(topics.getValue()!=null) {
			total = total+topics.getValue();
			}
		}
		return total;
		}
	/**
	 * 获取试题试卷全部试题信息
	 * @param id
	 * @return
	 */
	public List<TbTopics> FindByPaperId(String id)
	{
		TbTopicPaper topicPaper = new TbTopicPaper();
		topicPaper.setPaperId(id);		
		List<TbTopicPaper> listData = tbTopicPaperService.queryList(topicPaper);		
		List<TbTopics> lisData = new ArrayList<TbTopics>();
		if(!listData.isEmpty()) {
		Map<String,List<TbTopicPaper>> btnMap = new HashMap<String, List<TbTopicPaper>>();
		for(TbTopicPaper paper : listData)	{
			String topicId = paper.getTopicId();
			TbTopic topic = tbTopicService.findById(topicId);
			paper.setTopics(topic);			
			if(!btnMap.containsKey(paper.getType())) {
				btnMap.put(paper.getType(), new ArrayList<TbTopicPaper>());
			}
			   btnMap.get(paper.getType()).add(paper);
			
		}
		for(String key : btnMap.keySet()) {
			TbTopics topic = new TbTopics();
			topic.setType(key);
			List<TbTopicPaper> newList = sort(btnMap.get(key));
			topic.setTbTopicPaper(newList);
			topic.setHasData(true);
			lisData.add(topic);
		}
		}
		
		return lisData;
	}
	/**
	 * 试题排序
	 * @param List<TbTopicPaper>
	 * @return
	 */
	public List<TbTopicPaper> sort(List<TbTopicPaper> listdata){
		List<TbTopicPaper> newLisData = new ArrayList<TbTopicPaper>();
		if(listdata.size()>0) {
			for(int i= 1;i<listdata.size()+1;i++)
				for(TbTopicPaper topicPaper:listdata) {
					if(topicPaper.getSerial()==null) {
						int j = 1;
						for(TbTopicPaper topicPapers : listdata) {
							topicPapers.setIntSerial(j);
							j++;
						}
					}
					if(topicPaper.getIntSerial()==i) {
						newLisData.add(topicPaper);
					}
				}
			}
		return newLisData;
		}

//	/**
//	 * 组装用户
//	 * @param user
//	 * @return
//	 */
//	public TbUser generateUser(TbUser user) {
//		List<TbRole> lstRole = tbRoleService.queryList(new TbRole());
//
//		UserRole[] userArray = new UserRole[lstRole.size()];
//		int index = 0;
//		for (TbRole tbRole : lstRole) {
//			UserRole userRole = new UserRole();
//			userRole.setRoleId(tbRole.getId());
//			userRole.setRoleName(tbRole.getRoleName());
//
//			userArray[index] = userRole;
//			index++;
//		}
//		user.setRoleArrays(userArray);
//
//		return user;
//	}
//	/**
//	 * 添加用户
//	 * @param queryModel
//	 * @throws Exception 
//	 */
//	public void addUser(TbUser queryModel) throws Exception {
//		
//		TbUser queryUser = new TbUser();
//		queryUser.setAccount(queryModel.getAccount());
//		
//		int count = userService.queryCount(queryUser);
//		
//		if (count > 1)
//		{
//			throw new Exception("登录账号重复!");
//		}
//  
//		if (StringUtils.isEmpty(queryModel.getRoleId()))
//		{
//			return;
//		}
//		
//		queryModel.setId(UUIdUtil.getUUID());
//		
//		String[] roleArrays = queryModel.getRoleId().split(",");
//		
//		List<TbUserRole> lstUserRole = new ArrayList<TbUserRole>();
//		TbUserRole temAdd = null;
//		
//		for (String role : roleArrays) {
//			temAdd = new TbUserRole();
//			temAdd.setRoleId(role);
//			temAdd.setUserId(queryModel.getId());
//			lstUserRole.add(temAdd);
//		}
//		
//		tbUserRoleService.batchAdd(lstUserRole);
//		
//		userService.add(queryModel);
//	}
//	
//	/**
//	 * 添加用户
//	 * @param queryModel
//	 * @throws Exception 
//	 */
//	public void updateUser(TbUser queryModel) throws Exception {
//		
//		TbUser queryUser = new TbUser();
//		queryUser.setAccount(queryModel.getAccount());
//		
//		List<TbUser> lstUser = userService.queryList(queryUser);
//		
//		for (TbUser tbUser : lstUser) {
//			if (queryModel.getAccount().equals(tbUser.getAccount())
//					&& !queryModel.getId().equals(tbUser.getId()))
//			{
//				throw new Exception("登录账号重复!");
//			}
//		}
//		
//        TbUserRole deleteModel = new TbUserRole();
//        deleteModel.setUserId(queryModel.getId());
//        tbUserRoleService.delete(deleteModel);
//        
//		if (StringUtils.isEmpty(queryModel.getRoleId()))
//		{
//			return;
//		} 
//		
//		String[] roleArrays = queryModel.getRoleId().split(",");
//		
//		List<TbUserRole> lstUserRole = new ArrayList<TbUserRole>();
//		TbUserRole temAdd = null;
//		
//		for (String role : roleArrays) {
//			temAdd = new TbUserRole();
//			temAdd.setRoleId(role);
//			temAdd.setUserId(queryModel.getId());
//			lstUserRole.add(temAdd);
//		}
//		
//		tbUserRoleService.batchAdd(lstUserRole);
//		
//		userService.update(queryModel);
//	}
//	/**
//	 * 编辑用户信息
//	 * @param queryModel
//	 * @throws Exception 
//	 */
//	public void editUser(TbUser queryModel) throws Exception {
//		
//		//TbUser queryUser = new TbUser();
//		//queryUser.setId(queryModel.getId());		
//		//List<TbUser> lstUser = userService.queryList(queryUser);
//		userService.update(queryModel);
	
	}