package com.exam.stu.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.exam.dao.TbExamDao;
import com.exam.stu.exam.model.TbExam;
import com.exam.stu.exam.service.TbExamService;
 


/**
 * 考试表  Service实现
 * 
 * @author  WANLITAO
 * 2020-04-20 16:43
 */
@Service("tbExamService")
public class TbExamServiceImpl extends AbstractServiceImpl<TbExam> implements TbExamService
{

    @Autowired
    private TbExamDao tbExamDao;

	@Override
	protected IAbstractDao<TbExam> getBaseDao()
	{
		return tbExamDao;
	}
}