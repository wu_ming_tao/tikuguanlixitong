package com.exam.stu.exam.model;

import java.io.Serializable;

/**
 * 问答题参数
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public class AnswerTopic{

	/** 选择题类型 */
 	private String answerType;
 	/** 随机题数 */
 	private String answerRandom;
 	/** 容易题数 */
 	private String answerLow;
 	/** 一般题数 */
 	private String answerMedium;
 	/** 困难题数 */
 	private String answerIssue;
 	/** 总题数 */
 	private String answerNumber;
 	/** 题的分值 */ 	
	private String answerScore;
	/** 是否有数据 */
 	boolean hasData = false;
	public String getAnswerType() {
		return answerType;
	}
	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}
	public  int getAnswerRandom() {
		int medium = Integer.parseInt(answerRandom);
		return medium;	
	}
	public void setAnswerRandom(String answerRandom) {
		this.answerRandom = answerRandom;
	}
	public int getAnswerLow() {
		int medium = Integer.parseInt(answerLow);
		return medium;	
	}
	public void setAnswerLow(String answerLow) {
		this.answerLow = answerLow;
	}
	public int getAnswerMedium() {
		int medium = Integer.parseInt(answerMedium);
		return medium;	
	}
	public void setAnswerMedium(String answerMedium) {
		this.answerMedium = answerMedium;
	}
	public int getAnswerIssue() {
		int medium = Integer.parseInt(answerIssue);
		return medium;	
	}
	public void setAnswerIssue(String answerIssue) {
		this.answerIssue = answerIssue;
	}
	public int getAnswerNumber() {
		return getAnswerRandom()+getAnswerLow()+getAnswerMedium()+getAnswerIssue();
	}
	public void setAnswerNumber(String answerNumber) {
		this.answerNumber = answerNumber;
	}
	public int getAnswerScore() {
		int medium = Integer.parseInt(answerScore);
		return medium;	
	}
	public void setAnswerScore(String answerScore) {
		this.answerScore = answerScore;
	}
	public boolean isHasData() {
		if(answerRandom==null&&answerLow==null&&answerMedium==null&&answerIssue==null) {
			return hasData;
			}
			return true;
	}
	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}
	
	
	
 	
}
