package com.exam.stu.exam.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.service.IAbstractService;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.exam.model.TbCourse;
import com.exam.stu.exam.model.TbExam;
import com.exam.stu.exam.model.TbResults;
import com.exam.stu.exam.model.TbTopicPaper;
import com.exam.stu.exam.service.TbCourseService;
import com.exam.stu.exam.service.TbResultsService;
import com.exam.stu.exam.service.TbTopicPaperService;

@Service("CourseBussiness")
public class CourseBussiness extends AbstractBusiness<TbCourse>{
	@Autowired
	private TbCourseService tbCourseService;
	@Autowired
	private TbResultsService tbResultsService;
	@Autowired
	private TbTopicPaperService tbTopicPaperService;
	@Override
	protected IAbstractService<TbCourse> getBaseService() {
		// TODO Auto-generated method stub
		return tbCourseService;
	}
	/**
	 * 获取考试表格数据
	 * @param request
	 * @return
	 */
	public ResponsePageModel<TbCourse> getCourseData(HttpServletRequest request) {
		try {

			TbCourse queryModel = ServletBeanTools
					.populate(TbCourse.class, request); 
			return this.getPageData(queryModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	/**
	 * 分数
	 * @param request
	 * @return
	 */
	public void setCourseScore(HttpServletRequest request) {
		String score = request.getParameter("value");
		String id = request.getParameter("id");
		TbCourse course = new TbCourse();
		course.setStudentId(id);
		List<TbCourse> lists = tbCourseService.queryList(course);
		TbCourse courses=lists.get(0);	
		int scores=0;
		if(courses.getGrade()==null) {
		TbResults result =new TbResults();
		result.setId(id);
		List<TbResults> list = tbResultsService.queryList(result);
		for(TbResults res:list) {
			if(res.getType().equals("选择题(单)")||res.getType().equals("判断题")){
			TbTopicPaper topicPaper = tbTopicPaperService.findById(res.getExamPaperId());
			if(res.getContent().equals(topicPaper.getAnswer())){
				scores=scores+res.getValue();
				}
			}
		}	
		int number=Integer.parseInt(score);
		scores=scores+number;	
		courses.setGrade(scores+"");
		}else {
			int value =Integer.parseInt(courses.getGrade());
			int x =Integer.parseInt(score);
			 value = value + x;			
			courses.setGrade(value+"");
		}
		tbCourseService.update(courses);
	}
}

