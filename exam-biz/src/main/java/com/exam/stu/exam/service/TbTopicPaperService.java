package com.exam.stu.exam.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.exam.model.TbTopicPaper;

/**
 * 试题试卷  Service
 * 
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public interface TbTopicPaperService extends IAbstractService<TbTopicPaper>
{

}
