package com.exam.stu.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.exam.dao.TbResultsDao;
import com.exam.stu.exam.model.TbResults;
import com.exam.stu.exam.service.TbResultsService;
 

/**
 * 答题结果  Service实现
 * 
 * @author  WANLITAO
 * 2020-04-20 19:49
 */
@Service("tbResultsService")
public class TbResultsServiceImpl extends AbstractServiceImpl<TbResults> implements TbResultsService
{

    @Autowired
    private TbResultsDao tbResultsDao;

	@Override
	protected IAbstractDao<TbResults> getBaseDao()
	{
		return tbResultsDao;
	}
}