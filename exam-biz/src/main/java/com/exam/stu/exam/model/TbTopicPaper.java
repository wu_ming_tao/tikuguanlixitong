package com.exam.stu.exam.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.jms.Topic;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;
import com.exam.stu.topic.model.TbTopic;

/**
 * 试题试卷 
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public class TbTopicPaper extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4846590705071117469L;
	/** id */
 	private String id;
	/** 试卷编号 */
 	private String paperId;
	/** 试题编号 */
 	private String topicId;
	/** 试卷试题编号 */
 	private String serial;
	/** 题型 */
 	private String type;
	/** 试题内容 */
 	private String topic;
	/** 答案 */
 	private String answer;
	/** 分值 */
 	private Integer value;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
 	
 	private TbTopic topics;
	/**
     * id
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * id
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 试卷编号
     * @param paperId
     */
    public void setPaperId(String paperId)
    {
        this.paperId = paperId;
    }
    
    /**
     * 试卷编号
     * @return
     */
	public String getPaperId()
    {
        return paperId;
    }
    
    
	/**
     * 试题编号
     * @param topicId
     */
    public void setTopicId(String topicId)
    {
        this.topicId = topicId;
    }
    
    /**
     * 试题编号
     * @return
     */
	public String getTopicId()
    {
        return topicId;
    }
    
    
	/**
     * 试卷试题编号
     * @param serial
     */
	
	  public void setIntSerial(int x) { 
		String serial = Integer.toString(x);
	  this.serial = serial; 
	  }
	 
    /**
     * 试卷试题编号
     * @return
     */
	
	  public int getIntSerial() {

     int serial1 = Integer.parseInt(this.serial);
     
	  return serial1; 
	  }
	 
    
    
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
     * 题型
     * @param type
     */
    public void setType(String type)
    {
        this.type = type;
    }
    
    /**
     * 题型
     * @return
     */
	public String getType()
    {
        return type;
    }
    
    
	/**
     * 试题内容
     * @param topic
     */
    public void setTopic(String topic)
    {
        this.topic = topic;
    }
    
    /**
     * 试题内容
     * @return
     */
	public String getTopic()
    {
        return topic;
    }
    
    
	/**
     * 答案
     * @param answer
     */
    public void setAnswer(String answer)
    {
        this.answer = answer;
    }
    
    /**
     * 答案
     * @return
     */
	public String getAnswer()
    {
        return answer;
    }
    
    
	/**
     * 分值
     * @param value
     */
    public void setValue(Integer value)
    {
        this.value = value;
    }
    
    /**
     * 分值
     * @return
     */
	public Integer getValue()
    {
        return value;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }

	public TbTopic getTopics() {
		return topics;
	}

	public void setTopics(TbTopic topics) {
		this.topics = topics;
	}


    
}