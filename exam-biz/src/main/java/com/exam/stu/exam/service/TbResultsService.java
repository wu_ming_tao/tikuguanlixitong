package com.exam.stu.exam.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.exam.model.TbResults;

/**
 * 答题结果  Service
 * 
 * @author WANLITAO
 * 2020-04-20 19:49
 */
public interface TbResultsService extends IAbstractService<TbResults>
{

}
