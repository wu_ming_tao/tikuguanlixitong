package com.exam.stu.exam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.exam.dao.TbExamPaperDao;
import com.exam.stu.exam.model.TbExamPaper;
import com.exam.stu.exam.service.TbExamPaperService;
/**
 * 试卷  Service实现
 * 
 * @author  WANLITAO
 * 2020-04-04 18:45
 */
@Service("tbExamPaperService")
public class TbExamPaperServiceImpl extends AbstractServiceImpl<TbExamPaper> implements TbExamPaperService
{

    @Autowired
    private TbExamPaperDao tbExamPaperDao;

	@Override
	protected IAbstractDao<TbExamPaper> getBaseDao()
	{
		return tbExamPaperDao;
	}
}