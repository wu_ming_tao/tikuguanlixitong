package com.exam.stu.exam.model;

import java.math.BigDecimal;
import java.util.Date;
 
 
import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 答题结果 
 * @author WANLITAO
 * 2020-04-21 21:24
 */
public class TbResults extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7581146537424028096L;
	/** 学生ID */
 	private String id;
	/** 试卷编号 */
 	private String examId;
	/** 题型 */
 	private String type;
	/** 试卷试题编号 */
 	private String number;
	/** 答题内容 */
 	private String content;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 分值 */
 	private Integer value;
	/** 试题试卷ID */
 	private String examPaperId;
	/** 题目 */
 	private String theme;
 	/** 试题试卷 */
 	private TbTopicPaper topicPaper;
	
	/**
     * 学生ID
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 学生ID
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 试卷编号
     * @param examId
     */
    public void setExamId(String examId)
    {
        this.examId = examId;
    }
    
    /**
     * 试卷编号
     * @return
     */
	public String getExamId()
    {
        return examId;
    }
    
    
	/**
     * 题型
     * @param type
     */
    public void setType(String type)
    {
        this.type = type;
    }
    
    /**
     * 题型
     * @return
     */
	public String getType()
    {
        return type;
    }
    
    
	/**
     * 试卷试题编号
     * @param number
     */
    public void setNumber(String number)
    {
        this.number = number;
    }
    
    /**
     * 试卷试题编号
     * @return
     */
	public String getNumber()
    {
        return number;
    }
    
    
	/**
     * 答题内容
     * @param content
     */
    public void setContent(String content)
    {
        this.content = content;
    }
    
    /**
     * 答题内容
     * @return
     */
	public String getContent()
    {
        return content;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 分值
     * @param value
     */
    public void setValue(Integer value)
    {
        this.value = value;
    }
    
    /**
     * 分值
     * @return
     */
	public Integer getValue()
    {
        return value;
    }
    
    
	/**
     * 试题试卷ID
     * @param examPaperId
     */
    public void setExamPaperId(String examPaperId)
    {
        this.examPaperId = examPaperId;
    }
    
    /**
     * 试题试卷ID
     * @return
     */
	public String getExamPaperId()
    {
        return examPaperId;
    }
    
    
	/**
     * 题目
     * @param theme
     */
    public void setTheme(String theme)
    {
        this.theme = theme;
    }
    
    /**
     * 题目
     * @return
     */
	public String getTheme()
    {
        return theme;
    }

	public TbTopicPaper getTopicPaper() {
		return topicPaper;
	}

	public void setTopicPaper(TbTopicPaper topicPaper) {
		this.topicPaper = topicPaper;
	}
    
    
}