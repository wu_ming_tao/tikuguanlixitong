package com.exam.stu.exam.model;

import java.math.BigDecimal;
import java.util.Date;
 
 
import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 考试表 
 * @author WANLITAO
 * 2020-04-20 16:43
 */
public class TbExam extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1185177430379763211L;
	/** 考试编号 */
 	private String id;
	/** 教师名 */
 	private String teacherName;
	/** 教师ID */
 	private String teacherId;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 试卷名称 */
 	private String examName;
	
	/**
     * 考试编号
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 考试编号
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 教师名
     * @param teacherName
     */
    public void setTeacherName(String teacherName)
    {
        this.teacherName = teacherName;
    }
    
    /**
     * 教师名
     * @return
     */
	public String getTeacherName()
    {
        return teacherName;
    }
    
    
	/**
     * 教师ID
     * @param teacherId
     */
    public void setTeacherId(String teacherId)
    {
        this.teacherId = teacherId;
    }
    
    /**
     * 教师ID
     * @return
     */
	public String getTeacherId()
    {
        return teacherId;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 试卷名称
     * @param examName
     */
    public void setExamName(String examName)
    {
        this.examName = examName;
    }
    
    /**
     * 试卷名称
     * @return
     */
	public String getExamName()
    {
        return examName;
    }
    
    
}