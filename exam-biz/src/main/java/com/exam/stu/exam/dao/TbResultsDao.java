package com.exam.stu.exam.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.exam.model.TbResults;

/**
 * 答题结果  DAO层
 * 
 * @author  WANLITAO
 * 2020-04-20 19:49
 */
public class TbResultsDao extends IAbstractDaoImpl<TbResults>
{

}