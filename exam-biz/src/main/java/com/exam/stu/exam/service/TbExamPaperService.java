package com.exam.stu.exam.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.exam.model.TbExamPaper;


/**
 * 试卷  Service
 * 
 * @author WANLITAO
 * 2020-04-04 18:45
 */
public interface TbExamPaperService extends IAbstractService<TbExamPaper>
{

}
