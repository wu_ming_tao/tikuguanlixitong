package com.exam.stu.role.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 



import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.role.dao.TbUserRoleDao;
import com.exam.stu.role.model.TbUserRole;
import com.exam.stu.role.service.TbUserRoleService;
  

/**
 * 用户角色表  Service实现
 * 
 * @author  WANLITAO
 * 2020-03-21 09:47
 */
@Service("tbUserRoleService")
public class TbUserRoleServiceImpl extends AbstractServiceImpl<TbUserRole> implements TbUserRoleService
{

    @Autowired
    private TbUserRoleDao tbUserRoleDao;

	@Override
	protected IAbstractDao<TbUserRole> getBaseDao()
	{
		return tbUserRoleDao;
	}
}