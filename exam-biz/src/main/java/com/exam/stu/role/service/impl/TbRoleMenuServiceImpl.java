package com.exam.stu.role.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 



import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.role.dao.TbRoleMenuDao;
import com.exam.stu.role.model.TbRoleMenu;
import com.exam.stu.role.service.TbRoleMenuService;
  

/**
 * 角色菜单关联  Service实现
 * 
 * @author  WANLITAO
 * 2020-03-21 09:47
 */
@Service("tbRoleMenuService")
public class TbRoleMenuServiceImpl extends AbstractServiceImpl<TbRoleMenu> implements TbRoleMenuService
{

    @Autowired
    private TbRoleMenuDao tbRoleMenuDao;

	@Override
	protected IAbstractDao<TbRoleMenu> getBaseDao()
	{
		return tbRoleMenuDao;
	}
}