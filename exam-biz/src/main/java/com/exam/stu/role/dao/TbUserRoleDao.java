package com.exam.stu.role.dao;

import com.exam.base.dao.IAbstractDaoImpl; 
import com.exam.stu.role.model.TbUserRole;

/**
 * 用户角色表  DAO层
 * 
 * @author  WANLITAO
 * 2020-03-21 09:42
 */
public class TbUserRoleDao extends IAbstractDaoImpl<TbUserRole>
{

}