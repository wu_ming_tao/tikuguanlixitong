package com.exam.stu.role.service;

import com.exam.base.service.IAbstractService; 
import com.exam.stu.role.model.TbRoleMenu;
/**
 * 角色菜单关联  Service
 * 
 * @author WANLITAO
 * 2020-03-21 09:47
 */
public interface TbRoleMenuService extends IAbstractService<TbRoleMenu>
{

}
