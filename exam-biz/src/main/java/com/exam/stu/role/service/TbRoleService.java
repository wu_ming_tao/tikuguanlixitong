package com.exam.stu.role.service;

import com.exam.base.service.IAbstractService; 
import com.exam.stu.role.model.TbRole;

/**
 * 角色表  Service
 * 
 * @author WANLITAO
 * 2020-03-21 09:47
 */
public interface TbRoleService extends IAbstractService<TbRole>
{

}
