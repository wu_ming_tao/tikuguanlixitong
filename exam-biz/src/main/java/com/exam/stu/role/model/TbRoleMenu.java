package com.exam.stu.role.model;

import java.util.Date;
import java.util.List;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 角色菜单关联 
 * @author WANLITAO
 * 2020-03-21 09:39
 */
public class TbRoleMenu extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5070364510659342604L;
	/** 主键 */
 	private String id;
	/** 角色ID */
 	private String roleId;
	/** 菜单ID */
 	private String menuId;
	/** 按钮ID */
 	private String btnId;
	/** 创建时间 */
 	private Date createTime;
	/** 创建时间 开始时间 */
 	private String createTimeBegin;
    /** 创建时间 结束时间 */
 	private String createTimeEnd;
	
 	/**
 	 * 角色集合
 	 */
 	private List<String> lstRoleId;
 	
	/**
     * 主键
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 主键
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 角色ID
     * @param roleId
     */
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    /**
     * 角色ID
     * @return
     */
	public String getRoleId()
    {
        return roleId;
    }
    
    
	/**
     * 菜单ID
     * @param menuId
     */
    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }
    
    /**
     * 菜单ID
     * @return
     */
	public String getMenuId()
    {
        return menuId;
    }
    
    
	/**
     * 按钮ID
     * @param btnId
     */
    public void setBtnId(String btnId)
    {
        this.btnId = btnId;
    }
    
    /**
     * 按钮ID
     * @return
     */
	public String getBtnId()
    {
        return btnId;
    }
    
    
	/**
     * 创建时间
     * @param createTime
     */
    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreateTime()
    {
        return createTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createTimeBegin
     */
    public void setCreateTimeBegin(String createTimeBegin)
    {
        this.createTimeBegin = createTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreateTimeBegin()
    {
        return createTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createTimeEnd
     */
    public void setCreateTimeEnd(String createTimeEnd)
    {
        this.createTimeEnd = createTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreateTimeEnd()
    {
        return createTimeEnd;
    }

	public List<String> getLstRoleId() {
		return lstRoleId;
	}

	public void setLstRoleId(List<String> lstRoleId) {
		this.lstRoleId = lstRoleId;
	}
    
	
}