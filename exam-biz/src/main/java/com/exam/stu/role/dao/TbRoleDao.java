package com.exam.stu.role.dao;

import com.exam.base.dao.IAbstractDaoImpl; 
import com.exam.stu.role.model.TbRole;

/**
 * 角色表  DAO层
 * 
 * @author  WANLITAO
 * 2020-03-21 09:42
 */
public class TbRoleDao extends IAbstractDaoImpl<TbRole>
{

}