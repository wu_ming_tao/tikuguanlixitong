package com.exam.stu.role.service;

import com.exam.base.service.IAbstractService; 
import com.exam.stu.role.model.TbUserRole;

/**
 * 用户角色表  Service
 * 
 * @author WANLITAO
 * 2020-03-21 09:47
 */
public interface TbUserRoleService extends IAbstractService<TbUserRole>
{

}
