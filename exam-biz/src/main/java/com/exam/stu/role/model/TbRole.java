package com.exam.stu.role.model;

import java.util.Date;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 角色表 
 * @author WANLITAO
 * 2020-03-21 09:39
 */
public class TbRole extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5824530872851952874L;
	/** 主键 */
 	private String id;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 角色名称 */
 	private String roleName;
	/** 角色编码 */
 	private String roleCode;
	
	/**
     * 主键
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 主键
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 角色名称
     * @param roleName
     */
    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }
    
    /**
     * 角色名称
     * @return
     */
	public String getRoleName()
    {
        return roleName;
    }
    
    
	/**
     * 角色编码
     * @param roleCode
     */
    public void setRoleCode(String roleCode)
    {
        this.roleCode = roleCode;
    }
    
    /**
     * 角色编码
     * @return
     */
	public String getRoleCode()
    {
        return roleCode;
    }
    
    
}