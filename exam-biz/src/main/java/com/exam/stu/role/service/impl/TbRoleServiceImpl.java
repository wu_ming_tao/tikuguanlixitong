package com.exam.stu.role.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 



import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.role.dao.TbRoleDao;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.service.TbRoleService;
  

/**
 * 角色表  Service实现
 * 
 * @author  WANLITAO
 * 2020-03-21 09:47
 */
@Service("tbRoleService")
public class TbRoleServiceImpl extends AbstractServiceImpl<TbRole> implements TbRoleService
{

    @Autowired
    private TbRoleDao tbRoleDao;

	@Override
	protected IAbstractDao<TbRole> getBaseDao()
	{
		return tbRoleDao;
	}
}