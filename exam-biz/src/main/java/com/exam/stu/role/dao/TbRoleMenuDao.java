package com.exam.stu.role.dao;

import com.exam.base.dao.IAbstractDaoImpl; 
import com.exam.stu.role.model.TbRoleMenu;

/**
 * 角色菜单关联  DAO层
 * 
 * @author  WANLITAO
 * 2020-03-21 09:42
 */
public class TbRoleMenuDao extends IAbstractDaoImpl<TbRoleMenu>
{

}