package com.exam.stu.power.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.power.model.TbMenu;

public class TbMenuDao extends IAbstractDaoImpl<TbMenu> {
	
	public boolean deleteBtnAuth(String id)
	{
		return getSqlMapClientTemplate().delete(namespace.concat("deletebtnAuthByMenuKey"), id) == 1;
	}
	
	public boolean deleteAuth(String id)
	{
		return getSqlMapClientTemplate().delete(namespace.concat("deleteAuthByMenuKey"), id) == 1;
	}
	
	public boolean deleteBtn(String id)
	{
		return getSqlMapClientTemplate().delete(namespace.concat("deleteBtnByMenuKey"), id) == 1;
	}
	
	public TbMenu getRootMenu()
	{
		return getSqlMapClientTemplate().selectOne(namespace.concat("selectRootMenu"));
	}
}
