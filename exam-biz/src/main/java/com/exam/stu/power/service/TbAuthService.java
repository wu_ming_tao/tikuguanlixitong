package com.exam.stu.power.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.power.model.TbAuth;
import com.exam.stu.user.model.TbUser;

public interface TbAuthService extends IAbstractService<TbAuth> {

}
