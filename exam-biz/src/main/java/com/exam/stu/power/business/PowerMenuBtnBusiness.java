package com.exam.stu.power.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.service.IAbstractService;
import com.exam.stu.power.model.TbBtn; 
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.power.service.TbBtnService;
import com.exam.stu.power.service.TbMenuService;

@Service("powerMenuBtnBusiness")
public class PowerMenuBtnBusiness extends AbstractBusiness<TbBtn>{

	@Autowired
	private TbBtnService tbBtnService;
	
	@Autowired
	private TbMenuService tbMenuService;
	
	@Override
	protected IAbstractService<TbBtn> getBaseService() { 
		return tbBtnService;
	}

	/**
	 * 添加按钮
	 * @param queryModel
	 * @throws Exception 
	 */
	public void addBtn(TbBtn addBtn) throws Exception {
		 
		TbBtn queryBtn = new TbBtn();
		queryBtn.setBtnCode(addBtn.getBtnCode());
		
		int count = getBaseService().queryCount(queryBtn);
		
		if (count > 0)
		{
			throw new Exception("按钮编码重复！");
		}
		
		getBaseService().add(addBtn);
	}
	
	/**
	 * 删除按钮
	 * @param menuId
	 */
	public void deleteBtn(String btnId)
	{
		tbMenuService.deleteBtnAuth(btnId);
		tbMenuService.deleteAuth(btnId); 
		getBaseService().delete(btnId);
	}

	/**
	 * 更新按钮
	 * @param upBtn
	 * @throws Exception
	 */
	public void updateBtn(TbBtn upBtn) throws Exception {
		
		TbBtn queryBtn = new TbBtn();
		queryBtn.setBtnCode(upBtn.getBtnCode());
		
		List<TbBtn> lstBtn = getBaseService().queryList(queryBtn);
		
		for (TbBtn tbBtn : lstBtn) {
			if (!tbBtn.getId().equals(upBtn.getId()))
			{
				throw new Exception("按钮编码重复！");
			}
		} 
		getBaseService().update(upBtn);
		
	}

}
