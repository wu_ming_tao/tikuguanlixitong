package com.exam.stu.power.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.service.IAbstractService;
import com.exam.stu.power.model.TbAuth;
import com.exam.stu.power.service.TbAuthService;

@Service("powerAttachBussiness")
public class PowerAttachBussiness extends AbstractBusiness<TbAuth>{

	@Autowired
	private TbAuthService tbAuthService;
	
	@Override
	protected IAbstractService<TbAuth> getBaseService() {
		return tbAuthService;
	}

}
