package com.exam.stu.power.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.power.dao.TbBtnDao;
import com.exam.stu.power.model.TbBtn;
import com.exam.stu.power.service.TbBtnService;

@Service("tbBtnService")
public class TbBtnServiceImpl extends AbstractServiceImpl<TbBtn> implements TbBtnService {

	@Autowired
	private TbBtnDao tbBtnDao;
	
	@Override
	protected IAbstractDao<TbBtn> getBaseDao() { 
		return tbBtnDao;
	}

}
