package com.exam.stu.power.model.vo;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

public abstract class BaseTreePropertyModel extends AbstractModel implements IAbstractModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5706199435486849308L;
	/**
	 * 是否选中
	 */
	private boolean checked;
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	
}
