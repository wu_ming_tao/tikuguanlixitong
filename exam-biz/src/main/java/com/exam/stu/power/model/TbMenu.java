package com.exam.stu.power.model;

import java.util.Date;
import java.util.List;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;
import com.exam.stu.power.model.vo.BaseTreePropertyModel;

/**
 * 菜单表 
 * @author WANLITAO
 * 2020-03-09 20:22
 */
public class TbMenu extends BaseTreePropertyModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4836705962012290686L;
	/** 主键 */
 	private String id;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 菜单编码 */
 	private String menuCode;
	/** 菜单路径 */
 	private String menuUrl;
	/** 父级编码 */
 	private String parentId;
	/** 排序字段 */
 	private Integer showOrder;
	/** 显示标识 */
 	private Integer display;
	/** 菜单名称 */
 	private String menuName;
	/** 菜单图标 */
 	private String menuIcon;
	
	private String sortName = "show_order";
 	
 	/**
 	 * 子菜单
 	 */
 	private List<TbMenu> lstChildMenu;
 	
 	/**
 	 * 按钮
 	 */
 	private List<TbBtn> lstBtn;
 	
	/**
     * 主键
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 主键
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 菜单编码
     * @param menuCode
     */
    public void setMenuCode(String menuCode)
    {
        this.menuCode = menuCode;
    }
    
    /**
     * 菜单编码
     * @return
     */
	public String getMenuCode()
    {
        return menuCode;
    }
    
    
	/**
     * 菜单路径
     * @param menuUrl
     */
    public void setMenuUrl(String menuUrl)
    {
        this.menuUrl = menuUrl;
    }
    
    /**
     * 菜单路径
     * @return
     */
	public String getMenuUrl()
    {
        return menuUrl;
    }
    
    
	/**
     * 父级编码
     * @param parentId
     */
    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }
    
    /**
     * 父级编码
     * @return
     */
	public String getParentId()
    {
        return parentId;
    }
    
    
	/**
     * 排序字段
     * @param showOrder
     */
    public void setShowOrder(Integer showOrder)
    {
        this.showOrder = showOrder;
    }
    
    /**
     * 排序字段
     * @return
     */
	public Integer getShowOrder()
    {
        return showOrder;
    }
    
    
	/**
     * 显示标识
     * @param display
     */
    public void setDisplay(Integer display)
    {
        this.display = display;
    }
    
    /**
     * 显示标识
     * @return
     */
	public Integer getDisplay()
    {
        return display;
    }
    
    
	/**
     * 菜单名称
     * @param menuName
     */
    public void setMenuName(String menuName)
    {
        this.menuName = menuName;
    }
    
    /**
     * 菜单名称
     * @return
     */
	public String getMenuName()
    {
        return menuName;
    }
    
    
	/**
     * 菜单图标
     * @param menuIcon
     */
    public void setMenuIcon(String menuIcon)
    {
        this.menuIcon = menuIcon;
    }
    
    /**
     * 菜单图标
     * @return
     */
	public String getMenuIcon()
    {
        return menuIcon;
    }

	public List<TbMenu> getLstChildMenu() {
		return lstChildMenu;
	}

	public void setLstChildMenu(List<TbMenu> lstChildMenu) {
		this.lstChildMenu = lstChildMenu;
	}

	public List<TbBtn> getLstBtn() {
		return lstBtn;
	}

	public void setLstBtn(List<TbBtn> lstBtn) {
		this.lstBtn = lstBtn;
	}
    
    
}