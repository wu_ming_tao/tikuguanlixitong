package com.exam.stu.power.model.vo;


/**
 * 交互对象
 * @author Administrator
 *
 */
public class RolePowerDataVo {
	
	/**
	 * 角色编号
	 */
	private String roleId;
	
	/**
	 * 树数据
	 */
	private ZkTreeNode[] treeData;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public ZkTreeNode[] getTreeData() {
		return treeData;
	}

	public void setTreeData(ZkTreeNode[] treeData) {
		this.treeData = treeData;
	}

	 
	
	
}
