package com.exam.stu.power.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.power.model.TbAuth;
import com.exam.stu.power.model.TbMenu;

public interface TbMenuService extends IAbstractService<TbMenu> {
	boolean deleteBtnAuth(String id);
	boolean deleteAuth(String id);
	boolean deleteBtn(String id);
    TbMenu getRootMenu();
}
