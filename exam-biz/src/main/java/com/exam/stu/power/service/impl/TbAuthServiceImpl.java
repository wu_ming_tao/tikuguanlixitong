package com.exam.stu.power.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.power.dao.TbAuthDao;
import com.exam.stu.power.model.TbAuth;
import com.exam.stu.power.service.TbAuthService;

@Service("tbAuthService")
public class TbAuthServiceImpl extends AbstractServiceImpl<TbAuth> implements TbAuthService {

	@Autowired
	private TbAuthDao tbAuthDao;
	
	@Override
	protected IAbstractDao<TbAuth> getBaseDao() {
 
		return tbAuthDao;
	}

	

}
