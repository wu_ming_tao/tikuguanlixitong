package com.exam.stu.power.model;

import java.util.Date;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;
import com.exam.stu.power.model.vo.BaseTreePropertyModel;

/**
 * 菜单按钮 
 * @author WANLITAO
 * 2020-03-09 20:22
 */
public class TbBtn extends BaseTreePropertyModel
{
	/** 主键 */
 	private String id;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 按钮编码 */
 	private String btnCode;
	/** 按钮名称 */
 	private String btnName;
	/** 按钮地址 */
 	private String btnUrl;
	/** 响应函数 */
 	private String btnFunc;
	/** 菜单编码 */
 	private String menuId;
	/** 按钮类型 */
 	private Integer btnType;
	/** 按钮样式 */
 	private String btnClass;
	/** 按钮展示 */
 	private Integer btnDisplay;
	/** 排序字段 */
 	private Integer showOrder;
	
	/**
     * 主键
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 主键
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 按钮编码
     * @param btnCode
     */
    public void setBtnCode(String btnCode)
    {
        this.btnCode = btnCode;
    }
    
    /**
     * 按钮编码
     * @return
     */
	public String getBtnCode()
    {
        return btnCode;
    }
    
    
	/**
     * 按钮名称
     * @param btnName
     */
    public void setBtnName(String btnName)
    {
        this.btnName = btnName;
    }
    
    /**
     * 按钮名称
     * @return
     */
	public String getBtnName()
    {
        return btnName;
    }
    
    
	/**
     * 按钮地址
     * @param btnUrl
     */
    public void setBtnUrl(String btnUrl)
    {
        this.btnUrl = btnUrl;
    }
    
    /**
     * 按钮地址
     * @return
     */
	public String getBtnUrl()
    {
        return btnUrl;
    }
    
    
	/**
     * 响应函数
     * @param btnFunc
     */
    public void setBtnFunc(String btnFunc)
    {
        this.btnFunc = btnFunc;
    }
    
    /**
     * 响应函数
     * @return
     */
	public String getBtnFunc()
    {
        return btnFunc;
    }
    
    
	/**
     * 菜单编码
     * @param menuId
     */
    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }
    
    /**
     * 菜单编码
     * @return
     */
	public String getMenuId()
    {
        return menuId;
    }
    
    
	/**
     * 按钮类型
     * @param btnType
     */
    public void setBtnType(Integer btnType)
    {
        this.btnType = btnType;
    }
    
    /**
     * 按钮类型
     * @return
     */
	public Integer getBtnType()
    {
        return btnType;
    }
    
    
	/**
     * 按钮样式
     * @param btnClass
     */
    public void setBtnClass(String btnClass)
    {
        this.btnClass = btnClass;
    }
    
    /**
     * 按钮样式
     * @return
     */
	public String getBtnClass()
    {
        return btnClass;
    }
    
    
	/**
     * 按钮展示
     * @param btnDisplay
     */
    public void setBtnDisplay(Integer btnDisplay)
    {
        this.btnDisplay = btnDisplay;
    }
    
    /**
     * 按钮展示
     * @return
     */
	public Integer getBtnDisplay()
    {
        return btnDisplay;
    }
    
    
	/**
     * 排序字段
     * @param showOrder
     */
    public void setShowOrder(Integer showOrder)
    {
        this.showOrder = showOrder;
    }
    
    /**
     * 排序字段
     * @return
     */
	public Integer getShowOrder()
    {
        return showOrder;
    }
    
    
}