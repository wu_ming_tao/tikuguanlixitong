package com.exam.stu.power.model.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 树结构
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public class ZkTreeNode
{
    /**
     * 编号
     */
    private String id;
    /**
     * 节点编码
     */
    private String code;
    /**
     * 展示内容
     */
    private String text;
    /**
     * 选中项
     */
    private Boolean checked = false;
    
    /**
     * 父节点
     */
    private String parentId;
    
    /**
     * 类型
     */
    private String type;
    
    /**
     * 子节点
     */
    private List<ZkTreeNode> children = new ArrayList<ZkTreeNode>();
    /**
     * 当前收缩状态
     */
    private String state = "open"; 

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Boolean getChecked()
    {
        return checked;
    }

    public void setChecked(Boolean checked)
    {
        this.checked = checked;
    }

    public List<ZkTreeNode> getChildren()
    {
        return children;
    }

    public void setChildren(List<ZkTreeNode> children)
    {
        this.children = children;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }
 
    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
    public boolean equals(Object obj)
    {
        boolean flag = false;
        if (this.getText().equals(obj))
        {
            flag =  true;
        }
        return flag;
    }
}