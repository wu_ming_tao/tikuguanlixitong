package com.exam.stu.power.model;

import java.util.Date;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 权限附属表 
 * @author WANLITAO
 * 2020-03-09 20:22
 */
public class TbAuth extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8128539354505995046L;
	/** 主键 */
 	private String id;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 权限地址 */
 	private String authUrl;
	/** 关联编号 */
 	private String mainId;
	/** 附属名称 */
 	private String authName;
	/** 附属类型 */
 	private String authType;
	
	/**
     * 主键
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 主键
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
	/**
     * 权限地址
     * @param authUrl
     */
    public void setAuthUrl(String authUrl)
    {
        this.authUrl = authUrl;
    }
    
    /**
     * 权限地址
     * @return
     */
	public String getAuthUrl()
    {
        return authUrl;
    }
    
    
	/**
     * 关联编号
     * @param mainId
     */
    public void setMainId(String mainId)
    {
        this.mainId = mainId;
    }
    
    /**
     * 关联编号
     * @return
     */
	public String getMainId()
    {
        return mainId;
    }
    
    
	/**
     * 附属名称
     * @param authName
     */
    public void setAuthName(String authName)
    {
        this.authName = authName;
    }
    
    /**
     * 附属名称
     * @return
     */
	public String getAuthName()
    {
        return authName;
    }
    
    
	/**
     * 附属类型
     * @param authType
     */
    public void setAuthType(String authType)
    {
        this.authType = authType;
    }
    
    /**
     * 附属类型
     * @return
     */
	public String getAuthType()
    {
        return authType;
    }
    
    
}