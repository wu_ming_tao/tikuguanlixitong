package com.exam.stu.power.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.power.dao.TbMenuDao;
import com.exam.stu.power.model.TbBtn;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.power.service.TbBtnService;
import com.exam.stu.power.service.TbMenuService;

@Service("tbMenuService")
public class TbMenuServiceImpl extends AbstractServiceImpl<TbMenu> implements TbMenuService {

	@Autowired
	private TbMenuDao tbMenuDao;
	
	@Override
	protected IAbstractDao<TbMenu> getBaseDao() { 
		return tbMenuDao;
	}

	public boolean deleteBtnAuth(String id) { 
		return tbMenuDao.deleteBtnAuth(id);
	}

	public boolean deleteAuth(String id) {
		 
		return tbMenuDao.deleteAuth(id);
	}

	public boolean deleteBtn(String id) { 
		return tbMenuDao.deleteBtn(id);
	}

	public TbMenu getRootMenu() { 
		return tbMenuDao.getRootMenu();
	}

}
