package com.exam.stu.common.util;

import java.util.List;

/**
 * 集合工具
 * @author Administrator
 *
 */
public class CollectionUtil {
	
	private CollectionUtil(){
		
	}
	
	/**
	 * 判空
	 * @param lst
	 * @return
	 */
	public static boolean isEmpty(List lst){
		if (lst == null){
			return true;
		}
		return lst.size() == 0;
	}
}
