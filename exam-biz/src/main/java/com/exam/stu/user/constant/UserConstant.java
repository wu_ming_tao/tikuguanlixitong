package com.exam.stu.user.constant;

public interface UserConstant {
	/**
	 * 登陆用户
	 */
	String LOGIN_USER = "loginUser";
	
	/**
	 * 菜单数据
	 */
	String MENU_DATA = "menuData";
	
	/**
	 * 根目录
	 */
	String ROOT_MENU = "rootMenu";
}
