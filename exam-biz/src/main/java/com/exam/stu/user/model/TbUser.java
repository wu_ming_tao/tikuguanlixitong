package com.exam.stu.user.model;

import java.util.Date;

import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;
 

/**
 * 用户表 
 * @author WANLITAO
 * 2020-03-07 13:25
 */
public class TbUser extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2732601135543863718L;
	/** 用户ID */
 	private String id;
	/** 账号 */
 	private String account;
	/** 密码 */
 	private String password;
	/** 名称 */
 	private String userName;
	/** 性别 */
 	private String sex;
	/** 出生年月 */
 	private Date birth;
	/** 出生年月 开始时间 */
 	private String birthBegin;
    /** 出生年月 结束时间 */
 	private String birthEnd;
	/** 手机号码 */
 	private String telephone;
	/** 地址 */
 	private String address;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	/** 邮箱 */
 	private String mailbox;
	/** QQ */
 	private String qq;
 	/**
 	 * 角色数组
 	 */
 	private UserRole[] roleArrays;
 	
 	/**
 	 * 角色编号
 	 */
 	private String roleId;
 	
 	
 	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
     * 用户ID
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 用户ID
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 账号
     * @param account
     */
    public void setAccount(String account)
    {
        this.account = account;
    }
    
    /**
     * 账号
     * @return
     */
	public String getAccount()
    {
        return account;
    }
    
    
	/**
     * 密码
     * @param password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    /**
     * 密码
     * @return
     */
	public String getPassword()
    {
        return password;
    }
    
    
	/**
     * 名称
     * @param userName
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    /**
     * 名称
     * @return
     */
	public String getUserName()
    {
        return userName;
    }
    
    
	/**
     * 性别
     * @param sex
     */
    public void setSex(String sex)
    {
        this.sex = sex;
    }
    
    /**
     * 性别
     * @return
     */
	public String getSex()
    {
        return sex;
    }
    
    
	/**
     * 出生年月
     * @param birth
     */
    public void setBirth(Date birth)
    {
        this.birth = birth;
    }
    
    /**
     * 出生年月
     * @return
     */
	public Date getBirth()
    {
        return birth;
    }
    
    
    /**
     * 出生年月 开始时间
     * @param birthBegin
     */
    public void setBirthBegin(String birthBegin)
    {
        this.birthBegin = birthBegin;
    }
    
 	/**
     * 出生年月 开始时间
     * @return
     */
	public String getBirthBegin()
    {
        return birthBegin;
    }
    
    /**
     * 出生年月 结束时间
     * @param birthEnd
     */
    public void setBirthEnd(String birthEnd)
    {
        this.birthEnd = birthEnd;
    }
 	
 	/**
     * 出生年月 结束时间
     * @return
     */
	public String getBirthEnd()
    {
        return birthEnd;
    }
    
	/**
     * 手机号码
     * @param telephone
     */
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }
    
    /**
     * 手机号码
     * @return
     */
	public String getTelephone()
    {
        return telephone;
    }
    
    
	/**
     * 地址
     * @param address
     */
    public void setAddress(String address)
    {
        this.address = address;
    }
    
    /**
     * 地址
     * @return
     */
	public String getAddress()
    {
        return address;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }

	public UserRole[] getRoleArrays() {
		return roleArrays;
	}

	public void setRoleArrays(UserRole[] roleArrays) {
		this.roleArrays = roleArrays;
	}
    
    /**
     * 邮箱
     * @return
     */
	public String getMailbox() {
		return mailbox;
	}
	/**
     * 邮箱
     * @param mailbox
     */
	public void setMailbox(String mailbox) {
		this.mailbox = mailbox;
	}
    /**
     * QQ
     * @return
     */
	public String getQq() {
		return qq;
	}
	/**
     * QQ
     * @param qq
     */
	public void setQq(String qq) {
		this.qq = qq;
	}
    
}