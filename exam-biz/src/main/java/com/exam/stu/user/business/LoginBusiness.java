package com.exam.stu.user.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.model.ResponseModel;
import com.exam.base.service.IAbstractService;
import com.exam.stu.common.util.CollectionUtil;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.role.model.TbUserRole;
import com.exam.stu.role.service.TbUserRoleService;
import com.exam.stu.user.constant.UserConstant;
import com.exam.stu.user.model.TbUser;
import com.exam.stu.user.service.UserService;

/**
 * 登录相关业务
 * @author Administrator
 *
 */
@Service("loginBusiness")
public class LoginBusiness extends AbstractBusiness<TbUser>{
	
	@Autowired
	private UserService userService;
	@Autowired
	private TbUserRoleService tbUserRoleService;
	/**
	 * 登录访问
	 * @param userName
	 * @param passWord
	 * @return
	 */
	@Override
	protected IAbstractService<TbUser> getBaseService() { 
		return userService;
	}
	public ResponseModel doLogin(String account,String passWord)
	{
		ResponseModel resModel = new ResponseModel();
		// 查询用户
		TbUser queryUser = new TbUser();
		queryUser.setAccount(account);
		List<TbUser> lstUser = userService.queryList(queryUser);
		
		if (CollectionUtil.isEmpty(lstUser))
		{
			resModel.setRequestFlag(false);
			resModel.setMessage("账号不存在！");
			return resModel;
		}
		TbUser curUser = lstUser.get(0);
		if (!curUser.getPassword().equals(passWord))
		{
			resModel.setRequestFlag(false);
			resModel.setMessage("密码错误！");
			return resModel;
		}  
		resModel.setRequestFlag(true); 
		resModel.setData(curUser);
		return resModel;
	}
	
	/**
	 * 注册
	 * @param account
	 * @param passWord
	 * @return
	 */
	public ResponseModel doRegister(String account,String passWord)
	{
		ResponseModel resModel = new ResponseModel();
		// 判断登录账号
		// 查询用户
		TbUser queryUser = new TbUser(); 
		queryUser.setAccount(account);
		List<TbUser> lstUser = userService.queryList(queryUser);
		if (!CollectionUtil.isEmpty(lstUser))
		{
			resModel.setRequestFlag(false);
			resModel.setMessage("账号已经存在！");
			return resModel;
		}
		
		// 创建实体
		TbUser addUser = new TbUser();
		addUser.setAccount(account);
		addUser.setUserName(account);
		addUser.setPassword(passWord);		
		userService.add(addUser);
		TbUser addUsers = new TbUser();
		List<TbUser> list=userService.queryList(addUsers);
		for(TbUser user:list) {
			TbUserRole userRole = new TbUserRole();
			userRole.setUserId(user.getId());
			int count = tbUserRoleService.queryCount(userRole);
			if(count==0){
				userRole.setRoleId("3");
				tbUserRoleService.add(userRole);
			}
		}		
		resModel.setRequestFlag(true);
		return resModel;
	}
	
	/**
	 * 获取用户对象
	 * @param request
	 * @return
	 */
	public Map<String,Object> getUserMap(HttpServletRequest request)
	{
		HttpSession session = request.getSession(); 
		TbUser tbUser = (TbUser) session.getAttribute(UserConstant.LOGIN_USER); 
		Map<String,Object> uerMap = new HashMap<String, Object>();
		uerMap.put(UserConstant.LOGIN_USER, tbUser);
		return uerMap;
	}

	
}
