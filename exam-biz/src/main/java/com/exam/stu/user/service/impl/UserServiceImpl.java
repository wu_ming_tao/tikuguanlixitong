package com.exam.stu.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.user.dao.UserDao;
import com.exam.stu.user.model.TbUser;
import com.exam.stu.user.service.UserService;

@Service("userService")
public class UserServiceImpl extends AbstractServiceImpl<TbUser> implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	protected IAbstractDao<TbUser> getBaseDao() { 
		return userDao;
	}

}
