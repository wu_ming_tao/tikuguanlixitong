package com.exam.stu.user.service;

import com.exam.base.service.IAbstractService;
import com.exam.stu.user.model.TbUser;

public interface UserService extends IAbstractService<TbUser> {

}
