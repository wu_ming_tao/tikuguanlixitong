package com.exam.stu.user.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.user.model.TbUser;

/**
 * 用户表  DAO层
 * 
 * @author  WANLITAO
 * 2020-03-25 18:18
 */
public class UserDao extends IAbstractDaoImpl<TbUser>
{

}