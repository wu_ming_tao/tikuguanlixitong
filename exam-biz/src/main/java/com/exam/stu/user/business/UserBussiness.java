package com.exam.stu.user.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.exam.base.business.AbstractBusiness;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.service.IAbstractService;
import com.exam.base.util.ServletBeanTools;
import com.exam.base.util.UUIdUtil;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.model.TbUserRole;
import com.exam.stu.role.service.TbRoleService;
import com.exam.stu.role.service.TbUserRoleService;
import com.exam.stu.user.model.TbUser;
import com.exam.stu.user.model.UserRole;
import com.exam.stu.user.service.UserService;

@Service("userBussiness")
public class UserBussiness  extends AbstractBusiness<TbUser> {

	@Autowired
	private UserService userService;
	@Autowired
	private TbUserRoleService tbUserRoleService;
	@Autowired
	private TbRoleService tbRoleService;
	
	
	@Override
	protected IAbstractService<TbUser> getBaseService() { 
		return userService;
	}
	/**
	 * 获取用户表格数据
	 * @param request
	 * @return
	 */
	public ResponsePageModel<TbUser> getUserPageData(HttpServletRequest request) {
		try {

			TbUser queryModel = ServletBeanTools
					.populate(TbUser.class, request); 
			return this.getPageData(queryModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 获取用户信息
	 * @param userId
	 * @return
	 */
	public TbUser findByUserId(String userId)
	{
		TbUser user = this.findById(userId);
		
		TbUserRole queryModel = new TbUserRole();
		queryModel.setUserId(userId);
		List<TbUserRole> lstuseRole = tbUserRoleService.queryList(queryModel);
		 
		Set<String> checkRoleId = new HashSet<String>();
		for (TbUserRole tbUserRole : lstuseRole) {
			checkRoleId.add(tbUserRole.getRoleId());
		}
		
		List<TbRole> lstRole = tbRoleService.queryList(new TbRole());
		
		UserRole[] userArray = new UserRole[lstRole.size()];
		int index = 0;
		for (TbRole tbRole : lstRole) {
			UserRole userRole = new UserRole();
			userRole.setRoleId(tbRole.getId());
			userRole.setRoleName(tbRole.getRoleName());
			if (checkRoleId.contains(tbRole.getId()))
			{
				userRole.setChecked(true);
			}
			
			userArray[index] = userRole;
			index ++;
		} 
		user.setRoleArrays(userArray);
		
		return user;
		
	}
	
	/**
	 * 组装用户
	 * @param user
	 * @return
	 */
	public TbUser generateUser(TbUser user) {
		List<TbRole> lstRole = tbRoleService.queryList(new TbRole());

		UserRole[] userArray = new UserRole[lstRole.size()];
		int index = 0;
		for (TbRole tbRole : lstRole) {
			UserRole userRole = new UserRole();
			userRole.setRoleId(tbRole.getId());
			userRole.setRoleName(tbRole.getRoleName());

			userArray[index] = userRole;
			index++;
		}
		user.setRoleArrays(userArray);

		return user;
	}
	/**
	 * 添加用户
	 * @param queryModel
	 * @throws Exception 
	 */
	public void addUser(TbUser queryModel) throws Exception {
		
		TbUser queryUser = new TbUser();
		queryUser.setAccount(queryModel.getAccount());
		
		int count = userService.queryCount(queryUser);
		
		if (count > 1)
		{
			throw new Exception("登录账号重复!");
		}
  
		if (StringUtils.isEmpty(queryModel.getRoleId()))
		{
			return;
		}
		
		queryModel.setId(UUIdUtil.getUUID());
		
		String[] roleArrays = queryModel.getRoleId().split(",");
		
		List<TbUserRole> lstUserRole = new ArrayList<TbUserRole>();
		TbUserRole temAdd = null;
		
		for (String role : roleArrays) {
			temAdd = new TbUserRole();
			temAdd.setRoleId(role);
			temAdd.setUserId(queryModel.getId());
			lstUserRole.add(temAdd);
		}
		
		tbUserRoleService.batchAdd(lstUserRole);
		
		userService.add(queryModel);
	}
	
	/**
	 * 添加用户
	 * @param queryModel
	 * @throws Exception 
	 */
	public void updateUser(TbUser queryModel) throws Exception {
		
		TbUser queryUser = new TbUser();
		queryUser.setAccount(queryModel.getAccount());
		
		List<TbUser> lstUser = userService.queryList(queryUser);
		
		for (TbUser tbUser : lstUser) {
			if (queryModel.getAccount().equals(tbUser.getAccount())
					&& !queryModel.getId().equals(tbUser.getId()))
			{
				throw new Exception("登录账号重复!");
			}
		}
		
        TbUserRole deleteModel = new TbUserRole();
        deleteModel.setUserId(queryModel.getId());
        tbUserRoleService.delete(deleteModel);
        
		if (StringUtils.isEmpty(queryModel.getRoleId()))
		{
			return;
		} 
		
		String[] roleArrays = queryModel.getRoleId().split(",");
		
		List<TbUserRole> lstUserRole = new ArrayList<TbUserRole>();
		TbUserRole temAdd = null;
		
		for (String role : roleArrays) {
			temAdd = new TbUserRole();
			temAdd.setRoleId(role);
			temAdd.setUserId(queryModel.getId());
			lstUserRole.add(temAdd);
		}
		
		tbUserRoleService.batchAdd(lstUserRole);
		
		userService.update(queryModel);
	}
	/**
	 * 编辑用户信息
	 * @param queryModel
	 * @throws Exception 
	 */
	public void editUser(TbUser queryModel) throws Exception {
		
		//TbUser queryUser = new TbUser();
		//queryUser.setId(queryModel.getId());		
		//List<TbUser> lstUser = userService.queryList(queryUser);
		userService.update(queryModel);
	
	}
}
