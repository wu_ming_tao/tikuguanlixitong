package com.exam.stu.topic.dao;

import com.exam.base.dao.IAbstractDaoImpl;
import com.exam.stu.topic.model.TbTopic;
/**
 * 试题表  DAO层
 * 
 * @author  WANLITAO
 * 2020-03-28 16:52
 */
public class TbTopicDao extends IAbstractDaoImpl<TbTopic>
{

}