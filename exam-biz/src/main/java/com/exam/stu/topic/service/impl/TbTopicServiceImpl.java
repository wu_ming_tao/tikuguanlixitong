package com.exam.stu.topic.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
import com.exam.stu.topic.dao.TbTopicDao;
import com.exam.stu.topic.model.TbTopic;
import com.exam.stu.topic.service.TbTopicService;
/**
 * 试题表  Service实现
 * 
 * @author  WANLITAO
 * 2020-03-28 16:52
 */
@Service("tbTopicService")
public class  TbTopicServiceImpl extends AbstractServiceImpl<TbTopic> implements TbTopicService{

    @Autowired
    private TbTopicDao tbTopicDao;

	@Override
	protected IAbstractDao<TbTopic> getBaseDao()
	{
		return tbTopicDao;
	}

}