package com.exam.stu.topic.business;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import com.exam.base.business.AbstractBusiness;
import com.exam.base.model.PageModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.service.IAbstractService;
import com.exam.base.util.ServletBeanTools;

import com.exam.stu.topic.model.TbTopic;
import com.exam.stu.topic.service.TbTopicService;

/**
 * 试题菜单业务管理
 * @author Administrator
 *
 */
@Service("topicBussiness")
public class TopicBussiness  extends AbstractBusiness<TbTopic> {

	@Autowired
	private TbTopicService tbTopicService;
	
	
	@Override
	protected IAbstractService<TbTopic> getBaseService() { 
		return tbTopicService;
	}
	
	/**
	 * 获取试题表数据
	 * @param request
	 * @return
	 */
	public ResponsePageModel<TbTopic> getTopicPageData(HttpServletRequest request) {
		try {

			TbTopic queryModel = ServletBeanTools
					.populate(TbTopic.class, request); 
			return this.getPageData(queryModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	/**
	 * 分页查询数据
	 * @param content
	 * @return
	 */
	public ResponsePageModel<TbTopic> getQueryPageData (String content)
	{
		try {
			ResponsePageModel<TbTopic> dataModel = new ResponsePageModel<TbTopic>(); 
			TbTopic queryModel = new TbTopic();
			int dataCount = this.countByQuery(content);
			if (dataCount <= 0)
			{
				dataModel.setHasData(false);
				return  dataModel;
			}
			queryModel.setBeginNum((queryModel.getPageOffset() - 1) * queryModel.getPageSize());
			List<TbTopic> lstData = this.queryPageBy(content);
			// 封装分页信息
			PageModel pageModel = new PageModel();
			pageModel.setPageNum(queryModel.getPageOffset());
			pageModel.setPageSize(queryModel.getPageSize());
			pageModel.setTotal(dataCount); 
			pageModel.calPageTotal();
			
			dataModel.setPageModel(pageModel);
			dataModel.setQueryModel(queryModel);
			dataModel.setResultData(lstData);
			return dataModel; 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}

	/**
	 * 添加试题
	 * @param addModel
	 * @throws Exception
	 */
	public void addTopic(TbTopic addModel) throws Exception { 
		TbTopic queryModel = new TbTopic();

		tbTopicService.add(addModel);
	}
    
	/**
	 * 更新试题
	 * @param updateRole
	 * @throws Exception
	 */
	public void updateTopic(TbTopic updateTopic) throws Exception {
		TbTopic queryModel = new TbTopic();

		tbTopicService.update(updateTopic);
	}
	/**
	 * 试题类型格式化
	 * @param userId
	 * @return
	 */
	public String typeOf(String type) {
		String types;
		if("1".equals(type)) {
			types ="选择题(单)";	
			return types;
		}else if("2".equals(type)) {
			types ="判断题"; 
			return types;
		}else if("3".equals(type)) {
			types ="问答题"; 
			return types;
		}
		return null;
	}
	/**
	 * 难度格式化
	 * @param userId
	 * @return
	 */
	public String gradeOf(String grade) {
		String grades;
		if("1".equals(grade)) {
			grades ="低";	
			return grades;
		}else if("2".equals(grade)) {
			grades ="一般"; 
			return grades;
		}else if("3".equals(grade)) {
			grades ="高"; 
			return grades;
		}
		return null;
	}
	/**
	 * 内容和答案的缩减格式化
	 * @param userId
	 * @return
	 */
	public ResponsePageModel<TbTopic> pageModelOf(ResponsePageModel<TbTopic> pageModel ) {
		List<TbTopic> listData = pageModel.getResultData();
		if(listData.size()>0) {
		for(TbTopic topic : listData) {
			if(topic.getContent()!=null) {
		if(topic.getContent().length()>40)
			topic.setContent(topic.getContent().substring(0, 37).concat("..."));
			}
			if(topic.getResult()!=null) {
		if(topic.getResult().length()>40)
			topic.setSaveResult(topic.getResult().substring(0, 37).concat("..."));
		}
		}
		}
		return pageModel;
	}
	/**
	 * 查询试题
	 * @param updateRole
	 * @throws Exception
	 */
	//public List<TbTopic>  queryTopic(String query) throws Exception {}



	
}
