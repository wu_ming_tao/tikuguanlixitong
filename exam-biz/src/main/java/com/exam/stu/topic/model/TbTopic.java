package com.exam.stu.topic.model;


import java.util.Date;
 
 
import com.exam.base.model.AbstractModel;
import com.exam.base.model.IAbstractModel;

/**
 * 试题表 
 * @author WANLITAO
 * 2020-03-28 16:52
 */
public class TbTopic extends AbstractModel implements IAbstractModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8212305315937791064L;
	/** 试题编号 */
 	private String id;
	/** 创建人ID */
 	private String userId;
	/** 试卷难度等级 */
 	private String grade;
	/** 题类 */
 	private String category;
	/** 题型 */
 	private String type;
	/** 内容 */
 	private String content;
	/** 选项一 */
 	private String optionA;
	/** 选项二 */
 	private String optionB;
	/** 选项三 */
 	private String optionC;
	/** 选项四 */
 	private String optionD;
	/** 答案 */
 	private String result;
	/** 创建人 */
 	private String createdBy;
	/** 创建时间 */
 	private Date createdTime;
	/** 创建时间 开始时间 */
 	private String createdTimeBegin;
    /** 创建时间 结束时间 */
 	private String createdTimeEnd;
	/** 更新人 */
 	private String updatedBy;
	/** 更新时间 */
 	private Date updatedTime;
	/** 更新时间 开始时间 */
 	private String updatedTimeBegin;
    /** 更新时间 结束时间 */
 	private String updatedTimeEnd;
	
	/**
     * 试题编号
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }
    
    /**
     * 试题编号
     * @return
     */
	public String getId()
    {
        return id;
    }
    
    
	/**
     * 创建人ID
     * @param userId
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
    /**
     * 创建人ID
     * @return
     */
	public String getUserId()
    {
        return userId;
    }
    
    
	/**
     * 试卷难度等级
     * @param grade
     */
    public void setGrade(String grade)
    {
        this.grade = grade;
    }
    /**
     * 试卷难度等级
     * @param grade
     */
    public void setGradeUpdata(String grade)
    {
    	if("低".equals(grade)) {
    		this.grade = "low"; 
    	}else if("一般".equals(grade)){
    		this.grade = "general";
    	}else if("高".equals(grade)){
    		this.grade = "high";
    	}else {
            this.grade = grade;
    	}
    }
    
    
    /**
     * 试卷难度等级
     * @return
     */
	public String getGrade()
    {
        return grade;
    }
    
    
	/**
     * 题类
     * @param category
     */
    public void setCategory(String category)
    {
        this.category = category;
    }
    
    /**
     * 题类
     * @return
     */
	public String getCategory()
    {
        return category;
    }
    
    
	/**
     * 题型
     * @param type
     */
    public void setType(String type)
    {
        this.type = type;
    }
    
    /**
     * 题型
     * @return
     */
	public String getType()
    {
        return type;
    }
    
    
	/**
     * 内容
     * @param content
     */
    public void setContent(String content)
    {
        this.content = content;
    }
    
    /**
     * 内容
     * @return
     */
	public String getContent()
    {
        return content;
    }
    
    
	/**
     * 选项一
     * @param optionA
     */
    public void setOptionA(String optionA)
    {
        this.optionA = optionA;
    }
    
    /**
     * 选项一
     * @return
     */
	public String getOptionA()
    {
        return optionA;
    }
    
    
	/**
     * 选项二
     * @param optionB
     */
    public void setOptionB(String optionB)
    {
        this.optionB = optionB;
    }
    
    /**
     * 选项二
     * @return
     */
	public String getOptionB()
    {
        return optionB;
    }
    
    
	/**
     * 选项三
     * @param optionC
     */
    public void setOptionC(String optionC)
    {
        this.optionC = optionC;
    }
    
    /**
     * 选项三
     * @return
     */
	public String getOptionC()
    {
        return optionC;
    }
    
    
	/**
     * 选项四
     * @param optionD
     */
    public void setOptionD(String optionD)
    {
        this.optionD = optionD;
    }
    
    /**
     * 选项四
     * @return
     */
	public String getOptionD()
    {
        return optionD;
    }   
	/**
     * 包装取答案
     * @param result
     */
    public void setTakeResult(String result)    
    {      	
    	if(getOptionA().equals(result)) {
    		this.result = "A"; 
    	}else if(getOptionB().equals(result)){
    		this.result = "B";
    	}else if(getOptionC().equals(result)){
    		this.result = "C";
    	}else if(getOptionD().equals(result)){
    		this.result = "D";
    	}else {
            this.result = result;
    	}
    	
    	
    }
    /**
     * 包装存答案
     * @param result
     */
    public void setSaveResult(String result)
    {  
  
    	if("A".equals(result)) {
    		this.result = getOptionA(); 
    	}else if("B".equals(result)){
    		this.result = getOptionB();
    	}else if("C".equals(result)){
    		this.result = getOptionC();
    	}else if("D".equals(result)){
    		this.result = getOptionD();
    	}else {
            this.result = result;
    	}
    	
    }
    /**
     * 答案
     * @param result
     */
    public void setResult(String result)
    {
        this.result = result;
    }
    
    /**
     * 答案
     * @return
     */
	public String getResult()
    {
        return result;
    }
    
    
	/**
     * 创建人
     * @param createdBy
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }
    
    /**
     * 创建人
     * @return
     */
	public String getCreatedBy()
    {
        return createdBy;
    }
    
    
	/**
     * 创建时间
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    /**
     * 创建时间
     * @return
     */
	public Date getCreatedTime()
    {
        return createdTime;
    }
    
    
    /**
     * 创建时间 开始时间
     * @param createdTimeBegin
     */
    public void setCreatedTimeBegin(String createdTimeBegin)
    {
        this.createdTimeBegin = createdTimeBegin;
    }
    
 	/**
     * 创建时间 开始时间
     * @return
     */
	public String getCreatedTimeBegin()
    {
        return createdTimeBegin;
    }
    
    /**
     * 创建时间 结束时间
     * @param createdTimeEnd
     */
    public void setCreatedTimeEnd(String createdTimeEnd)
    {
        this.createdTimeEnd = createdTimeEnd;
    }
 	
 	/**
     * 创建时间 结束时间
     * @return
     */
	public String getCreatedTimeEnd()
    {
        return createdTimeEnd;
    }
    
	/**
     * 更新人
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }
    
    /**
     * 更新人
     * @return
     */
	public String getUpdatedBy()
    {
        return updatedBy;
    }
    
    
	/**
     * 更新时间
     * @param updatedTime
     */
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }
    
    /**
     * 更新时间
     * @return
     */
	public Date getUpdatedTime()
    {
        return updatedTime;
    }
    
    
    /**
     * 更新时间 开始时间
     * @param updatedTimeBegin
     */
    public void setUpdatedTimeBegin(String updatedTimeBegin)
    {
        this.updatedTimeBegin = updatedTimeBegin;
    }
    
 	/**
     * 更新时间 开始时间
     * @return
     */
	public String getUpdatedTimeBegin()
    {
        return updatedTimeBegin;
    }
    
    /**
     * 更新时间 结束时间
     * @param updatedTimeEnd
     */
    public void setUpdatedTimeEnd(String updatedTimeEnd)
    {
        this.updatedTimeEnd = updatedTimeEnd;
    }
 	
 	/**
     * 更新时间 结束时间
     * @return
     */
	public String getUpdatedTimeEnd()
    {
        return updatedTimeEnd;
    }
    
}