package com.exam.stu.topic.service;
import com.exam.base.service.IAbstractService;
import com.exam.stu.topic.model.TbTopic;

/**
 * 试题表  Service
 * 
 * @author WANLITAO
 * 2020-03-28 16:52
 */
public interface TbTopicService extends IAbstractService<TbTopic>{


}
