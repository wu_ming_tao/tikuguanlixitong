package com.exam.stu.topic.constant;

public interface TopicConstant {
	/**
	 * 登陆用户
	 */
	String LOGIN_USER = "loginUser";
	
	/**
	 * 菜单数据
	 */
	String MENU_DATA = "menuData";
	
	/**
	 * 根目录
	 */
	String ROOT_MENU = "rootMenu";
}
