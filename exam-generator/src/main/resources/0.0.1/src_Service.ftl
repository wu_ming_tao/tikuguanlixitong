package ${servicepackage_name };

import com.exam.base.service.IAbstractService;
import ${modelpackage_name }.${tableInfo.className };

/**
 * ${tableInfo.tableComment } Service
 * 
 * @author ${author! }
 * ${date! }
 */
public interface ${tableInfo.className }Service extends IAbstractService<${tableInfo.className}>
{

}
