package ${servicepackage_name }.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.exam.base.dao.IAbstractDao;
import com.exam.base.service.impl.AbstractServiceImpl;
 
import ${daopackage_name }.${tableInfo.className }Dao;
import ${modelpackage_name }.${tableInfo.className };
import ${servicepackage_name }.${tableInfo.className }Service;

/**
 * ${tableInfo.tableComment} Service实现
 * 
 * @author  ${author! }
 * ${date! }
 */
@Service("${tableInfo.classname }Service")
public class ${tableInfo.className }ServiceImpl extends AbstractServiceImpl<${tableInfo.className}> implements ${tableInfo.className }Service
{

    @Autowired
    private ${tableInfo.className }Dao ${tableInfo.classname }Dao;

	@Override
	protected IAbstractDao<${tableInfo.className}> getBaseDao()
	{
		return ${tableInfo.classname }Dao;
	}
}