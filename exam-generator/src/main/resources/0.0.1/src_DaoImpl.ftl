package ${daopackage_name};

import com.exam.base.dao.IAbstractDaoImpl;
import ${modelpackage_name}.${tableInfo.className};

/**
 * ${tableInfo.tableComment} DAO层
 * 
 * @author  ${author! }
 * ${date! }
 */
public class ${tableInfo.className}Dao extends IAbstractDaoImpl<${tableInfo.className}>
{

}