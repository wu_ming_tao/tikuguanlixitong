package com.exam.codegen;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.exam.codegen.model.TargetTable;

public class CodeGenTest {
	public static void main(String[] args) throws Exception
	{
		// generate();
		generateWithAlias();
	}

	private static void generateWithAlias() throws Exception
	{
		// 目标数据表
		List<TargetTable> targetTbs = new ArrayList<TargetTable>();
		targetTbs.add(new TargetTable("WZJH_PARAM_ONE", "ParamOne"));
		CodeGenMain codeGenMain = new CodeGenMain();
		Properties p = new Properties();
		p.load(CodeGenMain.class.getResourceAsStream("/config.properties"));
		codeGenMain.generateWithAlias(targetTbs, p);
	}

	private static void generate() throws Exception
	{
		// 目标数据表
		List<String> targetTbs = new ArrayList<String>();
		// targetTbs.add("sys_org_person");
		// targetTbs.add("sys_log_login");
		// targetTbs.add("WZJH_BT_DEICVEONE");
		// targetTbs.add("WZJH_BT_DEICVETWO");
		// targetTbs.add("WZJH_BT_TXXL");
		// targetTbs.add("WZJH_ENTRUST_PARAM");
		// targetTbs.add("WZJH_PROJECT");
		// targetTbs.add("WZJH_ENTRUST_DESIGN");
		// targetTbs.add("WZJH_PASKSON");
		targetTbs.add("WZJH_PARAM_ONE");
		CodeGenMain codeGenMain = new CodeGenMain();
		Properties p = new Properties();
		p.load(CodeGenMain.class.getResourceAsStream("/config.properties"));
		codeGenMain.generate(targetTbs, p);
	} 
}
