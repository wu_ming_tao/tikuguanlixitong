package com.exam.codegen.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.exam.codegen.CodeGenMain;

/**
 * 生成代码入口方法
 * @author Administrator
 *
 */
public class CodeGenTest {
	public static void main(String[] args) {
		try {
			List<String> targetTbs = new ArrayList<String>(); 
		    targetTbs.add("tb_results");		    
			CodeGenMain codeGenMain = new CodeGenMain();
			Properties p = new Properties();
			p.load(CodeGenTest.class.getResourceAsStream("config.properties"));
			codeGenMain.generate(targetTbs, p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
