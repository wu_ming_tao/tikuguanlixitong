package com.exam.codegen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang3.StringUtils;

import com.exam.codegen.model.ColumnInfo;
import com.exam.codegen.model.TableInfo;
import com.exam.codegen.model.TargetTable;
import com.exam.codegen.util.CodeGenConfig;
import com.exam.codegen.util.FileUtil;
import com.exam.codegen.util.FreeMarkerTemplateUtils;
import com.exam.codegen.util.StringUtil;

import freemarker.template.Template;

public class CodeGenMain {
	private static final Log LOG = LogFactory.getLog(CodeGenMain.class);

	private void init(Properties p)
	{
		CodeGenConfig.init(p);
	}

	public void generateWithAlias(List<TargetTable> targetTbs, Properties p)
	{
		try
		{
			init(p);

			File rootFolder = new File(CodeGenConfig.DISKPATH);
			if (rootFolder.exists())
			{
				FileUtil.deleteFile(rootFolder);
			}
			FileUtil.mkDirs(rootFolder);

			List<TableInfo> tables = new ArrayList<TableInfo>();
			for (TargetTable tb : targetTbs)
			{
				tables.add(getTableInfo(tb.getTableName(), tb.getClassName()));
			}

			generateSrc(tables);
			generateConfig(tables);
			generatePage(tables);
			generateInitSql(tables);
		}
		catch (Exception e)
		{
			LOG.error(e);
		}
	}

	public void generate(List<String> targetTbs, Properties p)
	{
		try
		{
			init(p);

			File rootFolder = new File(CodeGenConfig.DISKPATH);
			if (rootFolder.exists())
			{
				FileUtil.deleteFile(rootFolder);
			}
			FileUtil.mkDirs(rootFolder);

			List<TableInfo> tables = new ArrayList<TableInfo>();
			for (String tb : targetTbs)
			{
				tables.add(getTableInfo(tb, null));
			}

			generateSrc(tables);
			generateConfig(tables);
			//generatePage(tables);
			generateInitSql(tables);
		}
		catch (Exception e)
		{
			LOG.error(e);
		}
	}

	private void generateInitSql(List<TableInfo> tables) throws Exception
	{
		final String configPath = CodeGenConfig.DISKPATH + "init/";
		String suffix = "init.sql";
		String path = configPath + suffix;
		String templateName = "init_sql.ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);

		suffix = "init_mysql.sql";
		path = configPath + suffix;
		templateName = "init_sql_mysql.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);
	}

	private TableInfo getTableInfo(String tb, String tbAlias) throws Exception
	{
		String _tb = tb.toLowerCase();
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			conn = getConnection();
			pst = conn.prepareStatement(CodeGenConfig.SELECT_TB);
			pst.setString(1, tb);
			rs = pst.executeQuery();
			TableInfo tableInfo = null;
			while (rs.next())
			{
				tableInfo = new TableInfo();
				tableInfo.setTableName(_tb);
				if (StringUtil.isNotEmpty(tbAlias))
				{
					tableInfo.setClassName(StringUtils.capitalize(tbAlias));
				}
				else
				{
					tableInfo.setClassName(replaceUnderLineAndUpperCase(_tb));
				}
				tableInfo.setClassname(StringUtils.uncapitalize(tableInfo.getClassName()));
				tableInfo.setTableComment(rs.getString("table_comment"));
			}

			if (tableInfo == null)
			{
				throw new RuntimeException("表[" + tb + "]不存在");
			}

			LOG.debug(tableInfo);

			pst = conn.prepareStatement(CodeGenConfig.SELECT_TBCOL);
			pst.setString(1, tb);
			rs = pst.executeQuery();
			List<ColumnInfo> columns = new ArrayList<ColumnInfo>();
			ColumnInfo column = null;
			while (rs.next())
			{
				column = new ColumnInfo();
				column.setColumnComment(rs.getString("column_comment"));
				String dataType = rs.getString("data_type").toLowerCase();

				if (dataType.indexOf("(") != -1)
				{
					dataType = dataType.substring(0, dataType.indexOf("("));
				}

				if ("number".equals(dataType))
				{
					String dataScale = rs.getString("data_scale");
					if (StringUtil.isNotEmpty(dataScale) && !"0".equals(dataScale))
					{
						dataType = "decimal";
					}
					else
					{
						String dataPrecision = rs.getString("data_precision");
						if (StringUtil.isNotEmpty(dataPrecision) && Integer.parseInt(dataPrecision) >= 20)
						{
							dataType = "bigint";
						}
					}
				}

				column.setColumnType(dataType);
				column.setColumnName(rs.getString("column_name").toLowerCase());
				column.setColumnKey(rs.getString("COLUMN_KEY"));
				column.setColumnLength(rs.getString("CHARACTER_MAXIMUM_LENGTH"));
				column.setAttrName(replaceUnderLineAndUpperCase(column.getColumnName()));
				column.setAttrname(StringUtils.uncapitalize(column.getAttrName()));
				// 列的数据类型，转换成Java类型
				String attrType = javaTypeMap.get(column.getColumnType());
				column.setAttrType(attrType);
				columns.add(column);

				if (StringUtil.isNotEmpty(column.getColumnKey()) && tableInfo.getPrimaryKey() == null)
				{
					tableInfo.setPrimaryKey(column);
				}

				LOG.debug(column);
			}

			if (null == tableInfo.getPrimaryKey())
			{
				tableInfo.setPrimaryKey(columns.get(0));
			}
			tableInfo.setColumns(columns);
			return tableInfo;
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
			if (pst != null)
			{
				try
				{
					pst.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
			if (conn != null)
			{
				try
				{
					conn.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private Connection getConnection() throws Exception
	{
		Class.forName(CodeGenConfig.DRIVER);
		Connection connection = DriverManager.getConnection(CodeGenConfig.URL, CodeGenConfig.USER, CodeGenConfig.PASSWORD);
		return connection;
	}

	private void generatePage(List<TableInfo> tables) throws Exception
	{
		final String configPath = CodeGenConfig.DISKPATH + "page/ftl/" + CodeGenConfig.MODULE_PATH + "/";
		for (TableInfo tableInfo : tables)
		{
			generatePageIndexFile(configPath, tableInfo);
			generatePageEditFile(configPath, tableInfo);
		}
	}

	private void generatePageEditFile(String configPath, TableInfo tableInfo) throws Exception
	{
		String suffix = "edit.htm";
		String path = configPath + tableInfo.getClassname() + "/" + suffix;
		String templateName = "page_edit.ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);

		suffix = "edit.htm";
		path = configPath + tableInfo.getClassname() + "/tab/" + suffix;
		templateName = "page_edit_tab.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);
	}

	private void generatePageIndexFile(String configPath, TableInfo tableInfo) throws Exception
	{
		String suffix = "index.htm";
		String path = configPath + tableInfo.getClassname() + "/" + suffix;
		String templateName = "page_index.ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);

		suffix = "index.htm";
		path = configPath + tableInfo.getClassname() + "/tab/" + suffix;
		templateName = "page_index_tab.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);

		suffix = "load.htm";
		path = configPath + tableInfo.getClassname() + "/" + suffix;
		templateName = "page_load.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);

		suffix = "query.htm";
		path = configPath + tableInfo.getClassname() + "/" + suffix;
		templateName = "page_query.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);
	}

	private void generateConfig(List<TableInfo> tables) throws Exception
	{
		final String configPath = CodeGenConfig.DISKPATH + "config/";
		generateMyBatis(configPath, tables);
		//generateContext(configPath, tables);
		//generateServlet(configPath, tables);
	}

	/** 生成 servlet 文件 */
	private void generateServlet(String configPath, List<TableInfo> tables) throws Exception
	{
		String suffix = "-servlet.xml";
		String path = configPath + "servlet/" + CodeGenConfig.MODULE_PATH + suffix;
		String templateName = "config_servlet.ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);
	}

	/** 生成spring ds dao service 配置文件 */
	private void generateContext(String configPath, List<TableInfo> tables) throws Exception
	{
		configPath += "context/" + CodeGenConfig.MODULE_PATH + "/";
		String suffix = "applicationContext-dao.xml";
		String path = configPath + suffix;
		String templateName = "config_dao.ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);

		suffix = "applicationContext-ds.xml";
		path = configPath + suffix;
		templateName = "config_ds.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);

		suffix = "applicationContext-service.xml";
		path = configPath + suffix;
		templateName = "config_service.ftl";
		mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);
	}

	/** 生成mybatis-model文件 */
	private void generateMyBatis(String configPath, List<TableInfo> tables) throws Exception
	{
		for (TableInfo tableInfo : tables)
		{
			generateMyBatisMapperFile(configPath + "mybatis/sqlmap/" + CodeGenConfig.MODULE_PATH + "/m/", tableInfo, "mysql");
			generateMyBatisMapperFile(configPath + "mybatis/sqlmap/" + CodeGenConfig.MODULE_PATH + "/o/", tableInfo, "orcale");
		}
		generateMyBatisConfigFile(configPath + "mybatis/sqlmap/", tables);
	}

	/** 生成mybatis-config文件 */
	private void generateMyBatisConfigFile(String configPath, List<TableInfo> tables) throws Exception
	{
		final String suffix = CodeGenConfig.MODULENAME + "-config.xml";
		final String path = configPath + suffix;
		final String templateName = "config_SqlConfig.ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tables);
	}

	/** 生成mybatis-Mapper文件 */
	private void generateMyBatisMapperFile(String configPath, TableInfo tableInfo, String database) throws Exception
	{
		final String suffix = "_SqlMap.xml";
		final String path = configPath + tableInfo.getClassName() + suffix;
		final String templateName = "config_SqlMap_" + database + ".ftl";
		File mapperFile = new File(path);
		FileUtil.mkParentDirs(mapperFile);
		generateFileByTemplate(templateName, mapperFile, tableInfo);
	}

	/** 根据模板生成指定文件 */
	private void generateFileByTemplate(final String templateName, File file, Object obj) throws Exception
	{
		Template template = FreeMarkerTemplateUtils.getTemplate(templateName);
		FileOutputStream fos = new FileOutputStream(file);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("tableInfo", obj);
		dataMap.put("author", CodeGenConfig.AUTHOR);
		dataMap.put("date", CodeGenConfig.CURRENT_DATE);
		dataMap.put("package_name", CodeGenConfig.PACKAGE_NAME);
		dataMap.put("modelpackage_name", CodeGenConfig.MODELPACKAGE_NAME);
		dataMap.put("module_name", CodeGenConfig.MODULE_NAME);
		dataMap.put("modulename", CodeGenConfig.MODULENAME);
		dataMap.put("module_path", CodeGenConfig.MODULE_PATH);
		dataMap.put("daopackage_name", CodeGenConfig.DAOPACKAGE_NAME);
		dataMap.put("servicepackage_name", CodeGenConfig.SERVICEPACKAGE_NAME);
		dataMap.put("txnpackage_name", CodeGenConfig.TXNPACKAGE_NAME);
		dataMap.put("controllerpackage_name", CodeGenConfig.CONTROLLERPACKAGE_NAME);

		Writer out = new BufferedWriter(new OutputStreamWriter(fos, "utf-8"), 10240);
		template.process(dataMap, out);
	}

	/** 生成src文件 */
	private void generateSrc(List<TableInfo> tables) throws Exception
	{
		final String srcPath = CodeGenConfig.DISKPATH + "src/";
		generateModelFile(srcPath, tables);
		generateDaoFile(srcPath, tables);
		generateServiceFile(srcPath, tables);
 
	}

	 
 
	/** 生成src-service文件 */
	private void generateServiceFile(String srcPath, List<TableInfo> tables) throws Exception
	{
		String servicePath = srcPath + CodeGenConfig.SERVICEPACKAGE_NAME.replace(".", "/") + "/";
		for (TableInfo tableInfo : tables)
		{
			String suffix = "Service.java";
			String path = servicePath + tableInfo.getClassName() + suffix;
			String templateName = "src_Service.ftl";
			File f = new File(path);
			FileUtil.mkParentDirs(f);
			generateFileByTemplate(templateName, f, tableInfo);
		}

		servicePath = servicePath + "impl/";
		for (TableInfo tableInfo : tables)
		{
			String suffix = "ServiceImpl.java";
			String path = servicePath + tableInfo.getClassName() + suffix;
			String templateName = "src_ServiceImpl.ftl";
			File f = new File(path);
			FileUtil.mkParentDirs(f);
			generateFileByTemplate(templateName, f, tableInfo);
		}

	}

	/** 生成src-dao文件 */
	private void generateDaoFile(String srcPath, List<TableInfo> tables) throws Exception
	{
		String modelPath = srcPath + CodeGenConfig.DAOPACKAGE_NAME.replace(".", "/") + "/impl/";
		for (TableInfo tableInfo : tables)
		{
			String suffix = "Dao.java";
			String path = modelPath + tableInfo.getClassName() + suffix;
			String templateName = "src_DaoImpl.ftl";
			File f = new File(path);
			FileUtil.mkParentDirs(f);
			generateFileByTemplate(templateName, f, tableInfo);
		}
	}

	/** 生成src-model文件 */
	private void generateModelFile(String srcPath, List<TableInfo> tables) throws Exception
	{
		String modelPath = srcPath + CodeGenConfig.MODELPACKAGE_NAME.replace(".", "/") + "/";
		for (TableInfo tableInfo : tables)
		{
			String suffix = ".java";
			String path = modelPath + tableInfo.getClassName() + suffix;
			String templateName = "src_Model.ftl";
			File f = new File(path);
			FileUtil.mkParentDirs(f);
			generateFileByTemplate(templateName, f, tableInfo);
		}
	}

	private static String replaceUnderLineAndUpperCase(String str)
	{
		return StringUtils.capitalize(replaceUnderLine(str));
	}

	private static String replaceUnderLine(String str)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		int count = sb.indexOf("_");
		while (count != 0)
		{
			int num = sb.indexOf("_", count);
			count = num + 1;
			if (num != -1)
			{
				char ss = sb.charAt(count);
				char ia = (char) (ss - 32);
				sb.replace(count, count + 1, ia + "");
			}
		}
		return sb.toString().replaceAll("_", "");
	}

	/** 状态编码转换 */
	private static Map<String, String> javaTypeMap = new HashMap<String, String>();

	static
	{
		initJavaTypeMap();
	}

	/**
	 * 返回状态映射
	 */
	private static void initJavaTypeMap()
	{
		javaTypeMap.put("tinyint", "Integer");
		javaTypeMap.put("smallint", "Integer");
		javaTypeMap.put("mediumint", "Integer");
		javaTypeMap.put("int", "Integer");
		javaTypeMap.put("integer", "Integer");
		javaTypeMap.put("number", "Integer");
		javaTypeMap.put("bigint", "Long");
		javaTypeMap.put("float", "Float");
		javaTypeMap.put("double", "Double");
		javaTypeMap.put("decimal", "BigDecimal");
		javaTypeMap.put("bit", "Boolean");
		javaTypeMap.put("char", "String");
		javaTypeMap.put("varchar", "String");
		javaTypeMap.put("varchar2", "String");
		javaTypeMap.put("tinytext", "String");
		javaTypeMap.put("text", "String");
		javaTypeMap.put("clob", "String");
		javaTypeMap.put("mediumtext", "String");
		javaTypeMap.put("longtext", "String");
		javaTypeMap.put("date", "Date");
		javaTypeMap.put("datetime", "Date");
		javaTypeMap.put("timestamp", "Date");
	}
}
