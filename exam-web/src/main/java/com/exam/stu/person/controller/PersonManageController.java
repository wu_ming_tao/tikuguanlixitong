package com.exam.stu.person.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.person.pathurl.PersonPathUrl;
import com.exam.stu.user.business.UserBussiness;
import com.exam.stu.user.model.TbUser;
import com.exam.stu.user.pathurl.UserPathUrl;

/**
 * 用户信息管理
 * @author Administrator
 *
 */
@Controller 
@RequestMapping("/person")
public class PersonManageController {
	@Autowired
	private UserBussiness userBussiness;
	/**
	 * 用户信息首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PersonPathUrl.PERSON_INDEX); 
		return modelAndView;
		
	}	
	/**
	 * 用户信息表数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		//ModelAndView modelAndView = new ModelAndView();
		//modelAndView.setViewName(PersonPathUrl.PERSON_INDEX); 
		//ResponsePageModel<TbUser> pageModel = userBussiness.getUserPageData(request);
		//Map<String, Object> dataMap = new HashMap<String, Object>();
		//dataMap.put("pageModel", pageModel);
		//modelAndView.addAllObjects(dataMap);
		//return modelAndView;
		String id = request.getParameter("id");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PersonPathUrl.PERSON_TABLELOAD);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{
			TbUser user = userBussiness.findById(id);
			dataMap.put("TbUser", user);
	     }
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	/**
	 * 用户信息编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editView(HttpServletRequest request)
	{
		String id = request.getParameter("id"); 
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PersonPathUrl.PERSON_EDIT);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
			TbUser tbUser = userBussiness.findByUserId(id);
			dataMap.put("TbUser", tbUser);
			dataMap.put("method", "update");		
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	/**
	 * 用户信息编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/editPassword")
	public ModelAndView editPasswordView(HttpServletRequest request)
	{
		String id = request.getParameter("id"); 
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PersonPathUrl.PERSON_REVISE);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
			TbUser tbUser = userBussiness.findByUserId(id);
			dataMap.put("TbUser", tbUser);
			dataMap.put("method", "revise");		
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 编辑用户信息
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResponseModel updateUser(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbUser queryModel = ServletBeanTools.populate(TbUser.class, request); 
			userBussiness.update(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 编辑用户信息
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/revise")
	public ResponseModel revisePassword(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbUser queryModel = ServletBeanTools.populate(TbUser.class, request); 
			userBussiness.update(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	


}
