package com.exam.stu.person.pathurl;

/**
 * 用户信息路径
 * @author Administrator
 *
 */
public interface PersonPathUrl {
	
	/**
	 * 用户信息首页
	 */
	String PERSON_INDEX = "/person/index";
	
	/**
	 * 用户信息加载
	 */
	String PERSON_TABLELOAD = "/person/tableload";
	
	/**
	 * 编辑账号界面
	 */
	String PERSON_EDIT = "/person/win/personUpdata";
	/**
	 * 修改密码界面
	 */
	String PERSON_REVISE = "/person/win/personRevise";
}
