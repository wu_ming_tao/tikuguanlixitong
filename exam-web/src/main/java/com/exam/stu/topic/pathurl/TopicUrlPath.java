package com.exam.stu.topic.pathurl;

/**
 * 角色控制
 * @author Administrator
 *
 */
public interface TopicUrlPath {
	/**
	 * 单选试题编辑
	 */
	String TOPIC_CHOICE = "选择题(单)";
	/**
	 * 多选试题编辑
	 */
	String TOPIC_CHOICES = "选择题(多)";
	/**
	 * 判断试题编辑
	 */
	String TOPIC_JUDEGE = "判断题";
	/** 
	 * 问答试题编辑
	 */
	String TOPIC_ANSWER = "问答题";	
	/**
	 * 试题首页
	 */
	String TOPIC_INDEX = "/topic/index";	
	/**
	 * 表格加载
	 */
	String TOPIC_TABLELOAD = "/topic/tableload";
	/**
	 * 表格加载
	 */
	String TOPIC_TABLELOADS = "/topic/win/tableload";
	
	/**
	 * 试题编辑
	 */
	String TOPIC_EDIT = "/topic/win/topicEdit";
	
	/**
	 * 试题加载
	 */
	String TOPIC_EDITLOAD = "/topic/win/topicload";
	/**
	 * 选择题（单）编辑页面加载
	 */
	String TOPIC_CHOICEEDIT = "/topic/win/topicChoiceEdit";
	/**
	 * 选择题（多）编辑页面加载
	 */
	String TOPIC_CHOICESEDIT = "/topic/win/topicChoicesEdit";
	/**
	 * 问答题编辑页面加载
	 */
	String TOPIC_ANSWEREDIT = "/topic/win/topicAnswerEdit";
	/**
	 * 判断题编辑页面加载
	 */
	String TOPIC_JUDEGEEDIT = "/topic/win/topicJudgmentEdit";
}
