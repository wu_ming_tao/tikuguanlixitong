package com.exam.stu.topic.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.exam.base.controller.BaseController;
import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.exam.model.TbExamPaper;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.power.model.vo.RolePowerDataVo;
import com.exam.stu.power.model.vo.ZkTreeNode;
import com.exam.stu.power.pathurl.PowerUrl;
import com.exam.stu.role.business.RoleMenuBussiness;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.pathurl.RoleUrlPath;
import com.exam.stu.topic.business.TopicBussiness;
import com.exam.stu.topic.model.TbTopic;
import com.exam.stu.topic.pathurl.TopicUrlPath;

/**
 * 角色管理控制
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/topic")
public class TopicManageController extends BaseController{
	
	@Autowired
	private TopicBussiness topicBussiness;
	
	/**
	 * 试题首页
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(TopicUrlPath.TOPIC_INDEX); 
		return modelAndView;
	}
	
	/**
	 * 试题表信息加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(TopicUrlPath.TOPIC_TABLELOAD);
		String type = request.getParameter("type");
		String grade = request.getParameter("grade");
		String category = request.getParameter("category");
		String pageOffset = request.getParameter("pageOffset");
	    int pageOffsets=Integer.parseInt(pageOffset);
	    String types = topicBussiness.typeOf(type);
	    String grades = topicBussiness.gradeOf(grade);
	    TbTopic topic = new TbTopic();
	    topic.setCategory(category);
	    topic.setType(types);
	    topic.setGrade(grades);
	    topic.setPageOffset(pageOffsets);
	    ResponsePageModel<TbTopic> pageModels = topicBussiness.getPageData(topic);
	    ResponsePageModel<TbTopic> pageModel  = topicBussiness.pageModelOf(pageModels);	   
	    Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);

		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	/**
	 * 试题编辑页面加载
	 * @param request
	 * @return
	 */

	@RequestMapping("/tabletypeload")
	public ModelAndView tabletypeload(HttpServletRequest request)
	{
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.setViewName(TopicUrlPath.TOPIC_CHOICEEDIT); 
		return modelAndView;
	}
	
	/**
	 * 编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editView(HttpServletRequest request)
	{
		String id = request.getParameter("id"); 
		ModelAndView modelAndView = new ModelAndView();		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{		
			TbTopic tbTopic = topicBussiness.findById(id);
			tbTopic.setGradeUpdata(tbTopic.getGrade());
			if(tbTopic.getType().equals(TopicUrlPath.TOPIC_CHOICE)||tbTopic.getType().equals(TopicUrlPath.TOPIC_CHOICES)) {
				tbTopic.setTakeResult(tbTopic.getResult());
				}
			String type = tbTopic.getType();
			dataMap.put("TbTopic", tbTopic);
			dataMap.put("method", "update");
			if (TopicUrlPath.TOPIC_CHOICE.equals(type))
			{
			modelAndView.setViewName(TopicUrlPath.TOPIC_CHOICEEDIT); 
			}else if (TopicUrlPath.TOPIC_CHOICES.equals(type)) {
				modelAndView.setViewName(TopicUrlPath.TOPIC_CHOICESEDIT);
			}else if (TopicUrlPath.TOPIC_JUDEGE.equals(type)) {
				modelAndView.setViewName(TopicUrlPath.TOPIC_JUDEGEEDIT);
			}else if (TopicUrlPath.TOPIC_ANSWER.equals(type)) {
				modelAndView.setViewName(TopicUrlPath.TOPIC_ANSWEREDIT);
			}
		}
		else
		{
			String type = request.getParameter("type"); 
			TbTopic tbTopic = new TbTopic();
			dataMap.put("method", "add");
			dataMap.put("TbTopic", tbTopic);
			tbTopic.setType(type);
			if (TopicUrlPath.TOPIC_CHOICE.equals(type))
			{
			modelAndView.setViewName(TopicUrlPath.TOPIC_CHOICEEDIT); 
			}else if (TopicUrlPath.TOPIC_CHOICES.equals(type)) {
				modelAndView.setViewName(TopicUrlPath.TOPIC_CHOICESEDIT);
			}else if (TopicUrlPath.TOPIC_JUDEGE.equals(type)) {
				modelAndView.setViewName(TopicUrlPath.TOPIC_JUDEGEEDIT);
			}else if (TopicUrlPath.TOPIC_ANSWER.equals(type)) {
				modelAndView.setViewName(TopicUrlPath.TOPIC_ANSWEREDIT);
			}
		}	
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 添加试题
	 * @param request
	 * @return
	 */	
	@ResponseBody
	@RequestMapping("/add")
	public ResponseModel addTopic(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try { 
			TbTopic queryModel = ServletBeanTools.populate(TbTopic.class, request);
			if(queryModel.getType().equals(TopicUrlPath.TOPIC_CHOICE)||queryModel.getType().equals(TopicUrlPath.TOPIC_CHOICES)) {
			queryModel.setSaveResult(queryModel.getResult());
			}
			topicBussiness.addTopic(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 更新试题
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResponseModel updateTopic(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbTopic queryModel = ServletBeanTools.populate(TbTopic.class, request);
			if(queryModel.getType().equals(TopicUrlPath.TOPIC_CHOICE)||queryModel.getType().equals(TopicUrlPath.TOPIC_CHOICES)) {
				queryModel.setSaveResult(queryModel.getResult());
				}
			topicBussiness.updateTopic(queryModel);			
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 删除试题
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResponseModel deleteTopic(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			topicBussiness.delete(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	
//	/**
//	 * 查询试题
//	 * @param request
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping("/query")
//	public ModelAndView queryTopic(HttpServletRequest request)
//	{
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName(TopicUrlPath.TOPIC_TABLELOADS); 		
//		ResponsePageModel<TbTopic> pageModel = topicBussiness.getTopicPageData(request);
//			//String content = request.getParameter("content");
//			//List<TbTopic> lstData = topicBussiness.queryTopic(content);
//		Map<String, Object> dataMap = new HashMap<String, Object>();
//		dataMap.put("pageModel", pageModel);
//		modelAndView.addAllObjects(dataMap);
//		return modelAndView;
//	}
}
