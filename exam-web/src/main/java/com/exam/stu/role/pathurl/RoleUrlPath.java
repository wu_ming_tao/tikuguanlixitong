package com.exam.stu.role.pathurl;

/**
 * 角色控制
 * @author Administrator
 *
 */
public interface RoleUrlPath {
	/**
	 * 角色首页
	 */
	String ROLE_INDEX = "/role/index";
	
	/**
	 * 表格视图
	 */
	String ROLE_TABLEVIEW = "/role/tableview";
	
	/**
	 * 表格加载
	 */
	String ROLE_TABLELOAD = "/role/tableload";
	
	/**
	 * 角色编辑
	 */
	String ROLE_EDIT = "/role/win/roleEdit";
	
	/**
	 * 角色加载
	 */
	String ROLE_EDITLOAD = "/role/win/roleload";
}
