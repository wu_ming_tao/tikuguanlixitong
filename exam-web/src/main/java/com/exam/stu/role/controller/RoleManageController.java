package com.exam.stu.role.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.exam.base.controller.BaseController;
import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.power.model.vo.RolePowerDataVo;
import com.exam.stu.power.model.vo.ZkTreeNode;
import com.exam.stu.power.pathurl.PowerUrl;
import com.exam.stu.role.business.RoleMenuBussiness;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.pathurl.RoleUrlPath;

/**
 * 角色管理控制
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/role")
public class RoleManageController extends BaseController{
	
	@Autowired
	private RoleMenuBussiness roleMenuBussiness;
	
	/**
	 * 角色首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(RoleUrlPath.ROLE_INDEX); 
		return modelAndView;
	}
	
	/**
	 * 登录首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableview")
	public ModelAndView tableView(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(RoleUrlPath.ROLE_TABLEVIEW); 
		return modelAndView;
	}
	
	/**
	 * 权限表数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(RoleUrlPath.ROLE_TABLELOAD); 
		ResponsePageModel<TbRole> pageModel = roleMenuBussiness.getRolePageData(request);
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editView(HttpServletRequest request)
	{
		String id = request.getParameter("id"); 
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(RoleUrlPath.ROLE_EDIT);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{
			TbRole tbRole = roleMenuBussiness.findById(id);
			dataMap.put("TbRole", tbRole);
			dataMap.put("method", "update");
		}
		else
		{
			TbRole tbRole = new TbRole();
		 
			dataMap.put("method", "add");
			dataMap.put("TbRole", tbRole);
		} 
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public ResponseModel addRole(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try { 
			TbRole queryModel = ServletBeanTools.populate(TbRole.class, request); 
			roleMenuBussiness.addRole(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResponseModel updateRole(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbRole queryModel = ServletBeanTools.populate(TbRole.class, request); 
			roleMenuBussiness.updateRole(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResponseModel deleteRole(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			roleMenuBussiness.delete(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 权限数据展示加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/treeload")
	public void treeLoad(HttpServletRequest request,HttpServletResponse response)
	{
		super.outJson(roleMenuBussiness.getRolePowerJsonData(request), response); 
	}
	
	/**
	 * 更新菜单权限
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateRolePower")
	public ResponseModel updateRolePower(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String dataStr = ServletBeanTools.convertStreamToString(request.getInputStream()); 
			RolePowerDataVo data = JSONArray.parseObject(dataStr, RolePowerDataVo.class);
			roleMenuBussiness.updateRoleMenu(data); 
			 
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
}
