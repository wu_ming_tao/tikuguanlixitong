package com.exam.stu.generate.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.exam.base.controller.BaseController;
import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.exam.business.ExamPaperBussiness;
import com.exam.stu.exam.business.TopicPaperBussiness;
import com.exam.stu.exam.model.TbExamPaper;
import com.exam.stu.exam.model.TbTopicPaper;
import com.exam.stu.exam.model.TbTopics;
import com.exam.stu.exam.model.TypeCount;
import com.exam.stu.exam.pathurl.ExamPathUrl;
import com.exam.stu.generate.pathurl.GeneratePathUrl;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.model.TbRoleMenu;
import com.exam.stu.topic.business.TopicBussiness;
import com.exam.stu.topic.model.TbTopic;
import com.exam.stu.topic.pathurl.TopicUrlPath;
import com.exam.stu.user.constant.UserConstant;
import com.exam.stu.user.model.TbUser;

/**
 * 组卷管理
 * @author Administrator
 *
 */
@Controller 
@RequestMapping("/generate")
public class GenerateManageController extends BaseController{
	@Autowired
	private ExamPaperBussiness examPaperBussiness;
	@Autowired
	private TopicPaperBussiness topicPaperBussiness;
	@Autowired
	private TopicBussiness topicBussiness;
	
	/**
	 * 组卷首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{   String id = request.getParameter("id");	    
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.GENERATE_INDEX); 
		Map<String, String> DataMap = new HashMap<String, String>();
		DataMap.put("id",id);
		modelAndView.addAllObjects(DataMap);
		return modelAndView;
	}
	/**
	 * 添加组卷试题
	 * @param request
	 * @return
	 */
	@RequestMapping("/topicIndex")
	public ModelAndView topicIndex(HttpServletRequest request)
	{   String id = request.getParameter("id");	    
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.TOPIC_TOPICINDEX);
		TypeCount count = topicPaperBussiness.getCount(id);			
		Map<String, Object> DataMap = new HashMap<String, Object>();		
		DataMap.put("Count",count);
		DataMap.put("id",id);
		modelAndView.addAllObjects(DataMap);
		return modelAndView;
	}
	/**
	 * 添加组卷试题信息加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/topicPaper")
	public ModelAndView topicPaper(HttpServletRequest request)
	{   
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.TOPIC_TOPICEPAPWE); 
		try {
		String id = request.getParameter("id");	 
		String type = request.getParameter("type");
		String grade = request.getParameter("grade");
		String pageOffset = request.getParameter("pageOffset");
	    int pageOffsets=Integer.parseInt(pageOffset);
	    TbExamPaper paper = examPaperBussiness.findById(id);
	    String types = topicPaperBussiness.typeOf(type);
	    String grades = topicPaperBussiness.gradeOf(grade);
	    TbTopic topic = new TbTopic();
	    topic.setCategory(paper.getCourseId());
	    topic.setType(types);
	    topic.setGrade(grades);
	    topic.setPageOffset(pageOffsets);
	    ResponsePageModel<TbTopic> pageModels = topicBussiness.getPageData(topic);
	    ResponsePageModel<TbTopic> pageModel  = topicPaperBussiness.pageModelOf(pageModels);	   
	    Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}	
	/**
	 * 组卷信息加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.GENERATE_TABLELOAD);
		String id = request.getParameter("id");
		if(id!=null) {
			TbExamPaper paper = examPaperBussiness.findById(id);
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("TbExamPaper", paper);
			modelAndView.addAllObjects(dataMap);
		}
		return modelAndView;
	}
	/**
	 * 带数据的组卷信息加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableloadData")
	public ModelAndView tableLoadData(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.GENERATE_RANDOMNEXT); 
		try {
			String id = request.getParameter("id");
			TbExamPaper tbExamPaper = topicPaperBussiness.examPaperFindById(id);			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("TbExamPaper", tbExamPaper);
		    modelAndView.addAllObjects(dataMap);		    
		    Map<String, List<TbTopics>> topicPaperDataMap = new HashMap<String, List<TbTopics>>();
		    String paperId = tbExamPaper.getId();
		    List<TbTopics> listData = topicPaperBussiness.FindByPaperId(paperId);
		    List<TbTopics> lisData = topicPaperBussiness.sortType(listData);
		    topicPaperDataMap.put("ListData", lisData);
		    modelAndView.addAllObjects(topicPaperDataMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}
	/**
	 * 自动组卷信息
	 * @param request
	 * @return
	 */
	@RequestMapping("/creator")
	public ModelAndView next(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.GENERATE_RANDOMNEXT); 
		try {
			TbExamPaper tbExamPaper = ServletBeanTools.populate(TbExamPaper.class, request);
			examPaperBussiness.add(tbExamPaper);
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("TbExamPaper", tbExamPaper);
		    modelAndView.addAllObjects(dataMap);
		    Map<String, List<TbTopics>> topicPaperDataMap = new HashMap<String, List<TbTopics>>();
		    String id = tbExamPaper.getId();
		    List<TbTopics> listData = topicPaperBussiness.addTbTopicPaper(id, request);
		    List<TbTopics> lisData = topicPaperBussiness.sortType(listData);
		    topicPaperDataMap.put("ListData", lisData);
		    modelAndView.addAllObjects(topicPaperDataMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}	
	/**
	 * 组卷试题信息加编辑
	 * @param request
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editTopic(HttpServletRequest request)
	{		
		TbTopicPaper topicPaper =new TbTopicPaper();
		String id = request.getParameter("id");
		if(id!=null) {
		topicPaper =topicPaperBussiness.findByTopicPaperId(id);
		}else {
			String topicId = request.getParameter("topicId");
			TbTopic topic = topicPaperBussiness.findByTopicId(topicId);
			topicPaper.setTopics(topic);
			topicPaper.setValue(0);
			topicPaper.setType(topic.getType());
		}
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("TopicPaper", topicPaper);		
		ModelAndView modelAndView = new ModelAndView();
		if(topicPaper.getType().equals(GeneratePathUrl.TYPE_CHOICE)) {
		modelAndView.setViewName(GeneratePathUrl.TOPIC_CHOICEEDIT); 
		}else if(topicPaper.getType().equals(GeneratePathUrl.TYPE_JUDEGE)) {
		modelAndView.setViewName(GeneratePathUrl.TOPIC_JUDEGEEDIT); 
		}else {
		modelAndView.setViewName(GeneratePathUrl.TOPIC_ANSWEREDIT); 
		}
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}

	/**
	 * 删除试题
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deletes")
	public ResponseModel deletes(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			TbExamPaper tbExamPaper = topicPaperBussiness.examPaperFindByTopicPaperId(id);	
			 topicPaperBussiness.deletes(id);
			 String paperId = tbExamPaper.getId();
			 resModel.setData(paperId);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 添加组卷试题
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deleteTopics")
	public ResponseModel deleteTopics(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			TbTopicPaper paper = new TbTopicPaper();
			paper.setPaperId(id);
			List<TbTopicPaper> listData = topicPaperBussiness.queryList(paper);
			for(TbTopicPaper topicPaper : listData){
				if(type!=null) {
				    if(type.equals(topicPaper.getType())){
				    	topicPaperBussiness.delete(topicPaper.getId());
				    }
				}
			}
			resModel.setData(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 更新试卷的试题
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResponseModel updateTopic(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbTopic queryModel = ServletBeanTools.populate(TbTopic.class, request);
			if(queryModel.getType().equals(TopicUrlPath.TOPIC_CHOICE)||queryModel.getType().equals(TopicUrlPath.TOPIC_CHOICES)) {
				queryModel.setSaveResult(queryModel.getResult());
				}
			topicBussiness.updateTopic(queryModel);
			String value = request.getParameter("value");
			String id = request.getParameter("topicPaperId");
			topicPaperBussiness.updataTopicP(id, value);
			String examPaperId = topicPaperBussiness.findById(id).getPaperId();
			resModel.setData(examPaperId);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 更新试卷的试题分值
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/score")
	public ResponseModel updateValue(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
				
				String value = request.getParameter("value");
				String id = request.getParameter("topicId");				
	         	topicPaperBussiness.updataTopicP(id, value);			
				String examPaperId = topicPaperBussiness.findById(id).getPaperId();
				TbExamPaper tbExamPaper = ServletBeanTools.populate(TbExamPaper.class, request);
				examPaperBussiness.update(tbExamPaper);
				resModel.setData(examPaperId);
				resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 更新试卷的试题分值s
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/scores")
	public ResponseModel updateValues(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
				
				String value = request.getParameter("value");
				String id = request.getParameter("topicId");				
	         	topicPaperBussiness.updataTopics(id, value);			
				String examPaperId = topicPaperBussiness.findById(id).getPaperId();
				TbExamPaper tbExamPaper = ServletBeanTools.populate(TbExamPaper.class, request);
				examPaperBussiness.update(tbExamPaper);
				resModel.setData(examPaperId);
				resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 生成试卷
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/creat")
	public ResponseModel creat(HttpServletRequest request,HttpServletResponse response)
	{
		ResponseModel resModel = new ResponseModel();

		try {
			TbExamPaper tbExamPaper = ServletBeanTools.populate(TbExamPaper.class, request);
			HttpSession session = request.getSession();
			TbUser tbUser =(TbUser) session.getAttribute(UserConstant.LOGIN_USER);		
			tbExamPaper.setCreatedBy(tbUser.getUserName());
			examPaperBussiness.update(tbExamPaper);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 添加组卷试题
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addTopic")
	public ResponseModel addTopic(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String paperId = request.getParameter("id");
			String topicId = request.getParameter("topicId");
			TbTopicPaper paperTopic = new TbTopicPaper();
			TbTopic topic = topicBussiness.findById(topicId);	
			paperTopic.setTopicId(topicId);
			paperTopic.setPaperId(paperId);
			paperTopic.setType(topic.getType());
			paperTopic.setTopic(topic.getContent());
			paperTopic.setAnswer(topic.getResult());
			if(paperTopic.getValue()==null) {
				paperTopic.setValue(0);
			};
			topicPaperBussiness.add(paperTopic);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 设置试题顺序
	 * @param request
	 * @return
	 */
	@RequestMapping("/order")
	public ModelAndView setOreder(HttpServletRequest request)
	{   String id = request.getParameter("id");	    
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(GeneratePathUrl.TOPIC_SETORDER);
		TbTopicPaper paper = new TbTopicPaper();
		paper.setPaperId(id);
		List<TbTopicPaper> listData = topicPaperBussiness.queryList(paper);
		Map<String,String> btnMap = new HashMap<String, String>();
		List<String> types = new ArrayList<String>();
		for(TbTopicPaper topicPaper : listData){
			if(!btnMap.containsKey(topicPaper.getType())) {
				types.add(topicPaper.getType());
			}
			btnMap.put(topicPaper.getType(), topicPaper.getType());
		}
		Map<String,List<String>> listDatas = new HashMap<String, List<String>>();
		listDatas.put("Types", types);
		modelAndView.addAllObjects(listDatas);
		return modelAndView;
	}
	
}
