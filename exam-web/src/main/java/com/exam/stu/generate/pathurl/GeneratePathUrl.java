package com.exam.stu.generate.pathurl;
/**
 * 试卷路径
 * @author Administrator
 *
 */
public interface GeneratePathUrl {
	/**
	 * 单选类型
	 */
	String TYPE_CHOICE = "选择题(单)";
	/**
	 * 多选类型
	 */
	String TYPE_CHOICES = "选择题(多)";
	/**
	 * 判断类型
	 */
	String TYPE_JUDEGE = "判断题";
	/** 
	 * 问答类型
	 */
	String TYPE_ANSWER = "问答题";
	/**
	 *系统组卷
	 */
	String GENERATE_RANDOM = "系统组卷";
	/**
	 * 手动组卷
	 */
	String GENERATE_MANUALLY = "手动组卷";

	/**
	 * 组卷管理首页
	 */
	String GENERATE_INDEX = "/generatePaper/index";	
	/**
	 * 组卷信息加载
	 */
	String GENERATE_TABLELOAD = "/generatePaper/tableload";
	/**
	 * 手动组卷编辑加载
	 */
	String GENERATE_RANDOMNEXT = "/generatePaper/randomNext";
	/**
	 * 手动组卷编辑加载
	 */
	String GENERATE_MANUALLYVIEW = "/generatePaper/manually";
	/**
	 * 选择题（单）编辑页面加载
	 */
	String TOPIC_CHOICEEDIT = "/generatePaper/win/topicChoiceEdit";
	/**
	 * 选择题（多）编辑页面加载
	 */
	String TOPIC_CHOICESEDIT = "/generatePaper/win/topicChoicesEdit";
	/**
	 * 问答题编辑页面加载
	 */
	String TOPIC_ANSWEREDIT = "/generatePaper/win/topicAnswerEdit";
	/**
	 * 判断题编辑页面加载
	 */
	String TOPIC_JUDEGEEDIT = "/generatePaper/win/topicJudgmentEdit";
	/**
	 * 判断题编辑页面加载
	 */
	String TOPIC_TOPICEPAPWE = "/generatePaper/topicPaper";
	/**
	 * 判断题编辑页面加载
	 */
	String TOPIC_TEMPLATE = "/generatePaper/win/topicTemplate";
	/**
	 * 添加组卷试题
	 */
	String TOPIC_TOPICINDEX = "/generatePaper/topicIndex";
	/**
	 * 添加组卷试题信息加载
	 */
	String TOPIC_TOPICPAPER = "/generatePaper/topicPaper";
	/**
	 * 添加组卷试题信息加载
	 */
	String TOPIC_SETORDER = "/generatePaper/win/setOrder";

}
