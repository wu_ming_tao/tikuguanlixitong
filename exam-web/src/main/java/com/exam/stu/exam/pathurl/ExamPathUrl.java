package com.exam.stu.exam.pathurl;
/**
 * 试卷路径
 * @author Administrator
 *
 */
public interface ExamPathUrl {
	/**
	 *系统组卷
	 */
	String EXAM_RANDOM = "系统组卷";
	/**
	 * 手动组卷
	 */
	String EXAM_MANUALLY = "手动组卷";

	/**
	 * 试卷管理首页
	 */
	String EXAM_INDEX = "/examPaper/index";
	/**
	 * 试卷表格视图
	 */
	String EXAM_TABLEVIEW = "/examPaper/tableview";
	
	/**
	 * 试卷表格加载
	 */
	String EXAM_TABLELOAD = "/examPaper/tableload";
	
	/**
	 * 试卷编辑界面
	 */
	String EXAM_EDIT = "/examPaper/win/userEdit";
	/**
	 * 手动组卷页面
	 */
	String EXAM_MANUALLYEVIEW = "/examPaper/win/manually";
	/**
	 * 系统组卷页面
	 */
	String EXAM_RANDOMVIEW = "/examPaper/win/random";
	/**
	 * 系统组卷参数设置
	 */
	String EXAM_RANDONEXT = "/examPaper/win/randomNext";
	/**
	 * 浏览试卷
	 */
	String EXAM_BROWSER= "/examPaper/browser";
	/**
	 * 考试管理首页
	 */
	String EXAM_INDEXS = "/exam/index";
	/**
	 * 考试数据加载
	 */
	String EXAM_TABLELOADS = "/exam/tableload";
	/**
	 * 考试视图页面
	 */
	String EXAM_EXATHOME = "/exam/testHome";
	/**
	 * 成绩页面
	 */
	String EXAM_ANSWERCAR = "/exam/answerCar";
	/**
	 * 成绩信息加载页面
	 */
	String EXAM_TABLEOADCAR = "/exam/tableloadCar";
	/**
	 * 批改问答题
	 */
	String EXAM_CORRECT = "/exam/correct";
	/**
	 * 学生成绩
	 */
	String EXAM_SCORES = "/exam/scores";
}
