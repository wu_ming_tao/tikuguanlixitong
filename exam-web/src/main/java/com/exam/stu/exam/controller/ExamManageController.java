package com.exam.stu.exam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.exam.base.controller.BaseController;
import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.exam.business.CourseBussiness;
import com.exam.stu.exam.business.ExamBussiness;
import com.exam.stu.exam.business.ExamPaperBussiness;
import com.exam.stu.exam.business.TopicPaperBussiness;
import com.exam.stu.exam.model.TbCourse;
import com.exam.stu.exam.model.TbExam;
import com.exam.stu.exam.model.TbExamPaper;
import com.exam.stu.exam.model.TbResults;
import com.exam.stu.exam.model.TbScore;
import com.exam.stu.exam.model.TbTopicPaper;
import com.exam.stu.exam.model.TbTopics;
import com.exam.stu.exam.pathurl.ExamPathUrl;
import com.exam.stu.generate.pathurl.GeneratePathUrl;
import com.exam.stu.power.model.vo.RolePowerDataVo;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.pathurl.RoleUrlPath;
import com.exam.stu.topic.business.TopicBussiness;
import com.exam.stu.topic.model.TbTopic;
import com.exam.stu.user.business.UserBussiness;
import com.exam.stu.user.constant.UserConstant;
import com.exam.stu.user.model.TbUser;



/**
 * 试卷管理
 * @author Administrator
 *
 */
@Controller 
@RequestMapping("/exam")
public class ExamManageController extends BaseController{
	@Autowired
	private ExamPaperBussiness examPaperBussiness;
	@Autowired
	private TopicBussiness topicBussiness;
	@Autowired
	private TopicPaperBussiness topicPaperBussiness;
	@Autowired
	private ExamBussiness examBussiness;
	@Autowired
	private CourseBussiness courseBussiness;
	@Autowired
	private UserBussiness userBussiness;
	/**
	 * 试卷管理首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_INDEX); 
		return modelAndView;
	}
	/**
	 * 成绩管理首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/answerCar")
	public ModelAndView answerCar(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_ANSWERCAR); 
		return modelAndView;
	}
	/**
	 * 试卷页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableview")
	public ModelAndView tableView(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_TABLEVIEW); 
		return modelAndView;
	}
	
	/**
	 * 试卷数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_TABLELOAD); 
		examPaperBussiness.clearn();
		ResponsePageModel<TbExamPaper> pageModel = examPaperBussiness.getExamPageData(request);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	/**
	 * 试卷数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableloadCar")
	public ModelAndView tableLoadCar(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_TABLEOADCAR); 	
		ResponsePageModel<TbCourse> pageModel = courseBussiness.getCourseData(request);
		List<TbCourse> list=pageModel.getResultData();
		for(TbCourse course:list) {
			TbUser user = userBussiness.findById(course.getStudentId());
			course.setAccount(user.getAccount());
		}
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	/**
	 * 删除试卷
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResponseModel delete(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			examPaperBussiness.delete(id);
			examBussiness.delete(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 删除答题卡
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deleteCar")
	public ResponseModel deleteCar(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			courseBussiness.delete(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 浏览试卷
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/browser")
	public ModelAndView texts(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_BROWSER); 
		String paperId = request.getParameter("id"); 
		TbTopicPaper topicPaper = new TbTopicPaper();
		topicPaper.setPaperId(paperId);
		Map<String, List<TbTopics>> topicPaperDataMap = new HashMap<String, List<TbTopics>>();
		List<TbTopics> listData = examPaperBussiness.FindByPaperId(paperId);				
		List<TbTopics> lisData = topicPaperBussiness.sortType(listData);
		List<TbTopics> lisDatas = examPaperBussiness.countType(lisData);		
	    topicPaperDataMap.put("ListData", lisDatas);	    
	    modelAndView.addAllObjects(topicPaperDataMap);	    
	    return modelAndView;
	}
	
	/**
	 * 考试管理首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/examIndex")
	public ModelAndView examIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_INDEXS); 
		return modelAndView;
	}
	/**
	 * 考试页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableloads")
	public ModelAndView tableLoads(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_TABLELOADS); 		
		ResponsePageModel<TbExam> pageModel = examBussiness.getExamData(request);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	/**
	 * 考试页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/addExam")
	public ModelAndView addExam(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_INDEXS); 
		String id = request.getParameter("id");
		if(examBussiness.findById(id)==null) {
		TbExamPaper examPaper = examPaperBussiness.findById(id);		
		TbExam exam = new TbExam();
		exam.setId(id);
		exam.setExamName(examPaper.getName());
		HttpSession session = request.getSession();
		TbUser tbUser =(TbUser) session.getAttribute(UserConstant.LOGIN_USER);		
		exam.setTeacherId(tbUser.getId());
		exam.setTeacherName(tbUser.getUserName());
		examBussiness.add(exam);
		}
		return modelAndView;
	}
	/**
	 * 删除考试
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deletes")
	public ResponseModel deletes(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			String id = request.getParameter("id");
			examBussiness.delete(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 考试视图页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/test")
	public ModelAndView text(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_EXATHOME); 
		TbExam exam = new TbExam();
		List<TbExam> listData=examBussiness.queryList(exam);
		Map<String, List<TbExam>> dataMap = new HashMap<String, List<TbExam>>();
		dataMap.put("ListData", listData);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}	
	/**
	 * 考试答题卡
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/results")
	public ResponseModel testResult(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {			
			String id = request.getParameter("id");
			String type =topicPaperBussiness.findById(id).getType();
			String paperId=topicPaperBussiness.findById(id).getPaperId();
			TbTopicPaper topicPaper=topicPaperBussiness.findById(id);
			String number = request.getParameter("number");
			String result = request.getParameter("result");
			TbResults results = new TbResults();
			results.setExamId(paperId);
			results.setNumber(number);
			results.setContent(result);
			results.setTheme(topicPaper.getTopic());
			results.setExamPaperId(topicPaper.getId());
			HttpSession session = request.getSession();
			TbUser tbUser =(TbUser) session.getAttribute(UserConstant.LOGIN_USER);
			results.setId(tbUser.getId());
			results.setType(type);
			results.setValue(topicPaper.getValue());
			examBussiness.addResults(results);			
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 考试学生信息
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/course")
	public ResponseModel course(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {			
			String id = request.getParameter("id");
			String paperId=topicPaperBussiness.findById(id).getPaperId();
			TbExamPaper examPaper = examPaperBussiness.findById(paperId);
			TbExam exam = examBussiness.findById(paperId);
			HttpSession session = request.getSession();
			TbUser tbUser =(TbUser) session.getAttribute(UserConstant.LOGIN_USER);
			TbCourse score = new TbCourse();
			score.setExamId(paperId);;
			score.setName(examPaper.getName());
			score.setTeacherId(exam.getTeacherId());
			score.setTeacherName(exam.getTeacherName());
			score.setStudentId(tbUser.getId());
			score.setStudentName(tbUser.getUserName());
			examBussiness.addCourse(score);;
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 考试视图页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/correct")
	public ModelAndView correct(HttpServletRequest request)
	{
		String id = request.getParameter("id");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_CORRECT); 
		TbCourse course = courseBussiness.findById(id);
		List<TbResults> listData = examBussiness.findResults(course.getStudentId());
		Map<String, List<TbResults>> dataMap = new HashMap<String, List<TbResults>>();
		dataMap.put("ListData", listData);
		Map<String, String> countMap = new HashMap<String,String>();
		String count = listData.size()+"";		
		countMap.put("Count", count);
		modelAndView.addAllObjects(dataMap);
		modelAndView.addAllObjects(countMap);
		return modelAndView;
	}
	/**
	 * 考试学生信息
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/scores")
	public ResponseModel scores(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {			
			courseBussiness.setCourseScore(request);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	/**
	 * 考试学生信息
	 * @param request
	 * @return
	 */
	@RequestMapping("/score")
	public ModelAndView score(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ExamPathUrl.EXAM_SCORES); 
		HttpSession session = request.getSession();
		TbUser tbUser =(TbUser) session.getAttribute(UserConstant.LOGIN_USER);
		String account=tbUser.getAccount();
		TbCourse course = new TbCourse();
		course.setStudentId(tbUser.getId());
		List<TbCourse> list = courseBussiness.queryList(course);
		if(list.size()>0) 
		{
		TbCourse courses=list.get(0);
		courses.setAccount(account);
		Map<String, TbCourse> dataMap = new HashMap<String,TbCourse>();		
		dataMap.put("Course", courses);
		modelAndView.addAllObjects(dataMap);
		}
		return modelAndView;
	}
//	/**
//	 * 编辑页面
//	 * @param request
//	 * @param id
//	 * @return
//	 */
//	@RequestMapping("/browser")
//	public ModelAndView editView(HttpServletRequest request)
//	{
//		String id = request.getParameter("id"); 
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName(RoleUrlPath.ROLE_EDIT);  
//		Map<String, Object> dataMap = new HashMap<String, Object>();
//		if (!StringUtils.isEmpty(id))
//		{
//			TbRole tbRole = roleMenuBussiness.findById(id);
//			dataMap.put("TbRole", tbRole);
//			dataMap.put("method", "update");
//		}
//		else
//		{
//			TbRole tbRole = new TbRole();
//		 
//			dataMap.put("method", "add");
//			dataMap.put("TbRole", tbRole);
//		} 
//		modelAndView.addAllObjects(dataMap);
//		return modelAndView;
//	}
//		
	
//	
//	/**
//	 * 添加菜单
//	 * @param request
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping("/update")
//	public ResponseModel updateRole(HttpServletRequest request)
//	{
//		ResponseModel resModel = new ResponseModel();
//		try {
//			
//			TbRole queryModel = ServletBeanTools.populate(TbRole.class, request); 
//			roleMenuBussiness.updateRole(queryModel);
//			resModel.setRequestFlag(true);
//		} catch (Exception e) {
//			resModel.setRequestFlag(false);
//			resModel.setMessage(e.getMessage());
//		}
//		return resModel;
//	}
//	

//	
//	/**
//	 * 权限数据展示加载
//	 * @param request
//	 * @return
//	 */
//	@RequestMapping("/treeload")
//	public void treeLoad(HttpServletRequest request,HttpServletResponse response)
//	{
//		super.outJson(roleMenuBussiness.getRolePowerJsonData(request), response); 
//	}
//	
//	/**
//	 * 更新菜单权限
//	 * @param request
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping("/updateRolePower")
//	public ResponseModel updateRolePower(HttpServletRequest request)
//	{
//		ResponseModel resModel = new ResponseModel();
//		try {
//			
//			String dataStr = ServletBeanTools.convertStreamToString(request.getInputStream()); 
//			RolePowerDataVo data = JSONArray.parseObject(dataStr, RolePowerDataVo.class);
//			roleMenuBussiness.updateRoleMenu(data); 
//			 
//			resModel.setRequestFlag(true);
//		} catch (Exception e) {
//			resModel.setRequestFlag(false);
//			resModel.setMessage(e.getMessage());
//		}
//		return resModel;
//	}
}
