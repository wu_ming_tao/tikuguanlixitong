package com.exam.stu.login.pathurl;

/**
 * 登录地址常量
 * @author Administrator
 *
 */
public interface LoginConstant {
	/**
	 * 登录首页
	 */
	String LOGIN_INDEX = "login";
	
	/**
	 * 注册首页
	 */
	String REGIETER_INDEX = "register";
	
	/**
	 * 首页
	 */
	String MAIN = "main";
	
	/**
	 * 头部
	 */
	String TOP = "top";
	
	/**
	 * 左侧导航
	 */
	String LEFT = "left";
	
	/**
	 * 默认主页
	 */
	String INDEX = "index";
	
	/**
	 * 错误提示页面
	 */
	String ERROR_404 = "404";

}
