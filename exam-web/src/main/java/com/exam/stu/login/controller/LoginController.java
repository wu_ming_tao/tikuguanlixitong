package com.exam.stu.login.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
 






import com.exam.base.model.ResponseModel;
import com.exam.stu.login.pathurl.LoginConstant;
import com.exam.stu.power.business.PowerBusiness;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.user.business.LoginBusiness;
import com.exam.stu.user.constant.UserConstant;
import com.exam.stu.user.model.TbUser;

/**
 * 登录控制
 * @author Administrator
 *
 */
@Controller
@RequestMapping("")
public class LoginController {
	
	@Autowired
	private LoginBusiness loginBusiness;
	
	@Autowired
	private PowerBusiness powerBussiness;
	/**
	 * 登录首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/login")
	public ModelAndView toLogin(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(LoginConstant.LOGIN_INDEX); 
		return modelAndView;
	}
	
	/**
	 * 首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/main")
	public ModelAndView toMain(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(LoginConstant.MAIN); 
		return modelAndView;
	}
	
	/**
	 * 头部
	 * @param request
	 * @return
	 */
	@RequestMapping("/top")
	public ModelAndView toTop(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.addAllObjects(loginBusiness.getUserMap(request));
		modelAndView.setViewName(LoginConstant.TOP); 
		return modelAndView;
	}
	
	/**
	 * 头部
	 * @param request
	 * @return
	 */
	@RequestMapping("/404")
	public ModelAndView to404Error(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName(LoginConstant.ERROR_404); 
		return modelAndView;
	}
	
	/**
	 * 左部导航
	 * @param request
	 * @return
	 */
	@RequestMapping("/left")
	public ModelAndView toLeft(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		//TODO: 左侧导航菜单数据
		HttpSession session = request.getSession();  
		List<TbMenu> lstData = (List<TbMenu>) session.getAttribute(UserConstant.MENU_DATA);
		Map<String,Object> modelMap = new HashMap<String, Object>();
		modelMap.put(UserConstant.MENU_DATA, lstData);
		modelMap.put(UserConstant.ROOT_MENU, (String)session.getAttribute(UserConstant.ROOT_MENU));
		modelAndView.addAllObjects(modelMap);
		modelAndView.setViewName(LoginConstant.LEFT);  
		return modelAndView;
	}
	
	/**
	 * 默认首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(LoginConstant.INDEX);  
		return modelAndView;
	}
	
	/**
	 * 异步登录
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping("/dologin")
	public ResponseModel doLogin(HttpServletRequest request,HttpServletResponse response)
	{
		// 获取用户参数
		String userName = request.getParameter("userName");
		String passWord = request.getParameter("passWord"); 
		// 判断登录成功
		ResponseModel resModel = loginBusiness.doLogin(userName, passWord); 
		if (resModel.isRequestFlag()){
			HttpSession session = request.getSession();  
			session.setAttribute(UserConstant.LOGIN_USER, resModel.getData());
			session.setAttribute(UserConstant.MENU_DATA, powerBussiness.getUserMenu(((TbUser)resModel.getData()).getId()));
			session.setAttribute(UserConstant.ROOT_MENU, powerBussiness.getRootMenu().getId());
		}
		return resModel;
		
	} 
	/**
	 * 登出首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/logout")
	public ModelAndView logout(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		HttpSession session = request.getSession(); 
		session.removeAttribute(UserConstant.LOGIN_USER);  
		session.removeAttribute(UserConstant.MENU_DATA);  
		session.removeAttribute(UserConstant.ROOT_MENU);  
		modelAndView.setViewName(LoginConstant.LOGIN_INDEX); 
		return modelAndView;
	}
	
	/**
	 *注册页面
	 * @param request
	 * @return
	 */
	@RequestMapping("/register")
	public ModelAndView register(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(LoginConstant.REGIETER_INDEX); 
		return modelAndView;
	}
	
	/**
	 * 进行注册
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/doregister")
	public ResponseModel doRegister(HttpServletRequest request,HttpServletResponse response)
	{
		// 获取用户参数
		String userName = request.getParameter("userName");
		String passWord = request.getParameter("passWord"); 
		// 判断注册成功
		ResponseModel resModel = loginBusiness.doRegister(userName, passWord);  
		return resModel;
		
	} 
	
}
