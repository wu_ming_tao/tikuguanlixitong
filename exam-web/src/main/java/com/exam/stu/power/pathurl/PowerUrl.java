package com.exam.stu.power.pathurl;

public interface PowerUrl {
	/**
	 * 权限管控首页
	 */
	String POWER_INDEX = "/power/index";
	
	/**
	 * 权限表
	 */
	String POWER_TABLEVIEW = "/power/powertable";
	
	/**
	 * 请求地址
	 */
	String POWER_LOAD = "/power/tableload";
	
	/**
	 * 编辑地址
	 */
	String POWER_EDITVIEW = "/power/win/edit";
	
	/**
	 * 菜单按钮
	 */
	String POWER_MENU_BTNVIEW = "/power/win/menuBtn";
	
	/**
	 * 菜单按钮加载
	 */
	String POWER_MENUBTN_LOAD = "/power/win/menuBtnload";
	
	/**
	 * 菜单按钮编辑
	 */
	String POWER_MENUBTN_EDIT = "/power/win/btnEdit";
	
	/**
	 * 附属视图
	 */
	String POWER_AUTHVIEW = "/power/win/authView";
	
	/**
	 * 加载视图
	 */
	String POWRE_AUTH_LOAD = "/power/win/authload";
	
	/**
	 * 编辑视图
	 */
	String POWER_AUTH_EDIT = "/power/win/authEdit";
}
