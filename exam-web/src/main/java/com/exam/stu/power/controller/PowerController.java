package com.exam.stu.power.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.exam.base.controller.BaseController; 
import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.power.business.PowerAttachBussiness;
import com.exam.stu.power.business.PowerBusiness;
import com.exam.stu.power.business.PowerMenuBtnBusiness;
import com.exam.stu.power.model.TbAuth;
import com.exam.stu.power.model.TbBtn;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.power.pathurl.PowerUrl;

/**
 * 权限控制界面
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/power")
public class PowerController extends BaseController { 
	
	@Autowired
	private PowerBusiness powerBusiness;
	
	@Autowired
	private PowerMenuBtnBusiness powerMenuBtnBusiness;
	
	@Autowired
	private PowerAttachBussiness powerAttachBussiness;
	
	/**
	 * 登录首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_INDEX); 
		return modelAndView;
	}
	
	/**
	 * 权限数据展示加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/treeload")
	public void treeLoad(HttpServletRequest request,HttpServletResponse response)
	{
		super.outJson(powerBusiness.getMenuJsonData(), response); 
	}
	
	/**
	 * 登录首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableview")
	public ModelAndView tableView(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_TABLEVIEW); 
		return modelAndView;
	}
	
	/**
	 * 权限表数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_LOAD); 
		ResponsePageModel<TbMenu> pageModel = powerBusiness.getMenuPageData(request);
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editView(HttpServletRequest request)
	{
		String id = request.getParameter("id");
		String parentId = request.getParameter("parentId");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_EDITVIEW);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{
			TbMenu tbMenu = powerBusiness.findById(id);
			dataMap.put("TbMenu", tbMenu);
			dataMap.put("method", "update");
		}
		else
		{
			TbMenu tbMenu = new TbMenu();
			tbMenu.setParentId(parentId);
			dataMap.put("method", "add");
			dataMap.put("TbMenu", tbMenu);
		} 
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public ResponseModel addMenu(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try { 
			TbMenu queryModel = ServletBeanTools.populate(TbMenu.class, request); 
			powerBusiness.addMenu(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResponseModel updateMenu(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbMenu queryModel = ServletBeanTools.populate(TbMenu.class, request); 
			powerBusiness.updateMenu(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 删除清单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResponseModel deleteMenu(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			powerBusiness.deleteMenu(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 按钮首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/toMenuBtn")
	public ModelAndView toMenuBtnIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		String id = request.getParameter("id");
		modelAndView.setViewName(PowerUrl.POWER_MENU_BTNVIEW); 
		modelAndView.addObject("id", id);
		return modelAndView;
	}
	
	/**
	 * 权限表数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/menuBtnload")
	public ModelAndView MenuBtnLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_MENUBTN_LOAD); 
		ResponsePageModel<TbBtn> pageModel = powerBusiness.getMenuBtnPageData(request); 
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/btnEdit")
	public ModelAndView btnEditView(HttpServletRequest request)
	{
		String id = request.getParameter("id");
		String parentId = request.getParameter("parentId");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_MENUBTN_EDIT);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{
			TbBtn tbBtn = powerMenuBtnBusiness.findById(id);
			dataMap.put("TbBtn", tbBtn);
			dataMap.put("method", "updateBtn");
		}
		else
		{
			TbBtn tbBtn = new TbBtn();
			tbBtn.setMenuId(parentId);
			dataMap.put("method", "addBtn");
			dataMap.put("TbBtn", tbBtn);
		} 
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}

	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addBtn")
	public ResponseModel addBtn(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try { 
			TbBtn queryModel = ServletBeanTools.populate(TbBtn.class, request); 
			powerMenuBtnBusiness.addBtn(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateBtn")
	public ResponseModel updateBtn(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbBtn queryModel = ServletBeanTools.populate(TbBtn.class, request); 
			powerMenuBtnBusiness.updateBtn(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 删除清单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deleteBtn")
	public ResponseModel deleteBtn(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			powerMenuBtnBusiness.deleteBtn(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	
	/**
	 * 按钮首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/toAuth")
	public ModelAndView toAuthIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		String id = request.getParameter("id");
		modelAndView.setViewName(PowerUrl.POWER_AUTHVIEW); 
		modelAndView.addObject("id", id);
		return modelAndView;
	}
	
	/**
	 * 权限表数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/authload")
	public ModelAndView authLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		try { 
			modelAndView.setViewName(PowerUrl.POWRE_AUTH_LOAD); 
			TbAuth queryModel = ServletBeanTools.populate(TbAuth.class, request);
			ResponsePageModel<TbAuth> pageModel =  powerAttachBussiness.getPageData(queryModel);
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("pageModel", pageModel);
			modelAndView.addAllObjects(dataMap);
		} catch (Exception e) {
			 e.printStackTrace();
		}
		
		return modelAndView;
	}
	
	/**
	 * 编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/authEdit")
	public ModelAndView authEditView(HttpServletRequest request)
	{
		String id = request.getParameter("id");
		String parentId = request.getParameter("parentId");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(PowerUrl.POWER_AUTH_EDIT);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{
			TbAuth tbAuth = powerAttachBussiness.findById(id);
			dataMap.put("TbAuth", tbAuth);
			dataMap.put("method", "updateAuth");
		}
		else
		{
			TbAuth tbAuth = new TbAuth();
			tbAuth.setMainId(parentId);
			dataMap.put("method", "addAuth");
			dataMap.put("TbAuth", tbAuth);
		} 
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}

	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addAuth")
	public ResponseModel addAuth(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try { 
			TbAuth queryModel = ServletBeanTools.populate(TbAuth.class, request); 
			powerAttachBussiness.add(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateAuth")
	public ResponseModel updateAuth(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbAuth queryModel = ServletBeanTools.populate(TbAuth.class, request); 
			powerAttachBussiness.update(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 删除清单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/deleteAuth")
	public ResponseModel deleteAuth(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			powerAttachBussiness.delete(id);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	
}
