package com.exam.stu.user.pathurl;

/**
 * 用户路径
 * @author Administrator
 *
 */
public interface UserPathUrl {
	
	/**
	 * 用户首页
	 */
	String USER_INDEX = "/user/index";
	
	/**
	 * 表格加载
	 */
	String USER_TABLELOAD = "/user/tableload";
	
	/**
	 * 编辑界面
	 */
	String USER_EDIT = "/user/win/userEdit";
}
