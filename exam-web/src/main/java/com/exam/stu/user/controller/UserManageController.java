package com.exam.stu.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.exam.base.model.ResponseModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.util.ServletBeanTools;
import com.exam.stu.power.model.TbMenu;
import com.exam.stu.power.pathurl.PowerUrl;
import com.exam.stu.role.business.RoleMenuBussiness;
import com.exam.stu.role.model.TbRole;
import com.exam.stu.role.model.TbUserRole;
import com.exam.stu.role.pathurl.RoleUrlPath;
import com.exam.stu.role.service.TbUserRoleService;
import com.exam.stu.user.business.UserBussiness;
import com.exam.stu.user.model.TbUser;
import com.exam.stu.user.pathurl.UserPathUrl;

/**
 * 用户管理
 * @author Administrator
 *
 */
@Controller 
@RequestMapping("/user")
public class UserManageController {
	@Autowired
	private UserBussiness userBussiness;
	@Autowired
	private TbUserRoleService tbUserRoleService;
	/**
	 * 用户首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView toIndex(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(UserPathUrl.USER_INDEX); 
		return modelAndView;
	}
	
	/**
	 * 权限表数据加载
	 * @param request
	 * @return
	 */
	@RequestMapping("/tableload")
	public ModelAndView tableLoad(HttpServletRequest request)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(UserPathUrl.USER_TABLELOAD); 
		ResponsePageModel<TbUser> pageModel = userBussiness.getUserPageData(request);
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("pageModel", pageModel);
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 编辑页面
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView editView(HttpServletRequest request)
	{
		String id = request.getParameter("id"); 
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(UserPathUrl.USER_EDIT);  
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(id))
		{
			TbUser tbUser = userBussiness.findByUserId(id);
			dataMap.put("TbUser", tbUser);
			dataMap.put("method", "update");
		}
		else
		{
			TbUser tbUser = new TbUser();
			tbUser = userBussiness.generateUser(tbUser);
			dataMap.put("method", "add");
			dataMap.put("TbUser", tbUser);
		} 
		modelAndView.addAllObjects(dataMap);
		return modelAndView;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public ResponseModel addRole(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try { 
			TbUser queryModel = ServletBeanTools.populate(TbUser.class, request); 
			userBussiness.addUser(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResponseModel updateRole(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			TbUser queryModel = ServletBeanTools.populate(TbUser.class, request); 
			userBussiness.updateUser(queryModel);
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
	
	/**
	 * 添加菜单
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResponseModel deleteRole(HttpServletRequest request)
	{
		ResponseModel resModel = new ResponseModel();
		try {
			
			String id = request.getParameter("id");
			userBussiness.delete(id);
			TbUserRole userRole=new TbUserRole();
			userRole.setUserId(id);
			List<TbUserRole> list=tbUserRoleService.queryList(userRole);
			if(list.size()>0){
			for(TbUserRole tbUser :list) {
				tbUserRoleService.delete(tbUser.getId());
			}
			}
			resModel.setRequestFlag(true);
		} catch (Exception e) {
			resModel.setRequestFlag(false);
			resModel.setMessage(e.getMessage());
		}
		return resModel;
	}
}
