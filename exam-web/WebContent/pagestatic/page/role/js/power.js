
// 初始化
$(function(){
	tableLoad();
});

// 表格加载
function tableLoad(){
	var treeId = $('#tableArea').val();
	var data = {
		treeId : treeId	
	};
	
	var loadtip = xtip.load('加载中...')
    $.ajax({
        //发送请求URL，可使用相对路径也可使用绝对路径
        url: mainActionUrl + "/role/tableview.do",
        //发送方式为GET，也可为POST，需要与后台对应
        type:"POST",
        //设置接收格式 
        dataType:"text",
        cache: false,
        timeout:30000,
        traditional:true,
        async: true,
      	//处理数组
        traditional:true, 
        //向后台发送的数据
        data:data,
        //后台返回成功后处理数据，data为后台返回的json格式数据
        success:function(data){
        	xtip.close(loadtip);
        	$('#tableArea').html( data );
        } ,
        error: function( data ) {
        	xtip.close(loadtip); 
        	 xtip.alert(data.message,'e');  
        }
    }); 
}


