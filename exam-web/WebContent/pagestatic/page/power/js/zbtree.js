
var firstFlag = false;
var selectContenType;
var selectTree;
var editOldTreeText;
var isRootNode = false; 
var mainActionUrl;
// 初始化树
function initTree(id){
	$('#' + id).tree({
		 url: mainActionUrl + "role/treeload.do",
		lines:false,
		animate:true, 
		onLoadSuccess:function(node, data){ 
			firstFlag = true; 
			 
		},
		onBeforeSelect:function(node){
			 
		},
		onSelect:function(node){    
            $("#treeid").val(node.id) ; 
            tableLoad();
		},
		onDblClick:function(node){ 
			 
		},
		onAfterEdit:function(node){ 
			  
		}
	});
}
// 获取当前选中节点编号
function getCurNodeId(){ 
	var node = $("#mainTree").tree("getSelected");
	if (node != null && typeof(node)!="undefined"){ 
		return node.id;
	}
	return null;
}

// 加载数据
function load(){
	initTree("mainTree");    
}

function getRoot(){
	var root = $('#mainTree').tree("getRoot");
	return root;
}

// 删除当前树节点
function treeDelete(){
	var curId = getCurNodeId();
	if (curId != null && typeof(curId)!="undefined"){ 
		var parent = getParentTree(curId); 
	      if (parent == null || typeof(curId) =="undefined"){
	    	  $.bw.notify('不能删除根节点','error');
	    	  return;
	      }
		$.bw.confirm("请确定是否删除本结构！！！",
				{title:"确认消息",
			     confirm:function(){
			    	 
			    	 var data = {
								'id': curId
							};
							com_ajaxPost( mainActionUrl + '?method=treeDelete', data, function( data ) {
								  if (data.resultBool){
									  $.bw.notify('删除成功','success');
									  var node = getTreeNodeById(curId);
									  $('#mainTree').tree("remove",node.target);
									  tableLoad();
								  }
							} );
			   } 
		});
		return;
	}
	$.bw.notify('请选中节点','error');
}
// 添加树节点
function treeAdd(){
	var curId = getCurNodeId();
	if (curId != null && typeof(curId)!="undefined"){
		var parent = getParentTree(curId); 
	      if (parent == null || typeof(curId) =="undefined"){
	    	  isRootNode = true;
// 	    	  $.bw.notify('不能添加根节点','error');
// 	    	  return;
	      } else {
	    	  isRootNode = false;
	      }
		bWin= $.bw.window(mainActionUrl + '?method=treeAddWindow&id=' + curId,
				{title:'${itemTypeName}结构新增',iframe:true,width:350,height:280,success:function(){
					 
				}});
		return;
	}
	$.bw.notify('请选中节点','error');
}

// 添加树节点
function appendTree(data,type,curId){
	// 同级新增
	if (type == 'equal'){
		var node = getParentTree(curId);
		if (node){
			$('#mainTree').tree('append', {
				parent: node.target,
				data: data
			});
		}
		return;
	}
	// 子级新增
	if (type == 'child'){
		var node = getTreeNodeById(curId);
		
		if (node){
			$('#mainTree').tree('append', {
				parent: node.target,
				data: data
			});
		}
	}  
}

// 上移树节点
function treeUp(){
 
	var curId = getCurNodeId();
	var node = getTreeNodeById(curId);
	if (curId != null && typeof(curId)!="undefined"){ 
		var parentNode = getParentTree(node.id);
		var childs =  getSubChildren(parentNode);
		 var index = 0; 
		 for (index = 0; index <childs.length; index ++){
	        if (childs[index].id == node.id)
			{
			   break;
			}
	     } 
		 if (index == 0){
			$.bw.notify('不能移动第一个节点','error'); 
			return;
		 }
		 // 上一个节点
		 var preIndex = index - 1; 
		 var preNode = childs[preIndex];
		 
		 var data = {
					'id': curId,
					'preid': preNode.id
				};
		 com_ajaxPost( mainActionUrl + '?method=treeChange', data, function( data ) {
			  if (data.resultBool){
				  // 先移除
					 var data = $("#mainTree").tree("pop",node.target); 
					 // 再往上一个节点插入数据
					 $('#mainTree').tree('insert', {
						before: preNode.target,
						data: data
					});
			  }
		} ); 
		return;
	}
	$.bw.notify('请选中节点','error');
}
// 下移树节点
function treeDown(){
	var curId = getCurNodeId();
	var node = getTreeNodeById(curId);
	if (curId != null && typeof(curId)!="undefined"){ 
		var parentNode = getParentTree(node.id);
		 var childs =  getSubChildren(parentNode);
		 var index = 0;
		 
		 for (index = 0; index < childs.length; index ++){
	        if (childs[index].id == node.id)
			{
			   break;
			}
	     } 
		 if (index == childs.length - 1){
			 $.bw.notify('不能移动最后的节点','error'); 
			return;
		 }
		 var lastIndex = index + 1;
		 
		 var lastNode = childs[lastIndex];
		 // 后台交换顺序字符 必须查两次
		 		 var data = {
					'id': curId,
					'preid': lastNode.id
				};
		 com_ajaxPost( mainActionUrl + '?method=treeChange', data, function( data ) {
			  if (data.resultBool){
				  // 先移除
				 var data = $("#mainTree").tree("pop",node.target); 
					 // 再往上一个节点插入数据
					 $('#mainTree').tree('insert', {
						after: lastNode.target,
						data: data
					});
			  }
		} ); 
		 
		return;
	}
	$.bw.notify('请选中节点','error');
}

// 是否是叶子节点
function isLeaf(node){
	var flag = $('#mainTree').tree('isLeaf', node.target);
	return flag;
}

//某节点的一级节点
function getSubChildren(node){
     
    var subNodes = [];
    $(node.target)
    .next().children().children("div.tree-node").each(function(){   
        subNodes.push($("#mainTree").tree('getNode',this));
    });
   return subNodes;
}


// 设置选中树
function selectTree(id){
	var node = getTreeNodeById(id);//找到id为”mainTree“这个树的节点id为”1“的对象
	$('#mainTree').tree('select', node.target);//设置选中该节点
}

// 通过ID获取当前节点
function getTreeNodeById(id){
	var node = $('#mainTree').tree('find', id);
	return node;
}

// 获取当前节点的父节点
function getParentTree(id){
	var node = getTreeNodeById(id);//找到id为”mainTree“这个树的节点id为”1“的对象
	var nodePar = $('#mainTree').tree("getParent",node.target);
	return nodePar;
}

// 初始化加载
$(function(){
	mainActionUrl = $('#basePath').val();
	load();
})

function getisRootNode(){
	return isRootNode;
} 
