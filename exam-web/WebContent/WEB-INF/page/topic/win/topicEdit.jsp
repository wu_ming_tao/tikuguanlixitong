<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>试题编辑</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>
<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>

<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
<link href="<%=basePath %>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
	select {
	
	text-align: center;

text-align-last: center;
	
	}
</style>
</head>

<body>     	 
    <div class="formbody">    
    <div class="formtitle"><span>选择创建试题题型</span></div>   
    <form id="addMennForm">
      <input name="id" type="hidden" class="dfinput" value="" /> 
      <ul class="forminfo" >
	    <li>
	       <label>试题类别</label>
	       <input name="topicCategory" type="text" class="dfinput" value="${xtipid}"  style="width:220px" /><i></i>
	    </li>
	    
	    <li>
	    <label>题型</label>
	    <select class="form-control" id="type" style="width:160px" >
		  <option value="choice">选择题（单）</option>
		  <option value="choices">选择题（多）</option>
		  <option value="judgment">判断题</option>
		  <option value="answer">问答题</option>
		</select><i>默认为选择题（单）</i>
		</li> 		  
	   </ul>
    </form>
    </div> 
      <ul class="forminfo">
       <li><label>&nbsp;&nbsp;&nbsp;</label><input name="" type="button" class="btn" value="确认保存" onclick="confirm()" /></li>
      </ul>  
    <script type="text/javascript"> 
    
    function confirm(){
    	
    	 var parent = window.parent; //获取父窗口
    	  var selfid = self.frameElement.getAttribute('parentid'); //获取本窗口id
    	  parent.xtip.close(123); //关闭本窗口
    }
        
     function confirms(){
    
    	//var type="choice";
    	//var data = {
		//			type:type
		//	};
    	var loadtip = xtip.load('加载中...')
        $.ajax({
            //发送请求URL，可使用相对路径也可使用绝对路径
            url: "<%=basePath %>topic/tabletypeload.do",
            //发送方式为GET，也可为POST，需要与后台对应
            type:"POST",
            //设置接收格式 
            dataType:"json",
            cache: false,
            timeout:30000,
            traditional:true,
            async: true, 
            //向后台发送的数据
            data:data,
            //后台返回成功后处理数据，data为后台返回的json格式数据
            success:function(data){
            	xtip.close(loadtip);  
            	xtip.close(xipid); 
            	  if (!data.requestFlag){
               	   xtip.alert(data.message,'e'); 
                  }else{
                	  xtip.alert('操作成功','s');                 
                  } 
            } ,
            error: function( data ) {
            	xtip.close(loadtip);
            	xtip.close(xipid); 
            	 xtip.alert(data.message,'e');  
            }
        }); 
    	
    }
    
    		
    </script>
    
</body>
</html>

