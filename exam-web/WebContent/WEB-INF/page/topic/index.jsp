<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/icon.css"> 
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.easyui.min.js"></script>


<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
 
 <link href="<%=basePath %>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath %>static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


<style>

img { 
    height: auto;
    max-width: 100%; 
    border: 0;
    vertical-align: top;
    -ms-interpolation-mode: bicubic;
}
</style>

</head>


<body>
<input name="id" type="hidden" class="dfinput" value="" /> 
 		<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">试题管理</a></li>
    <li><a href="#">试题列表</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar" style="margin: 0 0;">
        <li class="click" onclick="topicEdit()"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
       <li class="click" onclick="flesh()"><span><img src="<%=basePath %>pagestatic/images/t04.png" /></span>刷新</li>
        </ul>
        
        
       <ul class="toolbar1" style="background: 0;border: 0">
        <li style="padding-right: 0;margin:0"><input id="topicQuery" type="text" class="query" placeholder="请输入查询内容" style="height: 35px;padding-right: 0;margin:0"/></li>
        <li style="padding-right: 0;margin:0"><input name="" type="button" class="scbtn" value="查询" style="width:70px; padding-right: 0;" onclick="finds()" /></li>
       </ul>
     <div class="btn-group"style="display:inline-block; float: right;margin: 0 5px;border: 0;border: solid 1px #cbcbcb;">
		<button type="button" class="btn btn-default"style="height: 32px;width: 64px;border: 0;">题型</button>
		<button type="button" class="btn btn-default dropdown-toggle" 
				data-toggle="dropdown"style="height: 32px;width: 26px;">
			<span class="caret"></span>
			<span class="sr-only"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" onclick="a('1')">选择题</a></li>
			<li><a href="#" onclick="a('2')">判断题</a></li>
			<li><a href="#" onclick="a('3')">问答题</a></li>
			<li><a href="#" onclick="a('4')">全部</a></li>
		</ul>
	   </div>
	   
	   <div class="btn-group"style="display:inline-block; float: right;margin: 0 5px;border: 0;border: solid 1px #cbcbcb;">
		<button type="button" class="btn btn-default"style="height: 32px;width: 64px;border: 0;">难度</button>
		<button type="button" class="btn btn-default dropdown-toggle" 
				data-toggle="dropdown"style="height: 32px;width: 26px;">
			<span class="caret"></span>
			<span class="sr-only"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" onclick="b('1')">低</a></li>
			<li><a href="#" onclick="b('2')">一般</a></li>
			<li><a href="#" onclick="b('3')">高</a></li>
			<li><a href="#" onclick="b('4')">全部</a></li>
		</ul>
	   </div>
	   	   
    </div>
    </div>
    
    
    <table class="imgtable">
    
    <thead>
    <tr>
    <th>试题科类</th>
    <th>试题内容</th>
    <th>题型</th>
    <th>难度</th>   
    <th width="300px;">操作按钮</th> 
    </tr>
    </thead>
    
    <tbody id="tabletb">
     
    </tbody>
    <tbody id="tablequer">
     
    </tbody>  
    </table>
       
    </div>
    
    <div>
      
    </div>
    
    
    
<script>
	$(function(){
		tableDataLoad();
	});
	var category;
	var grade = 0;
	var type = 0;
	var pageOffset = 1;
	function tableDataLoad(pageItem){ 
		
		if (pageItem){
			var data = { 
					type:type,
					grade:grade,
					pageOffset:pageItem,
					category:category
			};
			pageOffset = pageItem;
		}
		else{
			var data = { 
					type:type,
					grade:grade,
					pageOffset:pageOffset,
					category:category
			};
		}
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url:"<%=basePath %>topic/tableload.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);	      
	        	$('#tabletb').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	            xtip.alert(data.message,'e');  
	        }
	    }); 
	}
		// 添加
		function topicEdit(){ 
			  
		xtip.win({
				type:'confirm',
				tip: '请选择添加的试题题型',
				icon: 'success',
				btn: ['选择题(单)','选择题(多)','判断题','问答题'],
				title: '【选择试题题型】',
				width: '400px',
				height: '600px',
				min: true,
				shade: false,
				max: true,
				lock: false,
				closeBtn: true,
				btn1: function(){
					var type = "选择题(单)";
					xtip.open({
						type: 'url',
						content : '<%=basePath%>topic/edit.do?type=' + type,
						title: '【编辑单选试题】',
						width: '800px',
						height: '720px',
						min: true,
						shade: false,
						max: true,
						lock: true,
						closeBtn: true,
						}); 
				},
				<%-- btn2: function(){
					var type = "选择题(多)";
					xtip.open({
						type: 'url',
						content : '<%=basePath%>topic/edit.do?type=' + type,
						title: '【编辑多选试题】',
						width: '800px',
						height: '720px',
						min: true,
						shade: false,
						max: true,
						lock: true,
						closeBtn: true,
						});
				}, --%>
				btn2: function(){
				
					var type = "判断题";
					xtip.open({
						type: 'url',
						content : '<%=basePath%>topic/edit.do?type=' + type,
						title: '【编辑判断试题】',
						width: '800px',
						height: '700px',
						min: true,
						shade: false,
						max: true,
						lock: true,
						closeBtn: true,
						});
				},
				btn3: function(){
					
					var type = "问答题";
					xtip.open({
						type: 'url',
						content : '<%=basePath%>topic/edit.do?type=' + type,
						title: '【编辑问答试题】',
						width: '700px',
						height: '600px',
						min: true,
						shade: false,
						max: true,
						lock: true,
						closeBtn: true,
						});
				},
				}); 
		}
		// 更新
		function topicEdits(obj){ 
			var id = $( obj ).attr("attachId");
			xtip.open({
				type: 'url',
				content : '<%=basePath%>topic/edit.do?id=' + id,
				title: '【更新试题】',
				width: '800px',
				height: '700px',
				min: true,
				shade: false,
				max: true,
				lock: true,
				closeBtn: true,
				}); 
		}
	
		// 删除
		function topicDelete(obj){
			var id = $( obj ).attr("attachId"); 
			var data = {
				id:id
			};
			var loadtip = xtip.load('删除中...');
		    $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>topic/delete.do?id=" + id,
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip);
		        	xtip.alert('删除成功','s');  
		        	tableDataLoad();
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }
		    }); 
		}
		// 查询
		function finds(){
			category=$("#topicQuery").val();			
			tableDataLoad();
		}
	
	
		function a(x){			
			if(x){
				 type=x;
			} 
			pageOffset = 1;
			tableDataLoad();
		}
		function b(x){
					
			if(x){
				 grade=x;
			} 
			pageOffset = 1;
			tableDataLoad();
		}
		function flesh(){
			category="";
			grade = 0;
			type = 0;
		    pageOffset = 1;
		    tableDataLoad();
		}
</script> 
 
 
    
    
    
 	 		
</body>
</html>
