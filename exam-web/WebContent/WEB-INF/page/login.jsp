﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登陆</title>
<link href="pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="pagestatic/js/jquery.js"></script>
<script src="pagestatic/js/cloud.js" type="text/javascript"></script>
<script src="static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script language="javascript">
	$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    })  
});  
</script> 
<style>

</style>
</head>

<body style="background-color:#1c77ac; background-image:url(images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">



    <div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
    </div>  


<div class="logintop">    
    <span>欢迎登录考试系统平台</span>    
    <ul> 
    </ul>    
    </div>
    
    <div class="loginbody">
    
    <span class="systemlogo"style="font-family: cursive;font-size: 26pt;text-align: center;background: content-box;">题库管理系统登录页面</span> 
       
    <div class="loginbox">
    
    <ul>
    <li><input id="loginName" name="loginName" type="text" class="loginuser" value="" placeholder="请输入账号" /></li>
    <li><input id="loginPassword" name="loginPassword" type="password" class="loginpwd" value="" placeholder="请输入密码"/></li>
    <li><input name="" type="button" class="loginbtn" value="登录"  onclick="doLogin()"  />
     </li>
    </ul>
    <div class="content"style="text-align: center;">
     <small>没有账号?</small><a href="register.do" class="signup">&nbsp;注册</a>
     </div>
    </div>
    
    </div>
     
   
    
    <script type="text/javascript">
      // 登陆操作
      function doLogin(){
    	 var userName=$("#loginName").val();
		 var passWord=$("#loginPassword").val();
		 
		 if (!userName){
			 xtip.alert('账号不能为空！','e'); 
			 return;
		 }
		 
		 if (!passWord){
			 xtip.alert('密码不能为空！','e'); 
			 return;
		 } 
    	  	var data ={
        	      userName : userName,
        	      passWord : passWord
        		}; 
        		var loadtip = xtip.load('登录中...')
                $.ajax({
                    //发送请求URL，可使用相对路径也可使用绝对路径
                    url:"dologin.do",
                    //发送方式为GET，也可为POST，需要与后台对应
                    type:"POST",
                    //设置接收格式为JSON
                    dataType:"json",
                    cache: false,
                    async: true,
                  	//处理数组
                    traditional:true, 
                    //向后台发送的数据
                    data:data,
                    //后台返回成功后处理数据，data为后台返回的json格式数据
                    success:function(data){
                    	xtip.close(loadtip);
                       if (!data.requestFlag){
                    	   xtip.alert(data.message,'e'); 
                       }else{
                    	   window.location.href="main.do";
                       }
                    } 
                }); 
    	  
      }
    </script>
    
</body>
</html>
