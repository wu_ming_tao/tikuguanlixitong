<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/icon.css"> 
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.easyui.min.js"></script>


<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
 
 
<script type="text/javascript" src="<%=basePath %>pagestatic/page/role/js/zbtree.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/page/role/js/power.js"></script>


<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>

<style>

img { 
    height: auto;
    max-width: 100%; 
    border: 0;
    vertical-align: top;
    -ms-interpolation-mode: bicubic;
}
</style>

</head>


<body>
<input id="basePath" type="hidden" value = "<%=basePath %>"/> 
<input id="roleId" type="hidden" value = ""/> 
    	   <div class="leftfixed">
				<div class="leftfixedrelative">
					<div class="zkmovediv"> 
					</div> 
					<ul class="easyui-tree" id="mainTree">
 
					</ul>
				</div>
		 </div>
		 
		 <div class="fightContent" >
			<div class="columnbar">
				<h5>角色权限</h5>
			</div>
			<div class="columnbox mt10" style="">
				<div id="MoveIdbox">
				     
					<div class="columnmodule mt10">
						<div class="columnbox">
							 
						</div>
						 <div id="tableArea" class="data-area">
						     
						 </div>
					</div>
				</div>
			</div>
		</div>
		
		
		
</body>
</html>
