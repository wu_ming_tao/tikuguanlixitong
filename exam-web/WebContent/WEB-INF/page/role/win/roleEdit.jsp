<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
</head>

<body>
 
    <div class="formbody">
    
    <div class="formtitle"><span>角色信息</span></div>
    
    <form id="addMennForm">
      <input name="id" type="hidden" class="dfinput" value="${TbRole.id}" /> 
      <ul class="forminfo">
	    <li>
	       <label>角色名称</label>
	       <input name="roleName" type="text" class="dfinput" value="${TbRole.roleName}" /><i></i>
	    </li>
	    
	    <li>
	    <label>角色编码</label>
	    <input name="roleCode" type="text" class="dfinput"  value="${TbRole.roleCode}" /><i></i>
	    </li> 
	   </ul>
    </form> 
      <ul class="forminfo">
       <li><label>&nbsp;</label><input name="" type="button" class="btn" value="确认保存" onclick="saveAdd()" /></li>
      </ul>
    </div>
    
    <script type="text/javascript"> 
     function saveAdd(){
    	var data = serializeObject($("#addMennForm")); 
    	var loadtip = xtip.load('加载中...')
        $.ajax({
            //发送请求URL，可使用相对路径也可使用绝对路径
            url: "<%=basePath %>role/${method}.do",
            //发送方式为GET，也可为POST，需要与后台对应
            type:"POST",
            //设置接收格式 
            dataType:"json",
            cache: false,
            timeout:30000,
            traditional:true,
            async: true, 
            //向后台发送的数据
            data:data,
            //后台返回成功后处理数据，data为后台返回的json格式数据
            success:function(data){
            	xtip.close(loadtip);  
            	
            	  if (!data.requestFlag){
               	   xtip.alert(data.message,'e'); 
                  }else{
                	  xtip.alert('操作成功','s'); 
                	  window.parent.tableDataLoad();
                  } 
            } ,
            error: function( data ) {
            	xtip.close(loadtip); 
            	 xtip.alert(data.message,'e');  
            }
        }); 
    	
    }
   
    		
    </script>
    
</body>
</html>
