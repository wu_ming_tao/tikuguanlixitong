<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">角色权限</a></li>
    <li><a href="#">角色列表</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar">
        <li class="click" onclick="roleAdd()"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
       <li class="click" onclick="treeRefresh()"><span><img src="<%=basePath %>pagestatic/images/t04.png" /></span>刷新树</li>
        </ul>
        
        
        <ul class="toolbar1">
        
        </ul>
    
    </div>
    
    
    <table class="imgtable">
    
    <thead>
    <tr>
    <th width="300px;">操作按钮</th>
    <th>角色名称</th>
    <th>角色编码</th> 
    </tr>
    </thead>
    
    <tbody id="tabletb">
     
    </tbody> 
    </table>
       
    </div>
    
    <div>
      
    </div>
    
   
<script>
	$(function(){
		tableDataLoad();
	});
	
	function treeRefresh(){
		$('#treeid').val('');
		load();
		tableDataLoad();
	}
	var pageOffset = 1;
	function tableDataLoad(pageItem){ 
		var treeId = $('#treeid').val();
		
		if (pageItem){
			var data = {
					parentId : treeId,
					pageOffset:pageItem
			};
			pageOffset = pageItem;
		}
		else{
			var data = {
					parentId : treeId
			};
		}
		
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: mainActionUrl + "/role/tableload.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	$('#tabletb').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    }); 
		
	}
	
	// 添加
	function roleAdd(){ 
		  
		xtip.open({
			type: 'url',
			content : '<%=basePath%>role/edit.do',
			title: '【添加角色】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	// 更新
	function roleEdit(obj){ 
		var id = $( obj ).attr("attachId");
		xtip.open({
			type: 'url',
			content : mainActionUrl + '/role/edit.do?id=' + id,
			title: '【修改角色】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	// 删除
	function roleDelete(obj){
		var id = $( obj ).attr("attachId"); 
		var data = {
			id:id
		};
		var loadtip = xtip.load('删除中...');
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath%>role/delete.do?id=" + id,
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"json",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	xtip.alert('删除成功','s');  
	        	tableDataLoad();
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    });  
	}
	
	function rolePowerfresh(obj){
		var loadtip = xtip.load('加载中...')
		var roleId = $( obj ).attr("attachId");  
		$( '#roleId' ).val(roleId);  
		debugger
		load(roleId);
		xtip.close(loadtip); 
	}
	
	function addrolePower(obj){
		var tbRoleId = $( obj ).attr("attachId"); 
		var roleId = $( '#roleId' ).val();  
		if (tbRoleId != roleId){
			 xtip.alert('请先刷新当前操作树！','e');  
			 return;
		} 
		var selectTreeData = getselectTreeData(); 
		
		var data = JSON.stringify({
			roleId : roleId,
		    treeData : selectTreeData
		}); 
		
		var loadtip = xtip.load('加载中...')
		 $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>role/updateRolePower.do",
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip);
		        	xtip.alert('更新成功','s');   
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }
		    });  
	}
	 
	
</script> 
 
 
    
    
    
 