<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/icon.css"> 
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.easyui.min.js"></script>


<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>

<style>

img { 
    height: auto;
    max-width: 100%; 
    border: 0;
    vertical-align: top;
    -ms-interpolation-mode: bicubic;
}
h2 {
  margin: 20px 0 40px 0;
}
.classlist {
 margin: 20px 20px;
}
.classlist li {
   margin: 20px 20px;
}
</style>

</head>
   <body>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">成绩管理</a></li>
    <li><a href="#">成绩页面</a></li>
    </ul>
    </div>
     <div class="rightinfo">
  
    <table class="imgtable">
    
    <thead>
    <tr>
    <th>学生账号</th>
    <th>学生姓名</th>  
    <th>试卷名称</th>
    <th>任课教师</th>
    <th>成绩</th>
    </tr>
    </thead>
    
    <tbody id="tabletb">

     <tr>
        <td>${Course.account}</td>
        <td>${Course.studentName}</td> 
        <td>${Course.name}</td>
        <td>${Course.teacherName}</td> 
        <td>${Course.grade}</td>       
     </tr>  
    </tbody> 
    </table>
       
    </div>
    
    <div>
      
    </div>
    
    <div class="clear"></div>
    <script>
	
	
</script> 
</body>
</html>