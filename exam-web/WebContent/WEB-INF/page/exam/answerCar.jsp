<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/icon.css"> 
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.easyui.min.js"></script>


<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
 
  


<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>

<style>

img { 
    height: auto;
    max-width: 100%; 
    border: 0;
    vertical-align: top;
    -ms-interpolation-mode: bicubic;
}
</style>

</head>
   <body>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">成绩管理</a></li>
    <li><a href="#">成绩信息</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
   
    
    
    <table class="imgtable">
    
    <thead>
    <tr>
    <th>学生账号</th>
    <th>学生姓名</th>  
    <th>试卷名称</th>
    <th>任课教师</th>
    <th>成绩</th>
    <th width="200px;">操作按钮</th>
    </tr>
    </thead>
    
    <tbody id="tabletb">
     
    </tbody> 
    </table>
       
    </div>
    
    <div>
      
    </div>
    
   
<script>
	$(function(){
		tableDataLoad();
	});
	
	 
	var pageOffset = 1;

	function tableDataLoad(pageItem){  
		
		if (pageItem){
			var data = { 
					pageOffset:pageItem
			};
			pageOffset = pageItem;
		}
		else{
			var data = { 
			};
		}
		
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath %>exam/tableloadCar.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	$('#tabletb').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    }); 
		
	}
	//删除
	function deleteCar(obj){
		var id = $( obj ).attr("attachId"); 
		var data = {
			id:id
		};
		var loadtip = xtip.load('删除中...');
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath%>exam/deleteCar.do?",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"json",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	xtip.alert('删除成功','s');  
	        	tableDataLoad();
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    });  
	}
	
	// 编辑
	function editCar(obj){
		var id = $( obj ).attr("attachId"); 
		window.location.href="<%=basePath %>exam/correct.do?id="+id; 
	}
	
	 
	
</script> 
 
 </body>
 </html>
    