<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html>
<html>
<head>
<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>jQuery在线考试倒计时界面</title>
    <link href="../static/css/exam/main.css" rel="stylesheet" type="text/css" />
 
    <link href="../static/css/exam/test.css" rel="stylesheet" type="text/css" />

    <style>
        .hasBeenAnswer {
            background: #5d9cec;
            color: #fff;
        }

        .reading h2 {
            width: 100%;
            margin: 20px 0 70px;
            text-align: center;
            line-height: 2;
            font-size: 20px;
            color: #59595b;
        }

            .reading h2 a {
                text-decoration: none;
                color: #59595b;
                font-size: 20px;
            }

                .reading h2 a:hover {
                    color: #2183f1;
                }
    </style>
</head>

<body>
    <div class="main">
        <!--nr start-->
        <div class="test_main">
            <div class="nr_left">
                <div class="test">
                    <form action="" method="post">
                        <div class="test_title">        
                            <font><input type="button" name="test_jiaojuan" onclick="uput()" value="提交"></font>
                        </div>
                        
                        
                                           
                        <div class="test_content">
                            <div class="test_content_title">
                                <h2>问答题</h2>
                                
                            </div>
                        </div>
                       <input id="wt_count" name="wt_count" type="hidden" class="dfinput" value="${Count}" /> 
                        <c:forEach var="item" items="${ListData}">
                        <input id="idw_${item.number}" name="id" type="hidden" class="dfinput" value="${item.id}" /> 
                        <div class="test_content_nr">
                            <ul>

                                <li id="wt_${item.number}">
                                    <div class="test_content_nr_tt">
                                        <i>${item.number}</i><span>(${item.value}分)</span><font>${item.topicPaper.topic}</font><b class="icon iconfont">&#xe881;</b>
                                    </div>

                                    <div class="test_content_nr_main">
                                        <ul>

                                            <li class="option_${item.number}">
                                              <label for="2_answer_${item.number}_option_2">
                                                <textarea  style="width: 100%;height: 260px;" id="answer3${item.number}" >${item.content}</textarea>
                                                </label>
                                            </li> 
                                            </ul>
                                            <ul>  
                                            <li class="option_${item.number}">
                                              <label for="2_answer_${item.number}_option_2">
                                                <textarea  style="width: 100%;height: 260px;" id="answer3${item.number}" >${item.topicPaper.answer}</textarea>
                                                </label>
                                                <label for="2_answer_${item.number}_option_2" style="font-size: 11pt;"> 得分：
                                                <input id="score_${item.number}" name="score" type="number" class="dfinput" value="" style="margin: 20px 5px;height: 15px;width: 60px;font-size: 11pt;" /> 
                                                </label>
                                            </li>                                                                              
                                        </ul>
                                        
                                    </div>
                                </li>
                              </ul>  
                        </div>
                        </c:forEach>

                    </form>
                </div>

            </div>
            
        </div>
        <!--nr end-->


        <div class="foot"></div>

       
    </div>

    <script src="../static/js/jquery-1.11.3.min.js"></script>
    <script src="../static/js/jquery.easy-pie-chart.js"></script>

    <script>
    var value=0;
    var number=0;
    var score=0;
    function uput(){ 
    var count = $("#wt_count").val();     	
    for(number=1;number<=count;number++)
    {  	
    	score = Number($("#score_"+number+"").val()); 
    	value = value+score;
    }
    	var id = $("#idw_"+number+"").val(); 
    	var data = { 
    			value:value,
				id:id
		};			
	var loadtip = xtip.load('加载中...')
    $.ajax({
        //发送请求URL，可使用相对路径也可使用绝对路径
        url: "<%=basePath %>exam/scores.do",
        //发送方式为GET，也可为POST，需要与后台对应
        type:"POST",
        //设置接收格式 
        dataType:"text",
        cache: false,
        timeout:30000,
        traditional:true,
        async: true,
      	//处理数组
        traditional:true, 
        //向后台发送的数据
        data:data,
        //后台返回成功后处理数据，data为后台返回的json格式数据
        success:function(data){
        	xtip.close(loadtip); 
        	
        } ,
        error: function( data ) {
        	xtip.close(loadtip); 
        	 xtip.alert(data.message,'e');  
        }
      })
    	window.location.href="<%=basePath %>exam/answerCar.do";     
    }   
    
    </script>


</body>

</html>
