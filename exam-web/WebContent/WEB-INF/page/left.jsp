﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>菜单</title>
<link href="pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="pagestatic/js/jquery.js"></script>

<script type="text/javascript">
$(function(){	
	//导航切换
	$(".menuson li").click(function(){
		$(".menuson li.active").removeClass("active")
		$(this).addClass("active");
	});
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('ul').slideUp();
		if($ul.is(':visible')){
			$(this).next('ul').slideUp();
		}else{
			$(this).next('ul').slideDown();
		}
	});
})	
</script>


</head>

<body style="background:#f0f9fd;">
	<div class="lefttop"><span></span>菜单目录</div>
    
    <dl class="leftmenu">
    
    <c:forEach var="item" items="${menuData}" varStatus="status">
          <c:if test="${item.parentId == rootMenu}">
         <dd>
             <div class="title">
               <span><img src="pagestatic/images/leftico01.png" /></span>${item.menuName }
             </div>
             <ul class="menuson">
                  <c:forEach var="childitem" items="${menuData}" varStatus="childstatus"> 
                     <c:if test="${childitem.parentId == item.id}">
              <li><cite></cite><a href="<%=basePath%>${childitem.menuUrl}" target="rightFrame">${childitem.menuName }</a><i></i></li> 
                     </c:if>
                  </c:forEach>
             </ul> 
         </dd>
         </c:if>
    </c:forEach>
     
    </dl>
</body>
</html>
