<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 


   <div class="formbody">
    
	    <div class="formtitle"><span><a>个人信息</a></span></div>
	    <form id="editUserForm"> 	  
	    <ul class="forminfo">	     
        <li><input name="id" id="userId" type="hidden" class="dfinput" value="${TbUser.id}" disabled/></li> 
	    <li><label>账号</label><input id="account" type="text" class="dfinput" value="${TbUser.account}"disabled/></li>
	    <li><label>姓名</label><input id="userName" type="text" class="dfinput" value="${TbUser.userName}"disabled/></li>
	    <li><label>性别</label><input id="sex" type="text" class="dfinput" value="${TbUser.sex}"disabled/></li> 
	   <%--  <li><label>出生年月</label><input id="birth" type="text" class="dfinput"value="${TbUser.birth}"disabled/></li> --%>
	    <li><label>手机号码</label><input id="telephone" type="text" class="dfinput" value="${TbUser.telephone}"disabled/></li>
	    <li><label>家庭地址</label><input id="address" type="text" class="dfinput" value="${TbUser.address}"disabled/></li>
	    <li><label>邮箱</label><input id="mailbox" type="text" class="dfinput" value="${TbUser.mailbox}"disabled/></li>
	    <li><label>QQ号</label><input id="qq" type="text" class="dfinput" value="${TbUser.qq}"disabled/></li>
	    </ul>
     </form>
     <ul class="forminfo">
       <li><label>&nbsp;</label><input  type="button" class="btn" value="修改" onclick="updata()" /></li>
      </ul>    
    </div>
    <script> 
    function updata(){ 
    	var id=$("#userId").val();
		xtip.open({
			type: 'url',
			content : '<%=basePath%>person/edit.do?id=' + id,
			title: '【修改个人信息】',
			width: '800px',
			height: '700px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	</script> 