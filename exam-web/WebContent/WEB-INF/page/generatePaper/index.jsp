<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
<link href="<%=basePath %>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath %>static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<style>

img { 
    height: auto;
    max-width: 100%; 
    border: 0;
    vertical-align: top;
    -ms-interpolation-mode: bicubic;
}
</style>

</head>
<body> 	  
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="<%=basePath %>exam/index.do">试卷管理</a></li>
    <li><a href="#">试卷生成</a></li>
    </ul>
    </div>
    <input id="basePath" type="hidden" value = "<%=basePath %>"/> 
    <input id="id" type="hidden" value = ""/>   
	 
		 <div id="tableHome" class="data-area">
						     
         </div>
         <div id="tableEdit" class="data-area">
						     
         </div>
         <div id="tableCreate" class="data-area">
						     
         </div>
		
<script>
function blank(){
	window.location.href="<%=basePath %>exam/index.do";
		
}
	$(function(){
	
		tableDataLoad("${id}");
		
	});
	
   
	function tableDataLoad(id){ 
		if(id){
		var data = {
				id:id
			};
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url:"<%=basePath %>generate/tableloadData.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	remove();
	        	$('#tableEdit').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	            xtip.alert(data.message,'e');  
	        }
	    }); 
	}else{
		var data = {
			};
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url:"<%=basePath %>generate/tableload.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	removeTo();
	        	$('#tableHome').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	            xtip.alert(data.message,'e');  
	        }
	    }); 
	}
	
		
	}
	 
	function remove(){ 	
		 $("#tableHome").empty();		 
	}
	function removeTo(){ 	
		 $("#tableEdit").empty();		 
	}
	var xipid;
	function topicEdits(obj){
		var id = $( obj ).attr("attachId");
	 xipid = xtip.open({
			type: 'url',
			content : "<%=basePath %>generate/edit.do?id=" + id,
			title: '【查看试题】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,			
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	function closep(){
		xtip.close(xipid);
	}
	function topicDelete(obj){
		var id = $( obj ).attr("attachId");
		var data = {
				id:id
			};
			var loadtip = xtip.load('删除中...');
		    $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>generate/deletes.do?",
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip);		        	
		        	xtip.alert('删除成功','s'); 
		        	tableDataLoad(data.data);
		        	
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }		       
		    }); 
	}
	
	 function creator(){
		 
	     var name=$("#examPaperName").val();
	  	 var course=$("#examCourse").val();
	  	 var total=$("#examPaperTotalScore").val();
	  	 var times=$("#examPaperTime").val();
	  	 
	  	var choicType=$('#chioceType option:selected').text();
	  	var answerRandom=$("#answerRandom").val();
	  	var answerLow=$("#answerLow").val();
	  	var answerMedium=$("#answerMedium").val();
	  	var answerIssue=$("#answerIssue").val();
	  	var choiceNumber=$("#answerNumber").val();
	  	 var answerScore=$("#answerScore").val();
	  	 
	  	var judgmentType=$('#judgmentType option:selected').text();
	  	 var judgmentRandom=$("#judgmentRandom").val();
	  	var judgmentLow=$("#judgmentLow").val();
	  	var judgmentMedium=$("#judgmentMedium").val();
	  	var judgmentIssue=$("#judgmentIssue").val();
	  	var judgmentNumber=$("#judgmentNumber").val();
	  	 var judgmentScore=$("#judgmentScore").val();
	  	 
	  	var answerType=$('#answerType option:selected').text();
	  	var choiceRandom=$("#choiceRandom").val();
	  	var choiceLow=$("#choiceLow").val();
	  	var choiceMedium=$("#choiceMedium").val();
	  	var choiceIssue=$("#choiceIssue").val();
	  	var choiceNumber=$("#choiceNumber").val();
	  	 var choiceScore=$("#choiceScore").val();
	  	 
	  	 var type="系统组卷";
		 var data = { 
					name:name,
					courseId:course,
					total:total,
					times:times,
					generationType : type,
					
					answerType:answerType,
					answerRandom:answerRandom,
					answerLow:answerLow,
					answerMedium:answerMedium,
					answerIssue:answerIssue,
					answerNumber:answerNumber,
					answerScore:answerScore,
					
					choicType:choicType,
					choiceRandom:choiceRandom,
					choiceLow:choiceLow,
					choiceMedium:choiceMedium,
					choiceIssue:choiceIssue,
					choiceNumber:choiceNumber,
					choiceScore:choiceScore,
					
					judgementType:judgmentType,
					judgementRandom:judgmentRandom,
					judgementLow:judgmentLow,
					judgementMedium:judgmentMedium,
					judgementIssue:judgmentIssue,
					judgementNumber:judgmentNumber,
					judgementScore:judgmentScore
					
			};
		 var loadtip = xtip.load('加载中...')

		 $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath %>generate/creator.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	remove(); 
	        	$('#tableEdit').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    }); 
		 
 }
	

	</script> 	
		
</body>
</html>