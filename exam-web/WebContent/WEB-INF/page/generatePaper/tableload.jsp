<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<style>
.ibox p{
    font-weight: bold;
    font-size: 13pt;
    margin: 0 325px 6px 0;
}

.mainleft{
padding-right : 0px;
}

</style>
   <div class="formbody">
    <div class="formtitle" style="margin-bottom: 20px;" >
    
    <span>设置试卷参数</span>
    </div>  
    <div class="mainbox"style=" height:100%;">   
    <div class="mainleft"style=" height:100%;">        
    <div class="leftinfo" style=" height:100%;" >
    <div class="listtitle"><span style="text-align:center;font-size: 14pt;margin: 0 0px 0px -360px;">试卷参数设置</span></div>
    
    <div class="maintj">
    <table class="imgtable"style="border: 0;"> 
     <thead >
     <tr>
    <th style="text-align:left;font-size: 13pt;">试卷名称</th>
    <th style="text-align:left;font-size: 13pt"><input id="examPaperName" type="text" class="dfinput" value="${TbExamPaper.name}"  style="width: 290px;margin: 0 170px 4px 0;" /><i></i></th>
    <th style="text-align:left;font-size: 13pt;">总分值</th>
    <th style="text-align:left;font-size: 13pt;margin: 0 0px 10px 0;"><input id="examPaperTotalScore" type="text" class="dfinput" value="${TbExamPaper.total}"  style="width:80px;margin: 0 562px 4px -5px;" /><i></i></th>    
    </tr>
    <tr >
    <th style="text-align:left;font-size: 13pt;">试卷科目</th>
    <th style="text-align:left;font-size: 13pt;"><input id="examCourse" type="text" class="dfinput" value="${TbExamPaper.courseId}"  style="width: 290px;margin: 0 170px 4px 0; " /><i></i></th>
    <th style="text-align:left;font-size: 13pt;">考试时间</th>
    <th style="text-align:left;font-size: 13pt;"><input id="examPaperTime" type="text" class="dfinput" value="${TbExamPaper.time}"  style="width:80px;margin: 0 562px 4px -5px; " /><i></i></th>
    </tr>
        
    
    </thead>
    </table>
    <div class="ibox"style = " margin: 0px 0px -5px -33px;padding-left: 13px;padding-top: 5px;overflow: hidden;padding-bottom: 5px;">    
     
        <p>设置自动组卷的试题参数</p>
    
     </div>  
       <table class="imgtable"style="border: 0;" id="topicParameters" >
    
   <thead style="border: #d3dbde solid 1px;">     
        
    <tr >       
    <th style="text-align:left;font-size: 13pt;">题型</th>  
    <th style="text-align:center;font-size: 13pt;">随机难度</th>
    <th style="text-align:center;font-size: 13pt;">容易</th>
    <th style="text-align:center;font-size: 13pt;">中等</th>
    <th style="text-align:center;font-size: 13pt;">困难</th>
    <th style="text-align:center;font-size: 13pt;">题数</th>
    <th style="text-align:center;font-size: 13pt;">分值</th>
   <!--  <th style="text-align:center;font-size: 13pt;">操作按钮
    <input name='LearnTRLastIndex' type='hidden' id='LearnTRLastIndex' value="1" /> </th>    --> 
    </tr>    
    </thead>
    <tbody id="tabletb">
     <tr>       
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">
        <select class="form-control" id="chioceType" style = " margin: 0px 0px 0px -21px;width: 129px;">
		  <option value="chioce"selected>选择题(单)</option>
		  <option value="judgment">判断题</option>
		  <option value="answer">问答题</option>
		</select>
        </td>               
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceRandom" type="number" class="dfinput" value="0" max="100" min="0" style="width:80px;margin: 5px 50px  "/>
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceLow" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px  " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceMedium" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px  " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceIssue" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px  " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceNumber" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px  " disabled/><i></i></td> 
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceScore" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px; margin: 5px 50px" /><i></i></td>
        <!-- <td style="width:200px; border-right: 0;font-size: 12pt;"><a class="optLink" title="删除" href="javascript:void(0);" attachId="" onclick="topicDelete()"><i style="color:blue;font-size: 11pt;">删除</i></a></td>    -->              
     </tr> 
     <tr>       
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">
		<select class="form-control" id="judgmentType" style = " margin: 0px 0px 0px -21px;width: 129px;">
		  <option value="chioce">选择题(单)</option>
		  <option value="judgment"selected>判断题</option>
		  <option value="answer">问答题</option>
		</select>
		</td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentRandom" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentLow" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin:5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentMedium" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentIssue" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin:5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentNumber" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px" disabled/><i></i></td>       
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentScore" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px; margin: 5px 50px" /><i></i></td>
        <!--  <td style="width:200px; border-right: 0;font-size: 12pt;"><a class="optLink" title="删除" href="javascript:void(0);" attachId="" onclick="topicDelete()"><i style="color:blue;font-size: 11pt;">删除</i></a></td>   -->              
     </tr>    
     <tr>       
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">
		<select class="form-control" id="answerType" style = " margin: 0px 0px 0px -21px;width: 129px;">
		  <option value="chioce">选择题(单)</option>
		  <option value="judgment">判断题</option>
		  <option value="answer"selected>问答题</option>
		</select>
		</td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerRandom" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerLow" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin:5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerMedium" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin:5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerIssue" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin: 5px 50px " />
        <i style=" color: #356155c4;margin: 0 0 0 -45px">道</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerNumber" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px;margin:5px 50px " disabled/><i></i></td>        
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerScore" type="number" class="dfinput" value="0" max="100" min="0"  style="width:80px; margin:5px 50px" /><i></i></td>
       <!--  <td style="width:200px; border-right: 0;font-size: 12pt;"><a class="optLink" title="删除" href="javascript:void(0);" attachId="" onclick="topicDelete()"><i style="color:blue;font-size: 11pt;">删除</i></a></td>   -->              
     </tr>  
    </tbody> 
    </table>
    </div>  
    </div>
    </div>               
    </div> 
   
     <ul class="forminfo" style="margin: 131px 0 0 356px;">   
       <li>
       <label style="width: 200px;">&nbsp;</label>
       <input name="" type="button" class="btn" value="下一步" onclick="creator()" style="width:80px;margin: 0 15px ;"  />
       <input name="" type="button" class="btn" value="取消" onclick="blank()" style="width:80px;margin: 0 15px ;"  />
       </li>
     </ul> 
     </div>
 <script type="text/javascript"> 
 $("input").blur(function(){
	 var x ="123";
	 var choiceNumber = Number($("#choiceRandom").val())+Number($("#choiceLow").val())+Number($("#choiceMedium").val())+ Number($("#choiceIssue").val())+""; 
	 var judgmentNumber = Number($("#judgmentRandom").val())+Number($("#judgmentLow").val())+Number($("#judgmentMedium").val())+ Number($("#judgmentIssue").val())+""; 
	 var answerNumber = Number($("#answerRandom").val())+Number($("#answerLow").val())+Number($("#answerMedium").val())+ Number($("#answerIssue").val())+""; 
	 $("#answerNumber").val(answerNumber);
	 $("#judgmentNumber").val(judgmentNumber);
	 $("#choiceNumber").val(choiceNumber);	
})
 

 
 </script>

  