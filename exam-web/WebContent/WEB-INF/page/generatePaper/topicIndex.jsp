<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/icon.css"> 
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.easyui.min.js"></script>

<link href="<%=basePath %>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath %>static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<style>
p {
	display: inline-block;
    float: left;
    font-size: 16px;
    margin: 10px 26px;
}
p i {
	margin: 0 10px;
    font-style: initial;
    font-size: medium;
}
img { 
    height: auto;
    max-width: 100%; 
    border: 0;
    vertical-align: top;
    -ms-interpolation-mode: bicubic;
}
</style>

</head>


<body>
<input id="id" name="id" type="hidden" class="dfinput" value="${id}" /> 
 		<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">组卷试题</a></li>
    <li><a href="#">添加组卷试题</a></li>    
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar">
       <li class="click" onclick="refresh()"><span><img src="<%=basePath %>pagestatic/images/t04.png" /></span>刷新</li>
        </ul>
        
        
       <ul class="toolbar1" style="background: 0;border: 0">
        <li style="padding-right: 0;margin:0"><input id="topicQuery" type="text" class="query" placeholder="请输入查询内容" style="height: 35px;padding-right: 0;margin:0"/></li>
        <li style="padding-right: 0;margin:0"><input name="" type="button" class="scbtn" value="查询" style="width:70px; padding-right: 0;" onclick="topicQuery()" /></li>
       </ul>
	       <div class="btn-group"style="display:inline-block; float: right;margin: 0 5px;border: 0;border: solid 1px #cbcbcb;">
		<button type="button" class="btn btn-default"style="height: 32px;width: 64px;border: 0;">题型</button>
		<button type="button" class="btn btn-default dropdown-toggle" 
				data-toggle="dropdown"style="height: 32px;width: 26px;">
			<span class="caret"></span>
			<span class="sr-only"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" onclick="a('1')">选择题</a></li>
			<li><a href="#" onclick="a('2')">判断题</a></li>
			<li><a href="#" onclick="a('3')">问答题</a></li>
			<li><a href="#" onclick="a('4')">全部</a></li>
		</ul>
	   </div>
	   
	   <div class="btn-group"style="display:inline-block; float: right;margin: 0 5px;border: 0;border: solid 1px #cbcbcb;">
		<button type="button" class="btn btn-default"style="height: 32px;width: 64px;border: 0;">难度</button>
		<button type="button" class="btn btn-default dropdown-toggle" 
				data-toggle="dropdown"style="height: 32px;width: 26px;">
			<span class="caret"></span>
			<span class="sr-only"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" onclick="b('1')">低</a></li>
			<li><a href="#" onclick="b('2')">一般</a></li>
			<li><a href="#" onclick="b('3')">高</a></li>
			<li><a href="#" onclick="b('4')">全部</a></li>
		</ul>
	   </div>
	   	   
    </div>
    
    
    <table class="imgtable">
    
    <thead>
    <tr>
    <th>试题科类</th>
    <th>试题内容</th>
    <th>试题答案</th>
    <th>题型</th>
    <th>难度</th>   
    <th width="300px;">操作按钮</th> 
    </tr>
    </thead>
    
    <tbody id="tabletb">    
    </tbody>
   
     
     
    </table>
       
    </div>
   <p> 选择题<i>${Count.choiceCount}</i></p>
   <p>判断题<i>${Count.judgementCount}</i></p>
   <p> 问答题<i>${Count.anserCount}</i></p>
    <div>
      <input name="" type="button" class="btn" value="返回" onclick="back()" style="margin: 64px 756px;width: 100px;"/>
    </div>
    
    
    
<script>
	$(function(){
		tableDataLoad();
	});
	var grade = 0;
	var type = 0;
	var pageOffset = 1;
	function tableDataLoad(pageItem){ 
		var id = $("#id").val()
		if (pageItem){
			var data = { 
					id:id,
					type:type,
					grade:grade,
					pageOffset:pageItem
			};
			pageOffset = pageItem;
		}
		else{
			var data = { 
					pageOffset:pageOffset,
					id:id,
					type:type,
					grade:grade
			};
		}
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url:"<%=basePath %>generate/topicPaper.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);	      
	        	$('#tabletb').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	            xtip.alert(data.message,'e');  
	        }
	    }); 
	}
	
	function a(x){			
		if(x){
			 type=x;
		} 
		tableDataLoad(pageOffset);
	}
	function b(x){
				
		if(x){
			 grade=x;
		} 
		tableDataLoad(pageOffset);
	}
		 // 添加
		function refresh(){
			pageOffset = 1;
			tableDataLoad(pageOffset);
		 }
		// 更新
		var xipid;
		function topicEdits(obj){ 
			var topicId = $( obj ).attr("attachId");
			 xipid = xtip.open({
			type: 'url',
			content : "<%=basePath %>generate/edit.do?topicId=" + topicId,
			title: '【查看试题】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
		}
		function closep(){
			xtip.close(xipid);
		}
		function topicAdd(obj){
			var topicId = $( obj ).attr("attachId");
			var id = $("#id").val();
			var data = {
				topicId:topicId,
				id:id
				};
				var loadtip = xtip.load('添加中...');
			    $.ajax({
			        //发送请求URL，可使用相对路径也可使用绝对路径
			        url: "<%=basePath%>generate/addTopic.do?",
			        //发送方式为GET，也可为POST，需要与后台对应
			        type:"POST",
			        //设置接收格式 
			        dataType:"json",
			        cache: false,
			        timeout:30000,
			        traditional:true,
			        async: true,
			      	//处理数组
			        traditional:true, 
			        //向后台发送的数据
			        data:data,
			        //后台返回成功后处理数据，data为后台返回的json格式数据
			        success:function(data){
			        	xtip.close(loadtip);		        				        	
			        	window.location.href="<%=basePath %>generate/topicIndex.do?id="+id;			        	
			        } ,
			        error: function( data ) {
			        	xtip.close(loadtip); 
			        	 xtip.alert(data.message,'e');  
			        }		       
			    });
		}
		function back(){
			var id = $("#id").val()
			window.location.href="<%=basePath %>generate/index.do?id="+id;
		}
	<%--
		// 删除
		function topicDelete(obj){
			var id = $( obj ).attr("attachId"); 
			var data = {
				id:id
			};
			var loadtip = xtip.load('删除中...');
		    $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>topic/delete.do?id=" + id,
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip);
		        	xtip.alert('删除成功','s');  
		        	tableDataLoad();
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }
		    }); 
		}
		// 查询
		function topicQuery(){
			var category=$("#topicQuery").val();			
			var data = {
					category:category
			};
			var loadtip = xtip.load('查询中...')
		   $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>topic/query.do",
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip); 
		        /* $('#tablequer').html( data );  */
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }
		    });  
		}
	
	
	 --%>
		
</script> 
 
 
    
    
    
 	 		
</body>
</html>
