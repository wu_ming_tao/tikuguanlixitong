<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 
<style>
.tools{
	margin:-36px 0 0 0;
}
.toolbar1 li{
  text-indent: 4px;
}

</style>
    <div class="formbody"style = "height:100%;padding: 31px 18px;" >
    <div class="formtitle"style = "height:100%">
    
    <span>编辑试卷试题</span>
    </div>  
    <div class="mainbox"style = "height:100%">
    <div class="leftinfo" style="height: 105px">
    <div class="listtitle"><span style="text-align:center;font-size: 14pt;margin: 0 0px 0px -360px;">试卷参数设置</span></div>
    <div class="maintj">
     
    <table class="imgtable"style="border: 0;"> 
     <thead>
     <tr>
    <th style="text-align:left;font-size: 13pt;">试卷名称<input id="examPaperId" type="hidden" value ="${TbExamPaper.id}"/></th>
    <th style="text-align:left;font-size: 13pt"><input id="examPaperName" type="text" class="dfinput" value="${TbExamPaper.name} "  style="width: 290px;margin: 0 170px 4px 0;" /><i></i></th>
    <th style="text-align:left;font-size: 13pt;">总分值</th>
    <th style="text-align:left;font-size: 13pt;margin: 0 0px 10px 0;"><input id="examPaperTotalScore" type="text" class="dfinput" value="${TbExamPaper.total}"  style="width:80px;margin: 0 562px 4px -5px;" /><i></i></th>    
    </tr>
    <tr >
    <th style="text-align:left;font-size: 13pt;">试卷科目</th>
    <th style="text-align:left;font-size: 13pt;"><input id="examCourse" type="text" class="dfinput" value="${TbExamPaper.courseId}"  style="width: 290px;margin: 0 170px 4px 0; " /><i></i></th>
    <th style="text-align:left;font-size: 13pt;">考试时间</th>
    <th style="text-align:left;font-size: 13pt;"><input id="examPaperTime" type="text" class="dfinput" value="${TbExamPaper.time}"  style="width:80px;margin: 0 562px 4px -5px; " /><i></i></th>
    </tr>        
    </thead>
    </table>
     </div> 
     </div> 
     </div>
     
     <div id="ibox" class="ibox"style = "border: #d3dbde solid 1px; margin: -6px 8px 0 8px;padding-left: 13px;padding-top: 5px;overflow: hidden;padding-bottom: 5px;">
     <button type="button" class="btn btn-primary" id="add"onclick="addTopics()">手动添加试题</button>
     </div> 
            
     <c:forEach var="item" items="${ListData}">      
     <c:choose>
     <c:when test="${item.type=='选择题(单)'}">    
     <!-- 选择题大题模板 -->
    
    <div class="mainbox">
    <div class="leftinfo" style="height: 100%" > 
    <div class="maintj">
    <div class="listtitle" style=" font-size: 13pt;text-align:left">第${item.typeSort}大题、${item.type}
    <div class="tools"  >    
    	<ul class="toolbar1" style="margin: 0 0 0px 25px;">
        <li class="click" onclick="addTopics()"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
        <%-- <li class="click" onclick=""><span><img src="<%=basePath %>pagestatic/images/t02.png" /></span>修改</li> --%>
        <li class="click" attachId="${item.type}" onclick="deleteTopics(this)"><span><img src="<%=basePath %>pagestatic/images/t03.png" /></span>删除</li>                
        <li class="click" onclick="setOrder()"><span><img src="<%=basePath %>pagestatic/images/t05.png" /></span>设置</li>
        </ul>
    </div>
    </div>
    </div>     
     
       <table class="imgtable"style="border: 0;">
    
   <thead>     
        
    <tr > 
    <th style="text-align:left;font-size: 13pt;width:100px;">序号</th>       
    <th style="text-align:left;font-size: 13pt;width:200px;">试题类型</th>
    <th style="text-align:left;font-size: 13pt;width:900px;">试题内容</th>
    <th style="text-align:left;font-size: 13pt;width:200px;">标准答案</th>
    <th style="text-align:left;font-size: 13pt;width:150px;">难易程度</th>
    <th style="text-align:center;font-size: 13pt;width:150px;"><i style="font-style: inherit;font-size: 12pt;">分数</i>
        <a class="optLint" href="javascript:void(0);"onclick="updatas()">批量更新</a>
    </th>
    <th style="text-align:center;font-size: 13pt;width:247px;">操作</th>    
    </tr>    
    </thead>   
    <c:forEach var="item" items="${item.tbTopicPaper}">              
    <tbody id="tabletb">
     <tr>
        <td style="width:100px; border-right: 0;font-size: 12pt;text-indent: 31px;">${item.serial}</td>       
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">${item.type}</td>               
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;">${item.topic}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.answer}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.topics.grade}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="topicValue" type="number" class="dfinput" attachId="${item.id}" value="${item.value}" max="100" min="0"  style="width:80px; margin: 5px 42px;  " /></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;text-align:center;">
         <a class="optLink" title="编辑" href="javascript:void(0);" attachId="${item.id}" onclick="topicEdits(this)"><i  style="color:blue">查看</i></a>
         
         <a class="optLink" title="删除" href="javascript:void(0);" attachId="${item.id}" onclick="topicDelete(this)"><i style="color:blue">删除</i></a> 
        </td> 
                 
     </tr>       
    </tbody>
    </c:forEach>    
    </table>
    </div>
    </div>    
    </c:when>
    </c:choose>
    </c:forEach> 
    
    <c:forEach var="item" items="${ListData}">
     <c:choose>
    <c:when test="${item.type=='判断题'}">  
     <!-- 判断大题模板 -->
    <div class="mainbox">
    <div class="leftinfo" style="height: 100%" > 
    <div class="maintj">
    <div class="listtitle" style=" font-size: 13pt;text-align:left">第${item.typeSort}大题、${item.type}
    <div class="tools"  >    
    	<ul class="toolbar1" style="margin: 0 0 0px 25px;">
        <li class="click" onclick="addTopics()"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
        <%-- <li class="click" onclick=""><span><img src="<%=basePath %>pagestatic/images/t02.png" /></span>修改</li> --%>
        <li class="click" attachId="${item.type}" onclick="deleteTopics(this)"><span><img src="<%=basePath %>pagestatic/images/t03.png" /></span>删除</li>                
        <li class="click" onclick="setOrder()"><span><img src="<%=basePath %>pagestatic/images/t05.png" /></span>设置</li>
        </ul>
    </div>
    </div>
    </div>     
     
        <table class="imgtable"style="border: 0;">
    
   <thead>     
        
    <tr > 
    <th style="text-align:left;font-size: 13pt;width:100px;">序号</th>       
    <th style="text-align:left;font-size: 13pt;width:200px;">试题类型</th>
    <th style="text-align:left;font-size: 13pt;width:900px;">试题内容</th>
    <th style="text-align:left;font-size: 13pt;width:309px;">标准答案</th>
    <th style="text-align:left;font-size: 13pt;width:150px;">难易程度</th>
    <th style="text-align:center;font-size: 13pt;width:150px;">分数
    <a class="optLint" href="javascript:void(0);"onclick="updatas()">批量更新</a></th>
    <th style="text-align:center;font-size: 13pt;width:247px;">操作</th>    
    </tr>    
    </thead>  
   <c:forEach var="item" items="${item.tbTopicPaper}">
    <tbody id="tabletb">
     <tr>   
       <td style="width:100px; border-right: 0;font-size: 12pt;text-indent: 31px;">${item.serial}</td>      
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">${item.type}</td>               
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;">${item.topic}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.answer}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.topics.grade}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="topicValue" type="number" class="dfinput" value="${item.value}"  attachId="${item.id}"  max="100" min="0"   style="width:80px; margin: 5px 42px;  " /></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;text-align:center;">
         <a class="optLink" title="编辑" href="javascript:void(0);" attachId="${item.id}" onclick="topicEdits(this)"><i  style="color:blue">查看</i></a>
         
         <a class="optLink" title="删除" href="javascript:void(0);" attachId="${item.id}" onclick="topicDelete(this)"><i style="color:blue">删除</i></a> 
        </td> 
                 
     </tr>       
    </tbody> 
    </c:forEach>
    </table>
    </div>
    </div> 
    </c:when>
    </c:choose>
    </c:forEach>
    <c:forEach var="item" items="${ListData}">
    <c:choose>
    <c:when test="${item.type=='问答题'}">  
     <!-- 问答大题模板 -->
    <div class="mainbox">
    <div class="leftinfo" style="height: 100%" > 
    <div class="maintj">
    <div class="listtitle" style=" font-size: 13pt;text-align:left">第${item.typeSort}大题、${item.type}
    <div class="tools"  >    
    	<ul class="toolbar1" style="margin: 0 0 0px 25px;">
        <li class="click"onclick="addTopics(this)"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
       <%--  <li class="click" ><span><img src="<%=basePath %>pagestatic/images/t02.png" /></span>修改</li> --%>
        <li class="click" attachId="${item.type}" onclick="deleteTopics(this)"><span><img src="<%=basePath %>pagestatic/images/t03.png" /></span>删除</li>                
        <li class="click" onclick="setOrde()"><span><img src="<%=basePath %>pagestatic/images/t05.png" /></span>设置</li>
        </ul>
    </div>
    </div>
    </div>     
     
        <table class="imgtable"style="border: 0;">
    
   <thead>     
        
    <tr > 
    <th style="text-align:left;font-size: 13pt;width:100px;">序号</th>      
    <th style="text-align:left;font-size: 13pt;width:200px;">试题类型</th>
    <th style="text-align:left;font-size: 13pt;width:900px;">试题内容</th>
    <th style="text-align:left;font-size: 13pt;width:309px;">标准答案</th>
    <th style="text-align:left;font-size: 13pt;width:150px;">难易程度</th>
    <th style="text-align:center;font-size: 13pt;width:150px;">分数<a class="optLint" href="javascript:void(0);"onclick="updatas()">批量更新</a></th>
    <th style="text-align:center;font-size: 13pt;width:247px;">操作</th>    
    </tr>    
    </thead>
    <c:forEach var="item" items="${item.tbTopicPaper}">
    <tbody id="tabletb">
     <tr> 
        <td style="width:100px; border-right: 0;font-size: 12pt;text-indent: 31px;">${item.serial}</td>          
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">${item.type}</td>               
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;">${item.topic}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.answer}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.topics.grade}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="topicValue" type="number" class="dfinput" value="${item.value}"attachId="${item.id}" max="100" min="0"   style="width:80px; margin: 5px 42px;  " /></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;text-align:center;">
         <a class="optLink" title="编辑" href="javascript:void(0);" attachId="${item.id}" onclick="topicEdits(this)"><i  style="color:blue">查看</i></a>
         
         <a class="optLink" title="删除" href="javascript:void(0);" attachId="${item.id}" onclick="topicDelete(this)"><i style="color:blue">删除</i></a> 
        </td> 
                 
     </tr>       
    </tbody> 
    </c:forEach>
    </table>
    </div>
    </div>
    </c:when>
    </c:choose>
    </c:forEach>	
    </div>
    
    <ul class="forminfo"style="margin: 50px 0 10px 400px;">
       <li>
       <label style="width: 200px;">&nbsp;</label>
       <input name="" type="button" class="btn" value="生成" onclick="creat()"style="margin: 0px 10px ;width:68px;" />
       <input name="" type="button" class="btn" value="返回" onclick="backs()" style="margin: 0px 10px ;width:68px;"/>
       </li>
     </ul>
    
<script type="text/javascript">
 var updata = 0;
 function updatas(){
	 if(updata==0){
		 updata=1;
	 }else{
		 updata=0;
	 }
 }
 
    var xtipid;
   function setOrder(){
	     var id=$("#examPaperId").val();
		 xtipid = xtip.open({
				type: 'url',
				content : "<%=basePath %>generate/order.do?id=" + id,
				title: '【查看试题】',
				width: '400px',
				height: '300px',
				min: true,
				shade: false,			
				max: true,
				lock: true,
				closeBtn: true,
				}); 
   }
   function creat(){
	     var id=$("#examPaperId").val();
	     var name=$("#examPaperName").val();
	  	 var course=$("#examCourse").val();
	  	 var total=$("#examPaperTotalScore").val();
	  	 var times=$("#examPaperTime").val();
	  	 
		var data = {
				name:name,
				courseId:course,
				total:total,
				times:times,
				id:id
			};
			var loadtip = xtip.load('生成中...');
		    $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>generate/creat.do?",
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip);		        	
		        	xtip.alert('组卷成功','s'); 		        	
		        	window.location.href="<%=basePath %>exam/index.do";
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }		       
		    }); 
	
   }
   function backs(){  
	   var examPaperId = $("#examPaperId").val();
	   var data = {
			   id:examPaperId
		};
	var loadtip = xtip.load('加载中...')
   $.ajax({
       //发送请求URL，可使用相对路径也可使用绝对路径
       url:"<%=basePath %>generate/tableload.do",
       //发送方式为GET，也可为POST，需要与后台对应
       type:"POST",
       //设置接收格式 
       dataType:"text",
       cache: false,
       timeout:30000,
       traditional:true,
       async: true,
     	//处理数组
       traditional:true, 
       //向后台发送的数据
       data:data,
       //后台返回成功后处理数据，data为后台返回的json格式数据
       success:function(data){
       	xtip.close(loadtip);
       	removeTo();
       	$('#tableHome').html( data );
       } ,
       error: function( data ) {
       	xtip.close(loadtip); 
           xtip.alert(data.message,'e');  
       }
   }); 
   }
   
   $("input").blur(function(){
	  
	    var name=$("#examPaperName").val();
	  	var course=$("#examCourse").val();
	  	var total=$("#examPaperTotalScore").val();
	  	var times=$("#examPaperTime").val();
	    var paperId=$("#examPaperId").val();
	    var id = $(this).attr("attachId");
		var value = $(this).val();
		if(updata==0){
		if(id){
		var data = {
				name:name,
				courseId:course,
				total:total,
				times:times,
				topicId:id,
				value:value,
				id:paperId
			};
			var loadtip = xtip.load('更新中...');
		    $.ajax({
		        //发送请求URL，可使用相对路径也可使用绝对路径
		        url: "<%=basePath%>generate/score.do?",
		        //发送方式为GET，也可为POST，需要与后台对应
		        type:"POST",
		        //设置接收格式 
		        dataType:"json",
		        cache: false,
		        timeout:30000,
		        traditional:true,
		        async: true,
		      	//处理数组
		        traditional:true, 
		        //向后台发送的数据
		        data:data,
		        //后台返回成功后处理数据，data为后台返回的json格式数据
		        success:function(data){
		        	xtip.close(loadtip);		        			        	
		        	tableDataLoad(data.data);
		        	
		        } ,
		        error: function( data ) {
		        	xtip.close(loadtip); 
		        	 xtip.alert(data.message,'e');  
		        }		       
		    }); 
		}
		}else{
			var data = {
					name:name,
					courseId:course,
					total:total,
					times:times,
					topicId:id,
					value:value,
					id:paperId
				};
				var loadtip = xtip.load('更新中...');
			    $.ajax({
			        //发送请求URL，可使用相对路径也可使用绝对路径
			        url: "<%=basePath%>generate/scores.do?",
			        //发送方式为GET，也可为POST，需要与后台对应
			        type:"POST",
			        //设置接收格式 
			        dataType:"json",
			        cache: false,
			        timeout:30000,
			        traditional:true,
			        async: true,
			      	//处理数组
			        traditional:true, 
			        //向后台发送的数据
			        data:data,
			        //后台返回成功后处理数据，data为后台返回的json格式数据
			        success:function(data){
			        	xtip.close(loadtip);		        			        	
			        	tableDataLoad(data.data);
			        	
			        } ,
			        error: function( data ) {
			        	xtip.close(loadtip); 
			        	 xtip.alert(data.message,'e');  
			        }		       
			    }); 
		}
   })
   function topicScore(obj){	   
			var id = $(obj).attr("attachId");
			var value = $(obj).val();
			var data = {
					id:id,
					value:value
				};
				var loadtip = xtip.load('更新中...');
			    $.ajax({
			        //发送请求URL，可使用相对路径也可使用绝对路径
			        url: "<%=basePath%>generate/score.do?",
			        //发送方式为GET，也可为POST，需要与后台对应
			        type:"POST",
			        //设置接收格式 
			        dataType:"json",
			        cache: false,
			        timeout:30000,
			        traditional:true,
			        async: true,
			      	//处理数组
			        traditional:true, 
			        //向后台发送的数据
			        data:data,
			        //后台返回成功后处理数据，data为后台返回的json格式数据
			        success:function(data){
			        	xtip.close(loadtip);		        				        	 
			        	tableDataLoad(data.data);
			        } ,
			        error: function( data ) {
			        	xtip.close(loadtip); 
			        	 xtip.alert(data.message,'e');  
			        }		       
			    }); 
		
   } 
      
   function addTopics(){
	   var id=$("#examPaperId").val();
	   window.location.href="<%=basePath %>generate/topicIndex.do?id="+id;
   }
   function deleteTopics(obj){
	   var type = $(obj).attr("attachId");
	   var id=$("#examPaperId").val();
		var data = {
				id:id,
				type:type
			};
	   var loadtip = xtip.load('删除中...');
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath%>generate/deleteTopics.do?",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"json",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);		        	
	        	xtip.alert('删除成功','s'); 		        	
	        	tableDataLoad(data.data);
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }		       
	    })
   }  
	
</script>

 
 
 		

    
    
 