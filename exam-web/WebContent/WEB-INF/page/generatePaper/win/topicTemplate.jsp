<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 
     <c:forEach var="item" items="${ListData}">      
     <c:choose>
     <c:when test="${item.type=='选择题(单)'}">    
     <!-- 选择题大题模板 -->
    <div class="mainbox">
    <div class="leftinfo" style="height: 100%" > 
    <div class="maintj">
    <div class="listtitle" style=" font-size: 13pt;text-align:left">第${item.typeSort}大题、选择题
    <div class="tools"  >    
    	<ul class="toolbar1" style="margin: 0 0 0px 25px;">
        <li class="click" onclick=""><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
        <li class="click" onclick=""><span><img src="<%=basePath %>pagestatic/images/t02.png" /></span>修改</li>
        <li class="click" onclick=""><span><img src="<%=basePath %>pagestatic/images/t03.png" /></span>删除</li>                
        <li class="click" onclick=""><span><img src="<%=basePath %>pagestatic/images/t05.png" /></span>设置</li>
        </ul>
    </div>
    </div>
    </div>     
     
       <table class="imgtable"style="border: 0;">
    
   <thead>     
        
    <tr > 
    <th style="text-align:left;font-size: 13pt;width:100px;">序号</th>       
    <th style="text-align:left;font-size: 13pt;width:200px;">试题类型</th>
    <th style="text-align:left;font-size: 13pt;width:900px;">试题内容</th>
    <th style="text-align:left;font-size: 13pt;width:200px;">标准答案</th>
    <th style="text-align:left;font-size: 13pt;width:150px;">难易程度</th>
    <th style="text-align:center;font-size: 13pt;width:150px;">分数</th>
    <th style="text-align:center;font-size: 13pt;width:247px;">操作</th>    
    </tr>    
    </thead>   
    <c:forEach var="item" items="${item.tbTopicPaper}">              
    <tbody id="tabletb">
     <tr>
        <td style="width:100px; border-right: 0;font-size: 12pt;text-indent: 31px;">${item.serial}</td>       
        <td style="width:100px; border-right: 0;font-size: 12pt; text-align:left">${item.type}</td>               
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;">${item.topic}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.answer}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;">
        <i style=" color: #356155c4;text-align:center;">${item.topics.grade}</i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="topicValue" type="text" class="dfinput" attachId="${item.id}" value="${item.value}" style="width:80px; margin: 5px 42px;  " /></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;text-align:center;">
         <a class="optLink" title="编辑" href="javascript:void(0);" attachId="${item.id}" onclick="topicEdits(this)"><i  style="color:blue">查看</i></a>
         
         <a class="optLink" title="删除" href="javascript:void(0);" attachId="${item.id}" onclick="topicDelete(this)"><i style="color:blue">删除</i></a> 
        </td> 
                 
     </tr>       
    </tbody>
    </c:forEach>    
    </table>
    </div>
    </div>
    </c:when>
    </c:choose>
    </c:forEach> 