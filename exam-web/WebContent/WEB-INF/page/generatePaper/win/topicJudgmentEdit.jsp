<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>试题编辑</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
<link href="<%=basePath %>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />


<script  type="text/javascript"> 
	window.onload = function() { 
	var radios = document.getElementsByTagName("input"); 
	    for (var i = 0; i < radios.length; i++) { 
	        if (radios[i].type == "radio") { 
	            radios[i].ondblclick = function() { 
	                this.checked = false; 
	            } 
	        } 
	    } 
	} 

</script>
</head>

<body>     	 
    <div class="formbody">    
    <div class="formtitle"><span>试题编辑</span></div>   
    <form id="addMennForm">
      <input name="id" type="hidden" class="dfinput" value="${TopicPaper.topics.id}" /> 
      <input id="topicPaperId" type="hidden" class="dfinput" value="${TopicPaper.id}"/> 
      <ul class="forminfo" >
	    <li>
	       <label>试题类别</label>
	       <input id="topicCategory" type="text" class="dfinput" value="${TopicPaper.topics.category}"  style="width:220px" /><i></i>
	    </li>
	    
	    <li>
	    <label>题型</label>
	    <input id="topicType" type="text" class="dfinput" value="${TopicPaper.topics.type}"  style="width:100px" disabled/><i></i>
		</li>
		<li>
		    <label>难度</label>
		    <select class="form-control" id="grade" style="width:160px;float:left;">
			  <option value="low">低</option>
			  <option value="general">一般</option>
			  <option value="high">高</option>
			</select>
			
			<label style="float: left;margin: 0 0 5px 29px;width: 56px;">分值</label>
			<input id="topicValue" type="text" class="dfinput" value="${TopicPaper.value}" style="width:98px;float: left;"/>
			
	    </li>  
		<li>
	       <label>试题内容</label>
	       <textarea id="topicContent" rows="5" style="width:345px">${TopicPaper.topics.content}</textarea><i></i>
	    </li>
	    <li>
	    <label>&nbsp;</label>
	    <input type="radio" name="选项" value="正确" style="width: 50px; height:13px;  margin: 0 0 4px"><strong>正确</strong>
	    <input type="radio" name="选项" value="错误" style="width: 50px; height:13px;  margin: 0 0 4px"><strong>错误</strong>
	    </li>	    	    
	   </ul>
    </form>
    </div> 
      <ul class="forminfo">
       <li><label style="width: 200px;">&nbsp;</label><input name="" type="button" class="btn" value="关闭" onclick="closexip()" /></li>
      </ul>  
    <script type="text/javascript"> 
    $(function(){
		tableDataLoad();
	});
	function tableDataLoad(){ 
		
		
		var loadtip = xtip.load('加载中...')
		$(".form-control").val("${TopicPaper.topics.grade}");
		$("input:radio[value='${TopicPaper.topics.result}']").attr('checked','true');
		xtip.close(loadtip);
	    
	}
	function closexip(){
		
		 window.parent.closep();
		 
	 }
     function saveAdd(){
    	 var id=$("#id").val();
    	 var topicPaperId=$("#topicPaperId").val();
    	 var value=$("#topicValue").val();
    	 var category=$("#topicCategory").val();
    	 var type=$("#topicType").val();
    	 var content=$("#topicContent").val();
    	 var result=$('input:radio:checked').val();
    	 var grade=$('#grade option:selected').text();
			var data = { 
					topicPaperId:topicPaperId,
					value:value,
					id:id,
					category:category,
					type:type,
					content:content,
					grade:grade,
					result:result
			};
    	var loadtip = xtip.load('加载中...')
        $.ajax({
            //发送请求URL，可使用相对路径也可使用绝对路径
            url: "<%=basePath %>generate/update.do",
            //发送方式为GET，也可为POST，需要与后台对应
            type:"POST",
            //设置接收格式 
            dataType:"json",
            cache: false,
            timeout:30000,
            traditional:true,
            async: true, 
            //向后台发送的数据
            data:data,
            //后台返回成功后处理数据，data为后台返回的json格式数据
            success:function(data){
            	xtip.close(loadtip);  
            	
            	  if (!data.requestFlag){
               	   xtip.alert(data.message,'e'); 
                  }else{
                	  xtip.alert('操作成功','s'); 
                	  window.parent.tableDataLoad(data.data);
                  } 
            } ,
            error: function( data ) {
            	xtip.close(loadtip); 
            	 xtip.alert(data.message,'e');  
            }
        }); 
    	
    }
    		
    </script>
    
</body>
</html>

