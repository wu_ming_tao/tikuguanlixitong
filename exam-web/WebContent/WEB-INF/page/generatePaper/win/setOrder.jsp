<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>排序编辑</title>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>
<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
<style>
body {
	    margin: 50px 50px;
}
td {
   font-size: 12pt;
   margin: 0 0;
   width: 100px;
}
td a {
 width: 40px;
}	
</style>
</head>

<body>  	 
   <table style=" width: auto;text-align: center;">
  <tbody>
  <c:forEach var="item" items="${Types}">
    <tr>
      <td >${item}</td>
      <td ><a href="javascript:void(0)" onclick="moveUp(this)">上移</a></td>
      <td ><a href="javascript:void(0)" onclick="moveDown(this)">下移</a></td>
    </tr>
    </c:forEach>   
  </tbody>
</table>
<script type="text/javascript">

function moveUp(_a){
  var _row = _a.parentNode.parentNode;
  //如果不是第一行，则与上一行交换顺序
  var _node = _row.previousSibling;
  while(_node && _node.nodeType != 1){
    _node = _node.previousSibling;
  }
  if(_node){
    swapNode(_row,_node);
  }
}
function moveDown(_a){
  var _row = _a.parentNode.parentNode;
  //如果不是最后一行，则与下一行交换顺序
  var _node = _row.nextSibling;
  while(_node && _node.nodeType != 1){
    _node = _node.nextSibling;
  }
  if(_node){
    swapNode(_row,_node);
  }
}
function swapNode(node1,node2){
  //获取父结点
  var _parent = node1.parentNode;
  //获取两个结点的相对位置
  var _t1 = node1.nextSibling;
  var _t2 = node2.nextSibling;
  //将node2插入到原来node1的位置
  if(_t1)_parent.insertBefore(node2,_t1);
  else _parent.appendChild(node2);
  //将node1插入到原来node2的位置
  if(_t2)_parent.insertBefore(node1,_t2);
  else _parent.appendChild(node1);
}

</script>

</body>
</html>

