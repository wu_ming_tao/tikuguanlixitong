﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="pagestatic/js/jquery.js"></script>
<script type="text/javascript">
$(function(){	
	//顶部导航切换
	$(".nav li a").click(function(){
		$(".nav li a.selected").removeClass("selected")
		$(this).addClass("selected");
	})	
})	

</script>
<style>
.topleft p{
	font-size: 27pt;
    margin: 18px 35px;
    color: aquamarine;
    letter-spacing: 19px;
}

</style>

</head>

<body style="background:url(pagestatic/images/topbg.gif) repeat-x;">

    <div class="topleft">
    <a href="javascript:void(0)"><p>题库系统</p></a>
    </div>
        
    <ul class="nav"> 
    </ul>
            
    <div class="topright">    
    <ul>
    <li><span><img src="pagestatic/images/help.png" title="帮助"  class="helpimg"/></span><a href="#">帮助</a></li>
    <li><a href="javascript:void(0)" onclick="dologon()">退出</a></li>
    </ul>
     
    <div class="user">
    <span>${loginUser.userName}</span>
    <i></i> 
    </div>    
    
    </div>
    
    <script>
    function dologon(){
    	window.parent.location.href="logout.do"; 
    }
    </script>
    
</body>
</html>
