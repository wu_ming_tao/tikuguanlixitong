<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">试卷管理</a></li>
    <li><a href="#">试卷信息</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar">
        <li class="click" onclick="examAdd()"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</a></li>
        <li class="click" onclick="examRefresh()"><span><img src="<%=basePath %>pagestatic/images/t04.png" /></span>刷新</li>
        </ul>
    
        
        <ul class="toolbar1">
        
        </ul>
    
    </div>
    
    
    <table class="imgtable">
    
    <thead>
    <tr>    
    <th>科目</th>
    <th>试卷名称</th>
    <th>创建人</th>
    <th>生成方式</th>
    <th width="300px;">操作按钮</th>
    </tr>
    </thead>
    
    <tbody id="tabletb">
     
    </tbody> 
    </table>
       
    </div>
    
    <div>
      
    </div>
    
   
<script>
	$(function(){
		tableDataLoad();
	});	
	var pageOffset = 1;
	function tableDataLoad(pageItem){ 
		
		
		if (pageItem){
			var data = {
					
					pageOffset:pageItem
			};
			pageOffset = pageItem;
		}
		else{
			var data = {
					
			};
		}
		
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: mainActionUrl + "/exam/tableload.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	$('#tabletb').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    }); 
		
	}
	// 添加
	var xipid;
	function examAdd(){ 		  
		window.location.href="<%=basePath %>generate/index.do";
	}

	function exam(obj){	
		var id = $( obj ).attr("attachId"); 
		window.location.href="<%=basePath %>exam/browser.do?id="+id;
	} 
	function examRefresh(){
		window.location.href="<%=basePath %>exam/index.do";
	}
	function examEdit(obj){ 
		var id = $( obj ).attr("attachId");
		window.location.href="<%=basePath %>generate/index.do?id="+id;
	}
	function examPut(obj){
		var id = $( obj ).attr("attachId");
		window.location.href="<%=basePath %>exam/addExam.do?id="+id;
	}
	// 删除
	function examDelete(obj){
		var id = $( obj ).attr("attachId"); 
		var data = {
			id:id
		};
		var loadtip = xtip.load('删除中...');
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath%>exam/delete.do?",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"json",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	xtip.alert('删除成功','s');  
	        	tableDataLoad();
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    });  
	}
	
	 
	
</script> 
 
 
    
    
    
 