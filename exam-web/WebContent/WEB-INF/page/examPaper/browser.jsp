﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html>
<html>
<head>
<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>jQuery在线考试倒计时界面</title>
    <link href="../static/css/exam/main.css" rel="stylesheet" type="text/css" />
 
    <link href="../static/css/exam/test.css" rel="stylesheet" type="text/css" />

    <style>
        .hasBeenAnswer {
            background: #5d9cec;
            color: #fff;
        }

        .reading h2 {
            width: 100%;
            margin: 20px 0 70px;
            text-align: center;
            line-height: 2;
            font-size: 20px;
            color: #59595b;
        }

            .reading h2 a {
                text-decoration: none;
                color: #59595b;
                font-size: 20px;
            }

                .reading h2 a:hover {
                    color: #2183f1;
                }
    </style>
</head>

<body>
    <div class="main">
        <!--nr start-->
        <div class="test_main">
            <div class="nr_left">
                <div class="test">
                    <form action="" method="post">
                        <div class="test_title">
                            <p class="test_time">
                                <i class="icon iconfont">&#xe6fb;</i><b class="alt-1">01:00</b>
                            </p>
                            <font><input type="button" name="test_jiaojuan" onclick="task()" value="交卷"></font>
                        </div>
                        <c:forEach var="item" items="${ListData}">                        
                        <c:if test="${item.type=='选择题(单)'}">
                        <div class="test_content">
                            <div class="test_content_title">
                                <h2>单选题</h2>
                                <p>
                                <input id="ct_count" name="ct_count" type="hidden" class="dfinput" value="${item.count}" /> 
                                    <span>共</span><i class="content_lit">${item.count}</i><span>题，</span><span>合计</span><i class="content_fs">${item.totalNumber}</i><span>分</span>
                                </p>
                            </div>
                        </div>
                       
                        <c:forEach var="item" items="${item.tbTopicPaper}">
                        <input id="idc_${item.serial}" name="id" type="hidden" class="dfinput" value="${item.id}" /> 
                        <div class="test_content_nr">
                            <ul>

                                <li id="ct_${item.serial}">
                                    <div class="test_content_nr_tt">
                                        <i>${item.serial}</i><span>(${item.value}分)</span><font>${item.topic}</font><b class="icon iconfont">&#xe881;</b>
                                    </div>

                                    <div class="test_content_nr_main">
                                        <ul>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer1${item.serial}"
                                                       id="0_answer_${item.serial}_option_1" value="${item.topics.optionA}" />
                                                <label for="0_answer_${item.serial}_option_1">
                                                    A.${item.topics.optionA}
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer1${item.serial}"
                                                       id="0_answer_${item.serial}_option_2" value="${item.topics.optionB}"/>


                                                <label for="0_answer_${item.serial}_option_2">
                                                    B.${item.topics.optionB}
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer1${item.serial}"
                                                       id="0_answer_${item.serial}_option_3"value="${item.topics.optionC}" />


                                                <label for="0_answer_${item.serial}_option_3">
                                                    C.${item.topics.optionC}
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer1${item.serial}"
                                                       id="0_answer_${item.serial}_option_4"value="${item.topics.optionD}" />


                                                <label for="0_answer_${item.serial}_option_4">
                                                    D.${item.topics.optionD}
                                                </label>
                                            </li>

                                        </ul>
                                    </div>
                                </li>
                              </ul>  
                        </div>
                        </c:forEach>
                        </c:if>
                       </c:forEach>
                        <c:forEach var="item" items="${ListData}">
                        <c:if test="${item.type=='判断题'}">
                        <div class="test_content">
                            <div class="test_content_title">
                                <h2>判断题</h2>
                                <p>
                                <input id="pt_count" name="pt_count" type="hidden" class="dfinput" value="${item.count}" /> 
                                    <span>共</span><i class="content_lit">${item.count}</i><span>题，</span><span>合计</span><i class="content_fs">${item.totalNumber}</i><span>分</span>
                                </p>
                            </div>
                        </div>
                       
                        <c:forEach var="item" items="${item.tbTopicPaper}">
                        <input id="idp_${item.serial}" name="id" type="hidden" class="dfinput" value="${item.id}" /> 
                        <div class="test_content_nr">
                            <ul>

                                <li id="pt_${item.serial}">
                                    <div class="test_content_nr_tt">
                                        <i>${item.serial}</i><span>(${item.value}分)</span><font>${item.topic}</font><b class="icon iconfont">&#xe881;</b>
                                    </div>

                                    <div class="test_content_nr_main">
                                        <ul>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer2${item.serial}"
                                                       id="1_answer_${item.serial}_option_1"value="正确" />


                                                <label for="1_answer_${item.serial}_option_1">
                                                    A.正确
                                                </label>
                                            </li>

                                            <li class="option">

                                                <input type="radio" class="radioOrCheck" name="answer2${item.serial}"
                                                       id="1_answer_${item.serial}_option_2"value="错误" />
                                                <label for="1_answer_${item.serial}_option_2">
                                                    B.错误
                                                </label>
                                            </li>                                            
                                        </ul>
                                    </div>
                                </li>
                              </ul>  
                        </div>
                        </c:forEach>
                        </c:if>
                       </c:forEach>
                       
                       <c:forEach var="item" items="${ListData}">
                        <c:if test="${item.type=='问答题'}">
                        <div class="test_content">
                            <div class="test_content_title">
                                <h2>问答题</h2>
                                <p>
                                <input id="wt_count" name="wt_count" type="hidden" class="dfinput" value="${item.count}" /> 
                                    <span>共</span><i class="content_lit">${item.count}</i><span>题，</span><span>合计</span><i class="content_fs">${item.totalNumber}</i><span>分</span>
                                </p>
                            </div>
                        </div>
                       
                        <c:forEach var="item" items="${item.tbTopicPaper}">
                        <input id="idw_${item.serial}" name="id" type="hidden" class="dfinput" value="${item.id}" /> 
                        <div class="test_content_nr">
                            <ul>

                                <li id="wt_${item.serial}">
                                    <div class="test_content_nr_tt">
                                        <i>${item.serial}</i><span>(${item.value}分)</span><font>${item.topic}</font><b class="icon iconfont">&#xe881;</b>
                                    </div>

                                    <div class="test_content_nr_main">
                                        <ul>

                                            <li class="option_${item.serial}">
                                              <label for="2_answer_${item.serial}_option_2">
                                                <textarea  style="width: 100%;height: 349px;" id="answer3${item.serial}" ></textarea>
                                                </label>
                                            </li>                                                                                 
                                        </ul>
                                    </div>
                                </li>
                              </ul>  
                        </div>
                        </c:forEach>
                        </c:if>
                       </c:forEach>

                    </form>
                </div>

            </div>
            <div class="nr_right">
                <div class="nr_rt_main">
                    <div class="rt_nr1">
                        <div class="rt_nr1_title">
                            <h1>
                                <i class="icon iconfont">&#xe692;</i>答题卡
                            </h1>
                            <p class="test_time">
                                <i class="icon iconfont">&#xe6fb;</i><b class="alt-1">01:00</b>
                            </p>
                        </div>
                        <c:forEach var="item" items="${ListData}">
                        <c:if test="${item.type=='选择题(单)'}">
                        <div class="rt_content">
                            <div class="rt_content_tt">
                                <h2>单选题</h2>
                                <p>
                                    <span>共</span><i class="content_lit">${item.count}</i><span>题</span>
                                </p>
                            </div>
                            <div class="rt_content_nr answerSheet">
                                <ul>
                                   <c:forEach var="item" items="${item.tbTopicPaper}">
                                    <li><a href="#ct_${item.serial}">${item.serial}</a></li>

                                    </c:forEach>

                                </ul>
                            </div>
                        </div>
                        </c:if>
                        </c:forEach>
                        <c:forEach var="item" items="${ListData}">
                        <c:if test="${item.type=='判断题'}">
                        <div class="rt_content">
                            <div class="rt_content_tt">
                                <h2>判断题</h2>
                                <p>
                                    <span>共</span><i class="content_lit">${item.count}</i><span>题</span>
                                </p>
                            </div>
                            <div class="rt_content_nr answerSheet">
                                <ul>
                                   <c:forEach var="item" items="${item.tbTopicPaper}">
                                    <li><a href="#pt_${item.serial}">${item.serial}</a></li>

                                    </c:forEach>

                                </ul>
                            </div>
                        </div>
                        </c:if>
                        </c:forEach>
                        <c:forEach var="item" items="${ListData}">
                        <c:if test="${item.type=='问答题'}">
                        <div class="rt_content">
                            <div class="rt_content_tt">
                                <h2>问答题</h2>
                                <p>
                                    <span>共</span><i class="content_lit">${item.count}</i><span>题</span>
                                </p>
                            </div>
                            <div class="rt_content_nr answerSheet">
                                <ul>
                                   <c:forEach var="item" items="${item.tbTopicPaper}">
                                    <li><a href="#wt_${item.serial}">${item.serial}</a></li>

                                    </c:forEach>

                                </ul>
                            </div>
                        </div>
                        </c:if>
                                           
                       </c:forEach>
                    </div>

                </div>
            </div>
        </div>
        <!--nr end-->


        <div class="foot"></div>

       
    </div>

    <script src="../static/js/jquery-1.11.3.min.js"></script>
    <script src="../static/js/jquery.easy-pie-chart.js"></script>
    <!--时间js-->
    <script src="../static/js/jquery.countdown.js"></script>
    <script>
        window.jQuery(function ($) {
            "use strict";

            $('time').countDown({
                with_separators: false
            });
            $('.alt-1').countDown({
                css_class: 'countdown-alt-1'
            });
            $('.alt-2').countDown({
                css_class: 'countdown-alt-2'
            });

        });


        $(function () {
            $('li.option label').click(function () {                
                var examId = $(this).closest('.test_content_nr_main').closest('li').attr('id'); /*得到题目ID*/              
                var cardLi = $('a[href=#' + examId + ']'); /*根据题目ID找到对应答题卡*/
                /*设置已答题*/
                if (!cardLi.hasClass('hasBeenAnswer')) {
                    cardLi.addClass('hasBeenAnswer');
                }

            });
        });
        
        var names ;
        var number =0;
        function task(){            
            var ctcount = $("#ct_count").val(); 
            var ptcount = $("#pt_count").val(); 
            var wtcount = $("#wt_count").val();                      
        	for(number=1;number<=ctcount;number++)
            {
        		var idc = $("#idc_"+number+"").val();
        		names= "answer1"+number ;
        		var result=$("input:radio[name="+names+"]:checked").val();
        		var data = { 
        				id:idc,
        				result:result,
        				number:number
    			};			
    		var loadtip = xtip.load('加载中...')
    	    $.ajax({
    	        //发送请求URL，可使用相对路径也可使用绝对路径
    	        url: "<%=basePath %>exam/results.do",
    	        //发送方式为GET，也可为POST，需要与后台对应
    	        type:"POST",
    	        //设置接收格式 
    	        dataType:"text",
    	        cache: false,
    	        timeout:30000,
    	        traditional:true,
    	        async: true,
    	      	//处理数组
    	        traditional:true, 
    	        //向后台发送的数据
    	        data:data,
    	        //后台返回成功后处理数据，data为后台返回的json格式数据
    	        success:function(data){
    	        	xtip.close(loadtip);    	        	
    	        } ,
    	        error: function( data ) {
    	        	xtip.close(loadtip); 
    	        	 xtip.alert(data.message,'e');  
    	        }
    	      })
        	}
        	for(number=1;number<=ptcount;number++)
            {
        		var idp = $("#idp_"+number+"").val();
        		names= "answer2"+number ;
        		var result=$("input:radio[name="+names+"]:checked").val();
        		var data = { 
        				id:idp,
        				result:result,
        				number:number
    			};			
    		var loadtip = xtip.load('加载中...')
    	    $.ajax({
    	        //发送请求URL，可使用相对路径也可使用绝对路径
    	        url: "<%=basePath %>exam/results.do",
    	        //发送方式为GET，也可为POST，需要与后台对应
    	        type:"POST",
    	        //设置接收格式 
    	        dataType:"text",
    	        cache: false,
    	        timeout:30000,
    	        traditional:true,
    	        async: true,
    	      	//处理数组
    	        traditional:true, 
    	        //向后台发送的数据
    	        data:data,
    	        //后台返回成功后处理数据，data为后台返回的json格式数据
    	        success:function(data){
    	        	xtip.close(loadtip);    	        	
    	        } ,
    	        error: function( data ) {
    	        	xtip.close(loadtip); 
    	        	 xtip.alert(data.message,'e');  
    	        }
    	      })
        	}
        	for(number=1;number<=wtcount;number++)
            {
        		var idp = $("#idw_"+number+"").val();
        		names= "answer3"+number ;
        		var result =$("#"+names+"").val();
        		var data = { 
        				id:idp,
        				result:result,
        				number:number
    			};			
    		var loadtip = xtip.load('加载中...')
    	    $.ajax({
    	        //发送请求URL，可使用相对路径也可使用绝对路径
    	        url: "<%=basePath %>exam/results.do",
    	        //发送方式为GET，也可为POST，需要与后台对应
    	        type:"POST",
    	        //设置接收格式 
    	        dataType:"text",
    	        cache: false,
    	        timeout:30000,
    	        traditional:true,
    	        async: true,
    	      	//处理数组
    	        traditional:true, 
    	        //向后台发送的数据
    	        data:data,
    	        //后台返回成功后处理数据，data为后台返回的json格式数据
    	        success:function(data){
    	        	xtip.close(loadtip);    	        	
    	        } ,
    	        error: function( data ) {
    	        	xtip.close(loadtip); 
    	        	 xtip.alert(data.message,'e');  
    	        }
    	      })
        	}
        	var id = $("#idc_1").val();
        	var data = { 
    				id:id,
			};	
        	var loadtip = xtip.load('加载中...')
    	    $.ajax({
    	        //发送请求URL，可使用相对路径也可使用绝对路径
    	        url: "<%=basePath %>exam/course.do",
    	        //发送方式为GET，也可为POST，需要与后台对应
    	        type:"POST",
    	        //设置接收格式 
    	        dataType:"text",
    	        cache: false,
    	        timeout:30000,
    	        traditional:true,
    	        async: true,
    	      	//处理数组
    	        traditional:true, 
    	        //向后台发送的数据
    	        data:data,
    	        //后台返回成功后处理数据，data为后台返回的json格式数据
    	        success:function(data){
    	        	xtip.close(loadtip);    	        	
    	        } ,
    	        error: function( data ) {
    	        	xtip.close(loadtip); 
    	        	 xtip.alert(data.message,'e');  
    	        }
    	      })
        	window.location.href="<%=basePath %>index.do";
        }
    </script>


</body>

</html>

