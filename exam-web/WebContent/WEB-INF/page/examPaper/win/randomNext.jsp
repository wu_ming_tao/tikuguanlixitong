<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/page/power/css/power.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>pagestatic/css/common.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath %>static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=basePath %>pagestatic/jquery-easyui-1.7.0/themes/icon.css"> 
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>pagestatic/jquery-easyui-1.7.0/jquery.easyui.min.js"></script>


<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
 
</head> 
<body>
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">试卷管理</a></li>
    <li><a href="#">智能组卷</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    <div class="formtitle">
    
    <span>设置试卷参数</span></div>  
    
    <form id="addMennForm">
    <ul class="forminfo" >
     <li>
	    <label>试卷名称</label>
	    <input id="examPaperName" type="text" class="dfinput" value="${TbExamPaper.id}"  style="width:220px" /><i>默认为 智能试卷 </i>
	 </li>
	 <li>
	    <label>试卷科目</label>
	    <input id="examCourse" type="text" class="dfinput" value=""  style="width:220px" /><i></i>
	 </li>
	 <li>
	    <label>试卷总分值</label>
	    <input id="examPaperTotalScore" type="text" class="dfinput" value=""  style="width:220px" /><i></i>
	 </li>
    </ul>
    </form>
     <ul class="forminfo">
       <li>
       <label style="width: 200px;">&nbsp;</label>
       <input name="" type="button" class="btn" value="下一步" onclick="next()" />
       <input name="" type="button" class="btn" value="返回" onclick="cance()" />
       </li>
     </ul> 
    </div>
    
   
<script>
   function next(){ 
	     var name=$("#examPaperName").val();
	  	 var course=$("#examCourse").val();
	  	 var total=$("#examPaperTotalScore").val();
	  	 var type="系统组卷";
			var data = { 
					name:name,
					course:course,
					total:total,
					type : type
			};
  	var loadtip = xtip.load('加载中...')
  	$.ajax({
        //发送请求URL，可使用相对路径也可使用绝对路径
        url: "<%=basePath %>exam/next.do",
        //发送方式为GET，也可为POST，需要与后台对应
        type:"POST",
        //设置接收格式 
        dataType:"json",
        cache: false,
        timeout:30000,
        traditional:true,
        async: true, 
        //向后台发送的数据
        data:data,
        //后台返回成功后处理数据，data为后台返回的json格式数据
        success:function(data){
        	xtip.close(loadtip);  	
        	  if (!data.requestFlag){
           	   xtip.alert(data.message,'e'); 
              }else{
            	  xtip.alert('操作成功','s'); 
              } 
        } ,
        error: function( data ) {
        	xtip.close(loadtip); 
        	 xtip.alert(data.message,'e');  
        }
    }); 
	   
	   
	   
   }
	
	
	
</script> 
 
 
 		
</body>
</html>   
    
    
 