<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>默认首页</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
<link href="<%=basePath %>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 
</head> 
<body>   
    <div class="formbody">
    <div class="formtitle" style="margin-bottom: 70px;" >
    
    <span>设置试卷参数</span></div>  
    
    <form id="addMennForm" style="margin: 0 0 72px;" >
    <ul class="forminfo" style="margin: 0 0 10px 101px;"  >
     <li  style="margin-bottom: 30px;"  >
	    <label style="width: 100px;">试卷名称</label>
	    <input id="examPaperName" type="text" class="dfinput" value=""  style="width:300px" /><i>默认为 智能试卷 </i>
	 </li>
	 <li style="margin-bottom: 30px;" >
	    <label style="width: 100px;">试卷科目</label>
	    <input id="examCourse" type="text" class="dfinput" value=""  style="width:300px" /><i></i>
	 </li>
	 <li style="margin-bottom: 30px;" >
	    <label style="width: 100px;">试卷总分</label>
	    <input id="examPaperTotalScore" type="text" class="dfinput" value=""  style="width:80px" /><i></i>
	 </li>
    </ul>
    </form>
     <ul class="forminfo">
       <li>
       <label style="width: 200px;">&nbsp;</label>
       <input name="" type="button" class="btn" value="下一步" onclick="next()" style="width:80px;margin: 0 15px ;"  />
       <input name="" type="button" class="btn" value="取消" onclick="cancel()" style="width:80px;margin: 0 15px ;"  />
       </li>
     </ul> 
    </div>
    
   
<script>
   function next(){ 	   
	     var name=$("#examPaperName").val();
	  	 var course=$("#examCourse").val();
	  	 var total=$("#examPaperTotalScore").val();
	  	 var type="系统组卷";
		 var data = { 
					name:name,
					course:course,
					total:total,
					type : type
			};
		 var loadtip = xtip.load('加载中...')
  
		 $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: "<%=basePath%>exam/next.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);	        
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    }); 
	window.parent.opean(id);
	
  	window.parent.close();
	   
   }
	
	
	
</script> 
 
 
 		
</body>
</html>   
    
    
 