<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 
 
  <c:if test="${not pageModel.hasData}">
         暂无数据
  </c:if>

   <c:forEach var="item" items="${pageModel.resultData}">
     <tr>       
        <td>${item.courseId }</td>
        <td>${item.name }</td>
        <td>${item.createdBy }</td>
        <td>${item.generationType }</td>  
        <td> 
         <a class='optLink' title='编辑' href='javascript:void(0);' attachId='${item.id }' onclick='exam(this)'><i  style='color:blue'>浏览</i></a>               
         <a class='optLink' title='编辑' href='javascript:void(0);' attachId='${item.id }' onclick='examEdit(this)'><i  style='color:blue'>编辑</i></a>        
         <a class='optLink' title='删除' href='javascript:void(0);' attachId='${item.id }' onclick='examDelete(this)'><i style='color:blue'>删除</i></a>  
         <a class='optLink' title='删除' href='javascript:void(0);' attachId='${item.id }' onclick='examPut(this)'><i style='color:blue'>发布</i></a>  
         </td>
     </tr>  
   </c:forEach>
   
   <c:if test="${ pageModel.hasData}">
      
      <tr>
		<td colspan="5" style="line-height:30px">
	        <div class="pagin">
		    	<div class="message">共<i class="blue">${pageModel.pageModel.total}</i>条记录，共${pageModel.pageModel.pageTotal} 页，当前显示第&nbsp;<i class="blue">${pageModel.pageModel.pageNum}&nbsp;</i>页</div>
		        <ul class="paginList">
		        
		          <c:if test="${pageModel.pageModel.pageNum eq 1}"> 
		          <li class="paginItem">
		           <a href="javascript:;" style='opacity: 0.2'> <span class="pagepre"></span> </a>
		            </li>
		          </c:if>
		          <c:if test="${pageModel.pageModel.pageNum gt 1}"> 
		            <li class="paginItem">
		            <a href="javascript:;" onclick="tableDataLoad('1')"><span class="pagepre"></span></a>
		            </li>
		          </c:if>
		        
		        <c:if test="${ pageModel.pageModel.pageTotal < 5}">
		        	<c:forEach var="pageIndex" begin="1" end="${pageModel.pageModel.pageTotal}">
		            <c:if test="${pageIndex eq pageModel.pageModel.pageNum}">
		        	   <li class="paginItem" ><a href="javascript:;" style="color:black" onclick="tableDataLoad('${pageIndex }')"><center style="margin-left: -21px;">${pageIndex }</center></a></li>
		        	</c:if> 
		        	
		        	<c:if test="${pageIndex != pageModel.pageModel.pageNum}">
		        	   <li class="paginItem" ><a href="javascript:;" onclick="tableDataLoad('${pageIndex }')"><center style="margin-left: -21px;">${pageIndex }</center></a></li>
		        	</c:if> 
		        	
		          </c:forEach> 
		        </c:if>
		        
		        <c:if test="${pageModel.pageModel.pageTotal - pageModel.pageModel.pageNum <= 5 && pageModel.pageModel.pageTotal > 5}">
		          <c:forEach var="pageIndex" begin="${pageModel.pageModel.pageTotal - 5}" end="${pageModel.pageModel.pageTotal}">
		            <c:if test="${pageIndex - pageModel.pageModel.pageNum < 5}">
		        	   <c:if test="${pageIndex eq pageModel.pageModel.pageNum}">
		        	   <li class="paginItem" ><a href="javascript:;" style="color:black" onclick="tableDataLoad('${pageIndex }')"><center style="margin-left: -21px;">${pageIndex }</center></a></li>
		        	</c:if> 
		        	
		        	<c:if test="${pageIndex != pageModel.pageModel.pageNum}">
		        	   <li class="paginItem" ><a href="javascript:;" onclick="tableDataLoad('${pageIndex }')"><center style="margin-left: -21px;">${pageIndex }</center></a></li>
		        	</c:if> 
		        	</c:if> 
		          </c:forEach> 
		        </c:if>
		        
		        <c:if test="${pageModel.pageModel.pageTotal - pageModel.pageModel.pageNum > 5}">
		           <c:forEach var="pageIndex" begin="${pageModel.pageModel.pageNum}" end="${pageModel.pageModel.pageTotal}">
		            <c:if test="${pageIndex - pageModel.pageModel.pageNum < 5}">
		        	    <c:if test="${pageIndex eq pageModel.pageModel.pageNum}">
		        	   <li class="paginItem" ><a href="javascript:;" style="color:black" onclick="tableDataLoad('${pageIndex }')"><center style="margin-left: -21px;">${pageIndex }</center></a></li>
		        	</c:if> 
		        	
		        	<c:if test="${pageIndex != pageModel.pageModel.pageNum}">
		        	   <li class="paginItem" ><a href="javascript:;" onclick="tableDataLoad('${pageIndex }')"><center style="margin-left: -21px;">${pageIndex }</center></a></li>
		        	</c:if> 
		        	</c:if> 
		          </c:forEach> 
		        </c:if>
		         
		          <c:if test="${pageModel.pageModel.pageNum < pageModel.pageModel.pageTotal}">
		            <li class="paginItem"><a href="javascript:;" onclick="tableDataLoad('${pageModel.pageModel.pageTotal }')"><span class="pagenxt"></span></a></li>
		          </c:if>
		          
		           <c:if test="${pageModel.pageModel.pageNum eq pageModel.pageModel.pageTotal}">
		            <li class="paginItem"><span class="pagenxt" style='opacity: 0.2'></span></li>
		          </c:if> 
		        </ul>
		    </div>
		</td>           
     </tr>
      
   
   </c:if>
   
     
