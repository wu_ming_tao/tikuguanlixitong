<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">菜单权限</a></li>
    <li><a href="#">菜单列表</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar">
        <li class="click" onclick="menuAdd()"><span><img src="<%=basePath %>pagestatic/images/t01.png" /></span>添加</li>
       <li class="click" onclick="treeRefresh()"><span><img src="<%=basePath %>pagestatic/images/t04.png" /></span>刷新树</li>
        </ul>
        
        
        <ul class="toolbar1">
        
        </ul>
    
    </div>
    
    
    <table class="imgtable">
    
    <thead>
    <tr>
    <th width="200px;">操作按钮</th>
    <th>菜单名称</th>
    <th>菜单编码</th>
    <th>菜单路径</th>
    <th>排序字段</th> 
    </tr>
    </thead>
    
    <tbody id="tabletb">
     
    </tbody> 
    </table>
       
    </div>
    
    <div>
      
    </div>
    
   
<script>
	$(function(){
		tableDataLoad();
	});
	
	function treeRefresh(){
		$('#treeid').val('');
		load();
		tableDataLoad();
	}
	var pageOffset = 1;
	function tableDataLoad(pageItem){
		debugger
		var treeId = $('#treeid').val();
		
		if (pageItem){
			var data = {
					parentId : treeId,
					pageOffset:pageItem
			};
			pageOffset = pageItem;
		}
		else{
			var data = {
					parentId : treeId
			};
		}
		
		var loadtip = xtip.load('加载中...')
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: mainActionUrl + "/power/tableload.do",
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"text",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	$('#tabletb').html( data );
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    }); 
		
	}
	
	// 添加
	function menuAdd(){ 
		
		var treeId = $('#treeid').val(); 
		
		xtip.open({
			type: 'url',
			content : mainActionUrl + '/power/edit.do?parentId=' + treeId,
			title: '【添加菜单】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	// 更新
	function menuEdit(obj){ 
		var id = $( obj ).attr("attachId");
		xtip.open({
			type: 'url',
			content : mainActionUrl + '/power/edit.do?id=' + id,
			title: '【修改菜单】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	// 删除
	function menuDelete(obj){
		var id = $( obj ).attr("attachId"); 
		var data = {
			id:id
		};
		var loadtip = xtip.load('删除中...');
	    $.ajax({
	        //发送请求URL，可使用相对路径也可使用绝对路径
	        url: mainActionUrl + "/power/delete.do?id=" + id,
	        //发送方式为GET，也可为POST，需要与后台对应
	        type:"POST",
	        //设置接收格式 
	        dataType:"json",
	        cache: false,
	        timeout:30000,
	        traditional:true,
	        async: true,
	      	//处理数组
	        traditional:true, 
	        //向后台发送的数据
	        data:data,
	        //后台返回成功后处理数据，data为后台返回的json格式数据
	        success:function(data){
	        	xtip.close(loadtip);
	        	xtip.alert('删除成功','s');  
	        	tableDataLoad();
	        } ,
	        error: function( data ) {
	        	xtip.close(loadtip); 
	        	 xtip.alert(data.message,'e');  
	        }
	    });  
	}
	
	// 菜单按钮管理
	function menuBtn(obj){
		var id = $( obj ).attr("attachId"); 
		xtip.open({
			type: 'url',
			content : mainActionUrl + '/power/toMenuBtn.do?id=' + id,
			title: '【按钮管理】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	// 附属管理
	function menuAttach(obj){
		var id = $( obj ).attr("attachId"); 
		xtip.open({
			type: 'url',
			content : '<%=basePath%>/power/toAuth.do?id=' + id,
			title: '【管理附属】',
			width: '700px',
			height: '600px',
			min: true,
			shade: false,
			max: true,
			lock: true,
			closeBtn: true,
			}); 
	}
	
</script> 
 
 
    
    
    
 