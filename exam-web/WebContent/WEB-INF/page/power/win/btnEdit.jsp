<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%> 

<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="<%=basePath %>pagestatic/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.js"></script>
<script type="text/javascript" src="<%=basePath %>pagestatic/js/jquery.serializejson.min.js"></script>

<script src="<%=basePath %>static/asset/js/xtiper.min.js" type="text/javascript"></script>
<link href="<%=basePath %>static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=basePath %>pagestatic/js/common.js"></script>
</head>

<body>
 
    <div class="formbody">
    
    <div class="formtitle"><span>按钮信息</span></div>
    
    <form id="addMennForm">
      <input name="id" type="hidden" class="dfinput" value="${TbBtn.id}" />
      <input name="menuId" type="hidden" class="dfinput" value="${TbBtn.menuId}" />
      <ul class="forminfo">
	    <li>
	       <label>按钮名称</label>
	       <input name="btnName" type="text" class="dfinput" value="${TbBtn.btnName}" /><i></i>
	    </li>
	    <li>
	    <label>按钮编码</label>
	    <input name="btnCode" type="text" class="dfinput"  value="${TbBtn.btnCode}" /><i></i>
	    </li>
	    <li>
	    <label>按钮路径</label>
	    <input name="btnUrl" type="text" class="dfinput"  value="${TbBtn.btnUrl}" /><i></i>
	    </li>
	    
	    <li>
	    <label>按钮类型</label>
	    
	    <cite>
	    <c:if test="${TbBtn.btnType eq 1}">
	     <input name="btnType" type="radio" value="1" checked="checked" />菜单按钮&nbsp;&nbsp;&nbsp;&nbsp;
	     <input name="btnType" type="radio" value="2" />列表按钮
	    </c:if>
	    <c:if test="${TbBtn.btnType eq 2}">
	     <input name="btnType" type="radio" value="1"  />菜单按钮&nbsp;&nbsp;&nbsp;&nbsp;
	     <input name="btnType" type="radio" value="2" checked="checked" />列表按钮
	    </c:if>
	    <c:if test="${empty TbBtn.btnType}">
	     <input name="btnType" type="radio" value="1"  />菜单按钮&nbsp;&nbsp;&nbsp;&nbsp;
	     <input name="btnType" type="radio" value="2" />列表按钮
	    </c:if>
	    
	    </cite> 
	    </li>
	    
	    <li>
	    <label>按钮样式</label>
	    <input name="btnClass" type="text" class="dfinput"  value="${TbBtn.btnClass}" /><i></i>
	    </li>
	    
	    <li>
	    <label>按钮函数</label>
	    <input name="btnFunc" type="text" class="dfinput"  value="${TbBtn.btnFunc}" /><i></i>
	    </li>
	    
	    <li>
	    <label>菜单排序</label>
	    <input name="showOrder" type="text" class="dfinput"  value="${TbBtn.showOrder}" /><i></i>
	    </li>
	    <li>
	    <label>菜单显影</label>
	    <cite>
	    <c:if test="${TbBtn.btnDisplay eq 1}">
	     <input name="btnDisplay" type="radio" value="1" checked="checked" />是&nbsp;&nbsp;&nbsp;&nbsp;
	     <input name="btnDisplay" type="radio" value="0" />否
	    </c:if>
	    <c:if test="${TbBtn.btnDisplay eq 0}">
	     <input name="btnDisplay" type="radio" value="1"  />是&nbsp;&nbsp;&nbsp;&nbsp;
	     <input name="btnDisplay" type="radio" value="0" checked="checked" />否
	    </c:if>
	    <c:if test="${empty TbBtn.btnDisplay}">
	     <input name="btnDisplay" type="radio" value="1"  />是&nbsp;&nbsp;&nbsp;&nbsp;
	     <input name="btnDisplay" type="radio" value="0" />否
	    </c:if>
	    
	    </cite>
	      
	    </li> 
	   </ul>
    </form> 
      <ul class="forminfo">
       <li><label>&nbsp;</label><input name="" type="button" class="btn" value="确认保存" onclick="saveAdd()" /></li>
      </ul>
    </div>
    
    <script type="text/javascript"> 
     function saveAdd(){
    	var data = serializeObject($("#addMennForm")); 
    	var loadtip = xtip.load('加载中...')
        $.ajax({
            //发送请求URL，可使用相对路径也可使用绝对路径
            url: "<%=basePath %>power/${method}.do",
            //发送方式为GET，也可为POST，需要与后台对应
            type:"POST",
            //设置接收格式 
            dataType:"json",
            cache: false,
            timeout:30000,
            traditional:true,
            async: true, 
            //向后台发送的数据
            data:data,
            //后台返回成功后处理数据，data为后台返回的json格式数据
            success:function(data){
            	xtip.close(loadtip);  
            	
            	  if (!data.requestFlag){
               	   xtip.alert(data.message,'e'); 
                  }else{
                	  xtip.alert('操作成功','s'); 
                	  window.parent.tableDataLoad();
                  } 
            } ,
            error: function( data ) {
            	xtip.close(loadtip); 
            	 xtip.alert(data.message,'e');  
            }
        }); 
    	
    }
     
     
    		
    </script>
    
</body>
</html>
