<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
    <% 
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<style>



.mainright {
    width: 298px;
    position: absolute;
    top: 8px;
    right: 160px;
}
</style>
   <div class="formbody">
    <div class="formtitle" style="margin-bottom: 20px;" >
    
    <span>设置试卷参数</span>
    </div>  
    <div class="mainbox">   
    <div class="mainleft">        
    <div class="leftinfo" style="width:800px" >
    <div class="listtitle">试卷参数设置</div>
    <div class="maintj">  
    <form id="addMennForm" style="margin: 0 0 20px;" >
    <ul class="forminfo" style="margin: 22px 0 10px -4px;"  >
     <li  style="margin-bottom: 30px;width: 500px;"  >
	    <label style="width: 100px;">试卷名称</label>
	    <input id="examPaperName" type="text" class="dfinput" value=""  style="width:300px" /><i></i>
	 </li>
	 <li style="width: 500px;margin-bottom: 30px;" >
	    <label style="width: 100px;">试卷科目</label>
	    <input id="examCourse" type="text" class="dfinput" value=""  style="width:300px" /><i></i>
	 </li>
	 <li style="margin-bottom: 30px;width: 500px;">
	    <label style="width: 100px;">试卷总分</label>
	    <input id="examPaperTotalScore" type="text" class="dfinput" value=""  style="width:80px; margin: 0 0 0 -223px;" /><i></i>
	 </li>
    </ul>
    </form>
    </div>  
    </div>
    </div>  
     
    
             
    <div class="mainright" style="width:700px">
    <div class="dflist" style="width:700px">
    <div class="listtitle">试题参数设置</div>   
    <div class="parameter" style="width:500px;margin: 8px 28px 5px 20px;">
    <table class="imgtable"style="border: 0;">
    
    <thead>
    <tr >    
    <th style="text-align:left;font-size: 13pt;">题型</th>
    <th style="text-align:center;font-size: 13pt;">题数</th>
    <th style="text-align:center;font-size: 13pt;">分值</th>    
    </tr>
    </thead>
    <tbody id="tabletb">
     <tr>       
        <td style="width:200px; border-right: 0;font-size: 12pt; text-align:left">选择题</td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceNumber" type="text" class="dfinput" value=""  style="width:80px;margin: 5px 50px  " /><i></i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="choiceScore" type="text" class="dfinput" value=""  style="width:60px; margin: 5px 0 5px 50px" /><i></i></td>                
     </tr> 
     <tr>       
        <td style="width:200px; border-right: 0;font-size: 12pt; text-align:left">判断题</td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentNumber" type="text" class="dfinput" value=""  style="width:80px;margin: 5px 50px  " /><i></i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="judgmentScore" type="text" class="dfinput" value=""  style="width:60px; margin: 5px 0 5px 50px" /><i></i></td>                
     </tr>
     <tr>       
        <td style="width:200px; border-right: 0;font-size: 12pt; text-align:left">问答题</td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerNumber" type="text" class="dfinput" value=""  style="width:80px;margin: 5px 50px  " /><i></i></td>
        <td style="width:200px; border-right: 0;font-size: 12pt;"><input id="answerScore" type="text" class="dfinput" value=""  style="width:60px; margin: 5px 0 5px 50px" /><i></i></td>                
     </tr>  
    </tbody> 
    </table>
    </div>
    </div>  
    </div>  
    </div> 
   
     <ul class="forminfo" style="margin: 131px 0 0 435px;">   

       <li>
       <label style="width: 200px;">&nbsp;</label>
       <input name="" type="button" class="btn" value="下一步" onclick="next()" style="width:80px;margin: 0 15px ;"  />
       <input name="" type="button" class="btn" value="取消" onclick="cancel()" style="width:80px;margin: 0 15px ;"  />
       </li>
     </ul> 
     </div>


  