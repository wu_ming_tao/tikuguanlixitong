<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title>三级导航</title>
	
    <link rel="stylesheet" type="text/css" href="../static/asset/css/left-side-menu.css">
    <link rel="stylesheet" type="text/css" href="../static/asset/font/iconfont.css">
	
    <script type="text/javascript" src="../static/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../static/js/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="../static/asset/js/left-side-menu.js"></script>
	
</head>
<body>
<div class ="top"  style="width: 100%;height: 80px;background-color: #397bc5">
    <button onclick="logon()">退出登录</button>
</div>
<!-- 左边div -->
<div class="left-side-menu" >
    <div class="lsm-expand-btn">
        <div class="lsm-mini-btn">
            <label>
                <input type="checkbox" checked="checked">
                <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="50" cy="50" r="30" />
                    <path class="line--1" d="M0 40h62c18 0 18-20-17 5L31 55" />
                    <path class="line--2" d="M0 50h80" />
                    <path class="line--3" d="M0 60h62c18 0 18 20-17-5L31 45" />
                </svg>
            </label> 
        </div> 
    </div>
    <div class="lsm-container">
        <div class="lsm-scroll" >
            <div class="lsm-sidebar">
                <ul>
                    <li class="lsm-sidebar-item">
                        <a href="javascript:;">
                          <i class="my-icon lsm-sidebar-icon icon_1">
                          </i>
                          <span>系统设置</span>
                          <i class="my-icon lsm-sidebar-more"></i>
                          </a>
                        <ul>
                            <li><a href="javascript:;"><span>菜单权限</span></a></li>
                            <li><a class="active"  href="javascript:;"><span>无线月读</span></a></li>
                            <li><a href="javascript:;"><span>一乐拉面</span></a></li> 
                        </ul>
                    </li>
                   
                    <li class="lsm-sidebar-item">
                        <a href="javascript:;"><i class="my-icon lsm-sidebar-icon icon-users"></i><span>用户管理</span><i class="my-icon lsm-sidebar-more"></i></a>
                        <ul>
                            <li><a href="javascript:;"><span>火之国8</span></a></li>
                            <li><a href="javascript:;"><span>沙之国8</span></a></li>
                            <li><a href="javascript:;"><span>火影忍者8</span></a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>

</div>

<div class="container">

</div>

<script>
 function logon(){
	 window.location.href="logout.do";
 }
</script>
</body>
</html>


