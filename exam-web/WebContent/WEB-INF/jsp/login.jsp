<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>考试系统</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow"> 
      <link rel="stylesheet" href="../static/bootstrap/css/bootstrap.min.4.2.1.css"/> 
    <link rel="stylesheet" href="../static/css/login/style.default.css" id="theme-stylesheet"/> 
     <script src="../static/js/jquery-1.11.3.min.js"></script>
    <script src="../static/bootstrap/js/bootstrap.min.4.2.1.js"></script> 
    <script src="../static/js/jquery.validate.min.js"></script> 
    <!-- Main File-->
    <script src="../static/js/login/front.js"></script>
    <script src="../static/js/common.js"></script>
    
    <!--图标库-->
    <link rel="stylesheet" href="../static/asset/css/all.min.css">
  
  <!--图标库-->
    <script src="../static/asset/js/all.min.js"></script>
  
  <!--核心样式-->
    <link rel="stylesheet" href="../static/asset/css/modallayer.min.css">
   
   <!--插件-->
    <script src="../static/asset/js/modallayer-ie.min.js"></script>
    
    <script src="../static/asset/js/mcommon.js"></script>
    
    <script src="../static/asset/js/xtiper.min.js" type="text/javascript"></script>
    <link href="../static/asset/css/xtiper.css" type="text/css" rel="stylesheet" />
    
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>欢迎登录</h1>
                  </div>
                  <p>考试系统</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form method="post" action="dologin.do"   class="form-validate" id="loginFrom">
                    <div class="form-group">
                      <input id="login-username" type="text" name="userName" required data-msg="请输入用户名" placeholder="用户名" value="" class="input-material">
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="passWord" required data-msg="请输入密码" placeholder="密码" class="input-material">
                    </div>
                    <button id="login" type="button" class="btn btn-primary"  onclick="dologin()" >登录</button> 
                  </form>
                  <br/>
                  <small>没有账号?</small><a href="register.do" class="signup">&nbsp;注册</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
    <script>
    	$(function(){
    		
    
    		$("#form").validate();
    		 
    		
    		/*$("#check2").click(function(){
    			var flag=$('#check2').prop('checked');
    			if(flag){
    				var userName=$("#login-username").val();
	    			var passWord=$("#login-password").val();
	    			$.ajax({
	    				type:"post",
	    				url:"http://localhost:8080/powers/pow/regUsers",
	    				data:{"userName":userName,"passWord":passWord},
	    				async:true,
	    				success:function(res){
	    					alert(res);
	    				}
	    			});
    			}
    		})*/
    	});
    	
    	function dologin(){ 
    
    		if(!$("#loginFrom").valid()){
    			return;
    		} 
			var userName=$("#login-username").val();
			var passWord=$("#login-password").val();
			 
    		var data ={
    	      userName : userName,
    	      passWord : passWord
    		}; 
    		var loadtip = xtip.load('登录中...')
            $.ajax({
                //发送请求URL，可使用相对路径也可使用绝对路径
                url:"dologin.do",
                //发送方式为GET，也可为POST，需要与后台对应
                type:"POST",
                //设置接收格式为JSON
                dataType:"json",
                cache: false,
                async: true,
              	//处理数组
                traditional:true, 
                //向后台发送的数据
                data:data,
                //后台返回成功后处理数据，data为后台返回的json格式数据
                success:function(data){
                	xtip.close(loadtip);
                   if (!data.requestFlag){
                	   xtip.alert(data.message,'e'); 
                   }else{
                	   window.location.href="index.do";
                   }
                } 
            }); 
    	}
    	
    	  
    	
    </script>
  </body>
</html>