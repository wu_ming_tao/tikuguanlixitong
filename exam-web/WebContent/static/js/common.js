 
//获取指定form中的所有的<input>对象  
function getElements(formId) {  
  var form = document.getElementById(formId);  
  var elements = new Array();  
  var tagElements = form.getElementsByTagName('input');  
  for (var j = 0; j < tagElements.length; j++){ 
     elements.push(tagElements[j]); 
  
  } 
  return elements;  
}  
  
//获取单个input中的【name,value】数组 
function inputSelector(element) {  
 if (element.checked)  
   return [element.name, element.value];  
}  
    
function input(element) {  
  switch (element.type.toLowerCase()) {  
   case 'submit':  
   case 'hidden':  
   case 'password':  
   case 'text':  
    return [element.name, element.value];  
   case 'checkbox':  
   case 'radio':  
    return inputSelector(element);  
  }  
  return false;  
}  
  
//组合URL 
function serializeElement(element) {  
  var method = element.tagName.toLowerCase();  
  var parameter = input(element);  
   
  if (parameter) {  
   var key = encodeURIComponent(parameter[0]);  
   if (key.length == 0) return;  
   
   if (parameter[1].constructor != Array)  
    parameter[1] = [parameter[1]];  
      
   var values = parameter[1];  
   var results = [];  
   for (var i=0; i<values.length; i++) {  
    results.push(key + '=' + encodeURIComponent(values[i]));  
   }  
   return results.join('&');  
  }  
 }  
  
//调用方法   
function serializeForm(formId) {  
  var elements = getElements(formId);  
  var queryComponents = new Array();  
   
  for (var i = 0; i < elements.length; i++) {  
   var queryComponent = serializeElement(elements[i]);  
   if (queryComponent)  
    queryComponents.push(queryComponent);  
  }   
  return queryComponents.join('&'); 
}    

function alertMessage(title,message){
	let option = {
		      hook: {
		        initStart: function () {
		          // 检查之前老旧实例如果存在则销毁
		          if (document.querySelector('#modal-layer-container'))
		            ModalLayer.removeAll();
		        }
		      },
		      popupTime: 0,
		      type: 'alert',
		      title: title,
		      dragOverflow: true,
		      content: '<i class="fas fa-fault" style="color: deepskyblue"></i>' + message,
		    };

		    ModalLayer.alert(option);
}

function modelMsg(pupupTime,message){
	let option = {
		      popupTime: pupupTime,
		      hook: {
		        initStart: function () {
		          // 检查之前老旧实例如果存在则销毁
		          if (document.querySelector('#modal-layer-container'))
		            ModalLayer.removeAll();
		        }
		      },
		      displayProgressBar: true,
		      displayProgressBarPos: 'top',
		      displayProgressBarColor: 'red',
		      content: '<i class="fas fa-check" style="color: deepskyblue"></i>' + message,
		    };

		    ModalLayer.msg(option);
}

function confimrAlert(title,content,oktext,notext){
	let option = {
		      title: title,
		      popupTime: 10,
		      hook: {
		        initStart: function () {
		          // 检查之前老旧实例如果存在则销毁
		          if (document.querySelector('#modal-layer-container'))
		            ModalLayer.removeAll();
		        }
		      },
		      displayProgressBar: true,
		      displayProgressBarPos: 'left',
		      content: content,
		      okText: oktext,
		      noText: notext,
		      callback: {
		        ok: function () {
		          this.hide();
		          return true;
		        },
		        no: function () {
		           this.hide();
		           return false;
		        }
		      }
		    }

		    ModalLayer.confirm(option);
}

