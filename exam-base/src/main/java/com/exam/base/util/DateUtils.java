package com.exam.base.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	/**
	 * 时间转换
	 * @param value
	 * @param string
	 * @param string2
	 * @return
	 */
	public static Object parseDate(String value, String shortFormate, String longFormate) {
		
		try {
			   SimpleDateFormat formatter = new SimpleDateFormat(longFormate);
			   ParsePosition pos = new ParsePosition(0);
			   Date strtodate = formatter.parse(value, pos);
			   return strtodate;
		} catch (Exception e) {
			e.printStackTrace(); 
		} 
		
		try {
			   SimpleDateFormat formatter = new SimpleDateFormat(shortFormate);
			   ParsePosition pos = new ParsePosition(0);
			   Date strtodate = formatter.parse(value, pos);
			   return strtodate;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
}
