package com.exam.base.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

/**
 * 请求对象转换
 * @author Administrator
 *
 */
public class ServletBeanTools {
	/**自动匹配参数赋值到实体bean中
	 * @author LiuDing
	 * 2014-2-16 下午10:23:37
	 * @param bean
	 * @param request
	 * @throws Exception 
	 * @throws InstantiationException 
	 */
	public static <T> T populate(Class<T> clazz, HttpServletRequest request) throws Exception{

		T bean = clazz.newInstance();
		Method ms[] = clazz.getDeclaredMethods();
		
		List<Method> lstMethods = new ArrayList<Method>(ms.length + 10); 
		
		for (Method method : ms) {
			lstMethods.add(method);
		}
		Class temClazz = clazz;
		while(true) 
		{
			temClazz = temClazz.getSuperclass();
			if (Object.class == temClazz)
			{
				break;
			}
			Method temMs[] = temClazz.getDeclaredMethods();
			
			for (Method method : temMs) {
				lstMethods.add(method);
			}
		}
		
		String mname;
		String field;
		String fieldType;
		String value;
		for(Method m : lstMethods){
			mname = m.getName();
			if(!mname.startsWith("set")){
				continue;
			}
			try{
				field = mname.toLowerCase().charAt(3) + mname.substring(4, mname.length());
				value = request.getParameter(field);
				if(StringUtils.isEmpty(value)){
					continue;
				}
				fieldType = m.getParameterTypes()[0].getName();
				//以下可以确认value为String类型
				if(String.class.getName().equals(fieldType)){
					m.invoke(bean, (String)value);
				}else if(Integer.class.getName().equals(fieldType)){
					m.invoke(bean, Integer.valueOf((String)value));
				}else if(Short.class.getName().equals(fieldType)){
					m.invoke(bean, Short.valueOf((String)value));
				}else if(Float.class.getName().equals(fieldType)){
					m.invoke(bean, Float.valueOf((String)value));
				}else if(Double.class.getName().equals(fieldType)){
					m.invoke(bean, Double.valueOf((String)value));
				}else if(Date.class.getName().equals(fieldType)){
					m.invoke(bean, DateUtils.parseDate((String)value, "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss"));
				}else{
					m.invoke(bean, value);
				}
			}catch(Exception e){
				e.printStackTrace();
				continue;
			}
		}
		return bean;
	} 
	
	/**
	 * 转换输入流
	 * @param is
	 * @return
	 */
	public static String convertStreamToString(InputStream is) {

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                 e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
