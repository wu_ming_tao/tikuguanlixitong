package com.exam.base.util;

public class ArrayUtils {
	
	/**判空
	 * @param classArrays
	 * @return
	 */
	public static boolean isEmpty(Class<?>[] classArrays)
	{
		if (classArrays == null)
		{
			return false;
		}
		if (classArrays.length == 0)
		{
			return false;
		}
		return true; 
	}
}
