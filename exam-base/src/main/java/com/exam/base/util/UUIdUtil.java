package com.exam.base.util;

import java.util.UUID;

/**
 * 临时id
 * @author Administrator
 *
 */
public class UUIdUtil {
	
	private UUIdUtil(){
		
	}
	
	/**
	 * 获取UUID
	 * @return
	 */
	public static String getUUID(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
