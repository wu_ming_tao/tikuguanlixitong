package com.exam.base.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

import com.exam.base.dao.IAbstractDao;
import com.exam.base.model.IAbstractModel;
import com.exam.base.service.IAbstractService;
import com.exam.base.util.UUIdUtil;

public abstract class AbstractServiceImpl<T extends IAbstractModel> implements IAbstractService<T> {
	/**
	 * 子类必须实现返回dao
	 * @return
	 */
	protected abstract IAbstractDao<T> getBaseDao();

	public void add(T mainModel)
	{
		if (StringUtils.isEmpty(mainModel.getId()))
		{
			mainModel.setId(UUIdUtil.getUUID());
		}

		getBaseDao().add(mainModel);
	}

	public void batchAdd(List<T> addList)
	{
		if (addList.isEmpty())
		{
			return;
		}
		for (T mainModel : addList)
		{
			if (StringUtils.isEmpty(mainModel.getId()))
			{
				mainModel.setId(UUIdUtil.getUUID());
			}
		}
		getBaseDao().batchCreate(addList, "insert");
	}

	public void update(T mainModel)
	{
		getBaseDao().update(mainModel);
	}

	public void batchUpdate(List<T> updateList)
	{
		if (updateList.isEmpty())
		{
			return;
		}
		getBaseDao().batchUpdate(updateList, "updateByPrimaryKeySelective");
	}

	public void delete(String id)
	{
		getBaseDao().delete(id);
	}

	public void delete(String[] ids)
	{
		getBaseDao().deleteByIds(Arrays.asList(ids));
	}

	public void delete(T condition)
	{
		getBaseDao().deleteByModel(condition);
	}

	public T findById(String id)
	{
		return getBaseDao().findById(id);
	}

	public List<T> findByIds(String[] ids)
	{
		return getBaseDao().findByIds(Arrays.asList(ids));
	}

	public List<T> queryList(T condition)
	{
		return getBaseDao().findByModel(condition);
	}
	
	public List<T> findByQuery(String query)
	{
		return getBaseDao().findByQuery(query);
	}
	
	public int countByQuery(String query) {
		return getBaseDao().countByQuery(query);
	}
	public int queryCount(T condition)
	{
		return getBaseDao().countByModel(condition);
	}

	public List<T> queryPage(T condition)
	{
		return getBaseDao().findByModelPage(condition);
	}
}
