package com.exam.base.service;

import java.util.List;

import com.exam.base.model.IAbstractModel;

public interface IAbstractService<T extends IAbstractModel> {
	/**
	 * 新增
	 * @param mainModel
	 */
	void add(T mainModel);

	/**
	 * 批量新增
	 * @param addList
	 */
	void batchAdd(List<T> addList);

	/**
	 * 更新
	 * @param mainModel
	 */
	void update(T mainModel);

	/**
	 * 批量更新
	 * @param updateList
	 */
	void batchUpdate(List<T> updateList);

	/**
	 * 删除
	 * @param id
	 */
	void delete(String id);

	/**
	 * 删除
	 * @param ids
	 */
	void delete(String[] ids);

	/**
	 * 删除
	 * @param condition
	 */
	void delete(T condition);

	/**
	 * 查询单个
	 * @param id
	 * @return
	 */
	T findById(String id);

	/**
	 * 查询多个
	 * @param ids
	 * @return
	 */
	List<T> findByIds(String[] ids);

	/**
	 * 列表查询
	 * @param condition
	 * @return
	 */
	List<T> queryList(T condition);

	

	/**
	 * 根据条件记录总数
	 * @param condition
	 * @return
	 */
	int queryCount(T condition);

	/**
	 * 分页查询
	 * @param condition
	 * @return
	 */
	List<T> queryPage(T condition);
	
    int countByQuery(String query);
	
    List<T> findByQuery(String query);
}
