package com.exam.base.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;

import com.exam.base.util.ControllerUtil;

/**
 * 基础请求类
 * @author Administrator
 *
 */
public class BaseController {
	
	protected Log log = LogFactory.getLog(getClass());

	private String succView;
	private String warnView;
	private String errorView;
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final boolean DEFAULT_NOCACHE = true;
	public static final String TEXT_TYPE = "text/plain";
	public static final String JSON_TYPE = "application/json";

	/**
	 * 输出文本.
	 */
	protected void outText(final String text, final HttpServletResponse response)
	{
		print(TEXT_TYPE, text, response);
	}

	/**
	 * 输出JSON
	 * @param jsonString json字符串.
	 */
	protected void outJson(final String json, final HttpServletResponse response)
	{
		print(JSON_TYPE, json, response);
	}

	/**
	 * 输出内容.
	 */
	protected void print(final String contentType, final String content, final HttpServletResponse response)
	{
		response.setContentType(contentType + ";charset=" + DEFAULT_ENCODING);
		if (DEFAULT_NOCACHE)
		{
			setNoCacheHeader(response);
		}

		try
		{
			response.getWriter().write(content);
			response.getWriter().flush();
		}
		catch (IOException e)
		{
			log.error("IOException:", e);
		}
	}

	/**
	 * 下载文件
	 */
	public void down(HttpServletRequest request, HttpServletResponse response, String path, String fileName) throws Exception
	{
		ControllerUtil.downFile(request, response, path, fileName);
	}
 

	/**
	 * 设置客户端无缓存Header.
	 */
	protected void setNoCacheHeader(HttpServletResponse response)
	{
		// Http 1.0 header
		response.setDateHeader("Expires", 0);
		response.addHeader("Pragma", "no-cache");
		// Http 1.1 header
		response.setHeader("Cache-Control", "no-cache");
	}

	/**
	 * @param errMsg 错误提示信息
	 * @param modelMap
	 * @return
	 */
	protected ModelAndView resultErrView(String errMsg, Map<String, Object> modelMap)
	{
		if (modelMap == null || modelMap.isEmpty())
		{
			modelMap = new HashMap<String, Object>();
		}
		modelMap.put("errMsg", errMsg);
		modelMap.put("step", 2);
		return new ModelAndView(errorView).addAllObjects(modelMap);
	}

	/**
	 * @param promptMsg 成功提示信息
	 * @param modelMap
	 * @return
	 */
	protected ModelAndView resultSuccessView(String promptMsg, Map<String, Object> modelMap)
	{
		if (modelMap == null || modelMap.isEmpty())
		{
			modelMap = new HashMap<String, Object>();
		}
		modelMap.put("promptMsg", promptMsg);
		modelMap.put("step", 1);
		return new ModelAndView(succView).addAllObjects(modelMap);
	}

	/**
	 * @param warningMsg 警告提示信息
	 * @param modelMap
	 * @return
	 */
	protected ModelAndView resultWarningView(String warningMsg, Map<String, Object> modelMap)
	{
		if (modelMap == null || modelMap.isEmpty())
		{
			modelMap = new HashMap<String, Object>();
		}
		modelMap.put("warningMsg", warningMsg);
		modelMap.put("step", 3);
		return new ModelAndView(warnView).addAllObjects(modelMap);
	}

	public void setSuccView(String succView)
	{
		this.succView = succView;
	}

	public void setWarnView(String warnView)
	{
		this.warnView = warnView;
	}

	public void setErrorView(String errorView)
	{
		this.errorView = errorView;
	} 

	 
}
