package com.exam.base.model;

/**
 * 页面交互 对象
 * @author Administrator
 *
 */
public class ResponseModel {
	
	/**
	 * 请求标志
	 */
	private boolean requestFlag;
	
	/**
	 * 信息
	 */
	private String message;
	
	/**
	 * 码文
	 */
	private String sysCode;
	
	/**
	 * 返回数据
	 */
	private Object data;

	public boolean isRequestFlag() {
		return requestFlag;
	}

	public void setRequestFlag(boolean requestFlag) {
		this.requestFlag = requestFlag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
