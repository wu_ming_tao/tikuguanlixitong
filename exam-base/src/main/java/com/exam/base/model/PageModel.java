package com.exam.base.model;

/**
 * 分页信息
 * @author Administrator
 *
 */
public class PageModel {
	
	/**
	 * 总数
	 */
	private int total;
	
	/**
	 * 页码
	 */
	private int pageNum = 1;
	
	/**
	 * 页容量
	 */
	private int pageSize = 20;
	
	/**
	 * 总页数
	 */
	private int pageTotal;
	

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
 
	public void calPageTotal() {
		 if (total <= 0)
		 {
			 this.pageTotal = 0;
		 }
		 else
		 {
			 this.pageTotal = (int) Math.ceil((double)getTotal() / (double)getPageSize()) ;
		 }
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	} 
	
	
}
