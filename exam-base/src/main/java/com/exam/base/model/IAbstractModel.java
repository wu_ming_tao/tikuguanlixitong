package com.exam.base.model;

/**
 * 基础实体类
 * @author Administrator
 *
 */
public interface IAbstractModel {
	/** 主键唯一性ID */
	void setId(String id);

	/** 主键唯一性ID */
	String getId();

	/** 分页大小 */
	Integer getPageSize();

	/** 当前分页 */
	Integer getPageOffset();

	/** 分页起始下标 */
	void setBeginNum(Integer beginNum);

	/** 分页结束下标 */
	void setEndNum(Integer endNum);

	/** 查询条件 where */
	void linkQcStr(String qcPart);

	/** 查询条件参数 */
	void putQcParam(String key, Object value);

	/**
	 * 自定义更新字段 fieldname1=#{upParam.key1},fieldname2=#{upParam.key2},...
	 * @param upPart
	 */
	void linkUpStr(String upPart);

	/** 更新字段值 */
	void putUpParam(String key, Object value);
}
