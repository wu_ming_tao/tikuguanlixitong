package com.exam.base.model;

import java.io.Serializable;
import java.util.List;

/**
 * 传回分页信息 分页数据
 * @author Administrator
 *
 */
public class ResponsePageModel<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1190444149004778454L;
	
	/**
	 * 分页信息
	 */
	PageModel pageModel;
	
	/**
	 * 返回数据
	 */
	List<T> resultData;
	
	/**
	 * 是否有数据
	 */
	boolean hasData = true;
	
	/**
	 * 查询对象
	 */
	T queryModel;



	

	public PageModel getPageModel() {
		return pageModel;
	}

	public void setPageModel(PageModel pageModel) {
		this.pageModel = pageModel;
	}

	public List<T> getResultData() {
		return resultData;
	}

	public void setResultData(List<T> resultData) {
		this.resultData = resultData;
	}

	public boolean isHasData() {
		return hasData;
	}

	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}

	public T getQueryModel() {
		return queryModel;
	}

	public void setQueryModel(T queryModel) {
		this.queryModel = queryModel;
	}
	
	
	
}
