package com.exam.base.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 基础属性类
 * @author Administrator
 *
 */
public class AbstractModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4362335761811436404L;
	private Integer beginNum = 1;
	private Integer endNum;
	/** 默认20条 */
	private Integer pageSize = 20;
	/* 页码 */
	private Integer pageOffset = 1;
	private String sortName;
	private String sortOrder = "asc";

	private String p = "360000";
	private String c;
	private String a;

	public void setPage(int pageSize, int offset)
	{
		this.setPageOffset(offset);
		this.setPageSize(pageSize);
	}

	public String getP()
	{
		return p;
	}

	public void setP(String p)
	{
		this.p = p;
	}

	public String getC()
	{
		return c;
	}

	public void setC(String c)
	{
		this.c = c;
	}

	public String getA()
	{
		return a;
	}

	public void setA(String a)
	{
		this.a = a;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(Integer pageSize)
	{
		this.pageSize = pageSize > 0 ? pageSize : 20;
	}

	public Integer getPageOffset()
	{
		return pageOffset;
	}

	public void setPageOffset(Integer pageOffset)
	{
		this.pageOffset = pageOffset > 0 ? pageOffset : 1;
	}

	public String getSortName()
	{
		return sortName;
	}

	public void setSortName(String sortName)
	{
		this.sortName = sortName;
	}

	public String getSortOrder()
	{
		return sortOrder;
	}

	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public Integer getBeginNum()
	{
		return beginNum;
	}

	public void setBeginNum(Integer beginNum)
	{
		this.beginNum = beginNum;
	}

	public Integer getEndNum()
	{
		return endNum;
	}

	public void setEndNum(Integer endNum)
	{
		this.endNum = endNum;
	}

	/** 查询条件 */
	private transient String qcStr;
	/** 更新字段 */
	private transient String upStr;
	/** 查询条件参数 */
	private transient Map<String, Object> qcParam = new HashMap<String, Object>();
	/** 更新字段值 */
	private transient Map<String, Object> upParam = new HashMap<String, Object>();

	/** 查询条件 */
	public String getQcStr()
	{
		return qcStr;
	}

	/** 查询条件 where */
	public void linkQcStr(String qcPart)
	{
		if (this.qcStr == null)
		{
			this.qcStr = qcPart;
		}
		else
		{
			this.qcStr += qcPart;
		}
	}

	/** 查询条件参数 */
	public Map<String, Object> getQcParam()
	{
		return qcParam;
	}

	/** 查询条件参数 */
	public void putQcParam(String key, Object value)
	{
		this.qcParam.put(key, value);
	}

	/** 查询条件参数 */
	public String getUpStr()
	{
		return upStr;
	}

	/**
	 * 自定义更新字段 fieldname1=#{upParam.key1},fieldname2=#{upParam.key2},...
	 * @param upPart
	 */
	public void linkUpStr(String upPart)
	{
		if (this.upStr == null)
		{
			this.upStr = upPart;
		}
		else
		{
			this.upStr += upPart;
		}
	}

	/** 更新字段值 */
	public Map<String, Object> getUpParam()
	{
		return upParam;
	}

	/** 更新字段值 */
	public void putUpParam(String key, Object value)
	{
		this.upParam.put(key, value);
	}
}
