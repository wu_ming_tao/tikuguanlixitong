package com.exam.base.business;

import java.util.List;

import com.exam.base.model.IAbstractModel;
import com.exam.base.model.PageModel;
import com.exam.base.model.ResponsePageModel;
import com.exam.base.service.IAbstractService;

/**
 * 基本底层业务
 * @author Administrator
 *
 * @param <T>
 */
public abstract class AbstractBusiness<T extends IAbstractModel> {
	
	protected abstract IAbstractService<T> getBaseService();
	
	/**
	 * 分页查询数据
	 * @param queryModel
	 * @return
	 */
	public ResponsePageModel<T> getPageData (T queryModel)
	{
		try {
			ResponsePageModel<T> dataModel = new ResponsePageModel<T>(); 
			 
			int dataCount = getBaseService().queryCount(queryModel);
			if (dataCount <= 0)
			{
				dataModel.setHasData(false);
				return  dataModel;
			}
			queryModel.setBeginNum((queryModel.getPageOffset() - 1) * queryModel.getPageSize());
			List<T> lstData = getBaseService().queryPage(queryModel);
			// 封装分页信息
			PageModel pageModel = new PageModel();
			pageModel.setPageNum(queryModel.getPageOffset());
			pageModel.setPageSize(queryModel.getPageSize());
			pageModel.setTotal(dataCount); 
			pageModel.calPageTotal();
			
			dataModel.setPageModel(pageModel);
			dataModel.setQueryModel(queryModel);
			dataModel.setResultData(lstData);
			return dataModel; 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}	
	/**
	 * 通过主键获取数据
	 * @param id
	 * @return
	 */
	public T findById(String id)
	{
		return getBaseService().findById(id);
	}
	
	/**
	 * 新增
	 * @param mainModel
	 */
	public void add(T mainModel)
	{
	   getBaseService().add(mainModel);
	}

	/**
	 * 批量新增
	 * @param addList
	 */
	public void batchAdd(List<T> addList)
	{
		getBaseService().batchAdd(addList);
	}

	/**
	 * 更新
	 * @param mainModel
	 */
	public void update(T mainModel)
	{
		getBaseService().update(mainModel);
	}

	/**
	 * 批量更新
	 * @param updateList
	 */
	public void batchUpdate(List<T> updateList)
	{
		getBaseService().batchUpdate(updateList);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void delete(String id)
	{
		getBaseService().delete(id);
	}

	/**
	 * 删除
	 * @param ids
	 */
	public void delete(String[] ids)
	{
		getBaseService().delete(ids);
	}

	/**
	 * 删除
	 * @param condition
	 */
	public void delete(T condition)
	{
		getBaseService().delete(condition);
	}
 

	/**
	 * 查询多个
	 * @param ids
	 * @return
	 */
	public List<T> findByIds(String[] ids)
	{
		return getBaseService().findByIds(ids);
	}

	/**
	 * 列表查询
	 * @param condition
	 * @return
	 */
	public List<T> queryList(T condition)
	{
		return getBaseService().queryList(condition);
	}

	/**
	 * 根据条件记录总数
	 * @param condition
	 * @return
	 */
	public int queryCount(T condition)
	{
		return getBaseService().queryCount(condition);
	}

	/**
	 * 分页查询
	 * @param condition
	 * @return
	 */
	public List<T> queryPage(T condition)
	{
		return getBaseService().queryPage(condition);
		
	}
	/**
	 * 根据String条件记录总数
	 * @param query
	 * @return
	 */
	public int countByQuery(String query)
	{
		return getBaseService().countByQuery(query);
	}
	/**
	 * String分页查询
	 * @param query
	 * @return
	 */
	public List<T> queryPageBy(String query)
	{
		return getBaseService().findByQuery(query);
		
	}
}
