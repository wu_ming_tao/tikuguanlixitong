package com.exam.base.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.dao.DataAccessException;

import com.exam.base.util.DaoHelperUtil;

public class IAbstractDaoImpl<T> implements IAbstractDao<T> {
	protected String namespace = getDefaultSqlNamespace();
	private SqlSessionTemplate sqlSessionTemplate;

	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate)
	{
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	public SqlSessionTemplate getSqlMapClientTemplate()
	{
		return sqlSessionTemplate;
	}

	public String getNamespace()
	{
		return namespace;
	}

	public void setNamespace(String namespace)
	{
		this.namespace = namespace.concat(".");
	}

	@SuppressWarnings("unchecked")
	protected String getDefaultSqlNamespace()
	{
		Class<T> clazz = DaoHelperUtil.getClassGenricType((Class<T>) getClass(),0);
		return "nameSpace_" + clazz.getSimpleName().concat(".");
	}

	
	
	public T findById(Object primaryKey)
	{
		return sqlSessionTemplate.selectOne(namespace.concat("selectByPrimaryKey"), primaryKey);
	}

	public List<T> findByIds(List<String> ids)
	{
		if (ids == null || ids.size() == 0)
		{
			return Collections.emptyList();
		}

		// 去重
		List<String> idsUnqiue = DaoHelperUtil.removeDuplicate(ids);
		// 按800长度拆分
		Map<String, List<String>> idsMap = DaoHelperUtil.splitList(idsUnqiue, 800);

		List<T> allList = new ArrayList<T>();
		for (String key : idsMap.keySet())
		{
			List<T> splitList = sqlSessionTemplate.selectList(namespace.concat("selectByPrimaryKeys"), idsMap.get(key));
			allList.addAll(splitList);
		}
		return allList;
	}

	public Object add(T pojo)
	{
		return sqlSessionTemplate.insert(namespace.concat("insert"), pojo);
	}

	public boolean delete(Object id)
	{
		return sqlSessionTemplate.delete(namespace.concat("deleteByPrimaryKey"), id) == 1;
	}

	public boolean deleteByIds(List<String> ids)
	{
		boolean allflag = true;
		if (ids == null || ids.isEmpty())
		{
			return allflag;
		}
		// 按800长度拆分
		Map<String, List<String>> idsMap = DaoHelperUtil.splitList(ids, 800);
		for (String key : idsMap.keySet())
		{
			boolean flag = sqlSessionTemplate.delete(namespace.concat("deleteByPrimaryKeys"), idsMap.get(key)) == 1;
			if (flag == false)
			{
				allflag = false;
			}
		}

		return allflag;

	}
	
	
	
	public boolean deleteByModel(Object criteria)
	{
		return sqlSessionTemplate.delete(namespace.concat("deleteByExample"), criteria) >= 1;
	}

	public boolean update(T pojo)
	{
		return sqlSessionTemplate.update(namespace.concat("updateByPrimaryKeySelective"), pojo) == 1;
	}

	public List<T> findByModel(Object criteria)
	{
		return sqlSessionTemplate.selectList(namespace.concat("selectByExample"), criteria);
	}
	
	public List<T> findByQuery(String query)
	{
		return sqlSessionTemplate.selectList(namespace.concat("selectByQueryContent"), query);
	}

	public int countByModel(Object criteria)
	{
		return (Integer) sqlSessionTemplate.selectOne(namespace.concat("countByExample"), criteria);
	}
	
	public int countByQuery(String query)
	{
		return (Integer) sqlSessionTemplate.selectOne(namespace.concat("countByQueryContent"), query);
	}

	public List<T> findByModelPage(Object criteria)
	{
		return sqlSessionTemplate.selectList(namespace.concat("selectByPage"), criteria);
	}

	public void batchCreate(final List<T> pojoList, final String sqlId) throws DataAccessException
	{
		SqlSession session = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
		try
		{
			int batch = 0;
			for (T record : pojoList)
			{
				session.insert(namespace.concat(sqlId), record);
				batch++;
				if (batch == 1000)
				{
					session.commit();
					session.clearCache();
					batch = 0;
				}
			}
			session.commit();
		}
		finally
		{
			session.close();
		}
	}

	public void batchUpdate(final List<T> pojoList, final String sqlId)
	{
		SqlSession session = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
		try
		{
			int batch = 0;
			for (T record : pojoList)
			{
				session.update(namespace.concat(sqlId), record);
				batch++;
				if (batch == 1000)
				{
					session.commit();
					session.clearCache();
					batch = 0;
				}
			}
			session.commit();
		}
		finally
		{
			session.close();
		}
	}

	public void batchDel(final List<Long> pojoList, final String sqlId)
	{
		SqlSession session = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
		try
		{
			int batch = 0;
			for (Long record : pojoList)
			{
				session.delete(namespace.concat(sqlId), record);
				batch++;
				if (batch == 1000)
				{
					session.commit();
					session.clearCache();
					batch = 0;
				}
			}
			session.commit();
		}
		finally
		{
			session.close();
		}
	}

	
}
