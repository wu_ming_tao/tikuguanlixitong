package com.exam.base.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

/**
 * 基本服务接口
 * @author Administrator
 *
 */
public interface IAbstractDao<T> {
	/**
	 * 新增
	 * @param pojo
	 * @return
	 */
	public Object add(T pojo);

	/**
	 * 根据id查询单个
	 * @param primaryKey
	 * @return
	 */
	public T findById(Object id);

	/**
	 * 根据id查询多个
	 * @param ids
	 * @return
	 */
	public List<T> findByIds(List<String> ids);

	/**
	 * 根据id删除
	 * @param id
	 * @return
	 */
	public boolean delete(Object id);

	/**
	 * 根据id批量删除
	 * @param ids
	 * @return
	 */
	public boolean deleteByIds(List<String> ids);

	/**
	 * 条件删除
	 * @param criteria
	 * @return
	 */
	public boolean deleteByModel(Object model);

	/**
	 * 更新
	 * @param pojo
	 * @return
	 */
	public boolean update(T pojo);

	/**
	 * 条件查询
	 * @param criteria
	 * @return
	 */
	public List<T> findByModel(Object model);
	

	/**
	 * 条件总记录数
	 * @param criteria
	 * @return
	 */
	public int countByModel(Object model);
	/**
	 * 根据String条件查询
	 * @param query
	 * @return
	 */
	public List<T> findByQuery(String query);
	/**
	 * 根据String条件总记录数
	 * @param query
	 * @return
	 */
	public int countByQuery(String query);

	/**
	 * 条件分页
	 * @param criteria
	 * @return
	 */
	public List<T> findByModelPage(Object model);

	/**
	 * 批量创建
	 * @param pojoList
	 * @param sqlId
	 * @throws DataAccessException
	 */
	public void batchCreate(final List<T> pojoList, final String sqlId) throws DataAccessException;

	/**
	 * 批量更新
	 * @param pojoList
	 * @param sqlId
	 */
	public void batchUpdate(final List<T> pojoList, final String sqlId);

	/**
	 * 批量删除
	 * @param pojoList
	 * @param sqlId
	 */
	public void batchDel(final List<Long> pojoList, final String sqlId);
}
